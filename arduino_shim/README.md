# Arduino Shim for FreeRTOS
Allows Arduino suites to run on top of FreeRTOS and [emulated HAL clone GPIOs](../include/hal-clone/common/gpio.h).
Is a work in progress and provides limited support.

### Supported libraries
- [`Arduino.h`](include/Arduino.h) (partial)
- [`rstc.h`](include/include/rstc.h) (partial)
- [`arduino-timer.h`](include/arduino-timer.h) (partial shim)
- [`DueFlashStorage.h`](DueFlashStorage.h)
- [`twi.h`](include/include/twi.h) (do-nothing stubs only)

## Building
The GNU ARM toolchain 4_7-2013q3 version is used for building.

The C++ source files must be built using `-fno-exceptions` and `-lc_s`. Specifying `--specs nano.specs` to `g++`
during the linking step does not work due to a [bug](https://stackoverflow.com/questions/41535795/preprocessing-with-g-and-specs-file)
in the 4_7-2013q3 version of the GNU arm toolchain. Therefore, `-lAt91D` must be specified *after* `-lc_s` since the At91 library
defines some low-level functions needed by the standard C library.

Furthermore, if your code calls `exit()`, you may need to define `-fno-use-cxa-atexit` to the C++ compiler and ensure that there
is a `void _fini(void)` function defined somewhere.

### Simulating multiple Arduino units
This shim can differentiate between calls to Arduino standard functions from one
Arduino unit and another. To allow this, it is necessary to define the constant
`ARDUINO_UNIT_ID` to an 8-bit value that is unique to that Arduino unit prior
to using it. For example:

```c
// my-header.h
#define ARDUINO_UNIT_ID_TEST  (231)

#define MY_INPUT_PIN (67)
#define MY_OUTPUT_PIN (24)

// my-suite.c
#include "my-header.h"

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_TEST

void my_setup()
{
	pinMode(MY_INPUT_PIN, INPUT_PULLUP);
	pinMode(MY_OUTPUT_PIN, OUTPUT);

	// Rest of code here...
}
```

### Connecting Arduino pins
Prior to running the setup functions of Arduino suites, the pins that they configure
using `pinMode()` must be connected to another Arduino unit or to ground, VDD, or "nothing".
For example:
```c
#include <hal-clone/common/gpio.h>
#include <hal-clone/ardunio.h>

#include "my-header.h"

#define ARDUINO_UNIT_ID_ANOTHER  (149)
#define ANOTHER_INPUT_PIN        (5)

bool my_isr()
{
	// Return some value that can change depending on the time
	return millis() & 1;
}

void before_setup()
{
	// Connect MY_INPUT_PIN to ground
	hal_clone_gpio_connect_to_fixed(hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_TEST, MY_INPUT_PIN),
	                                "MY_PIN",
	                                 HAL_CLONE_GPIO_LOW);

	// Connect MY_OUTPUT_PIN to an input pin from another Arduino unit
	hal_clone_gpio_connect(hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_TEST, MY_OUTPUT_PIN),
	                       "MY_OUTPUT_PIN",
	                       hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_ANOTHER, ANOTHER_INPUT_PIN),
	                       "ANOTHER_INPUT_PIN");

	// Set the "output ISR" of MY_OUTPUT_PIN, such that when ANOTHER_INPUT_PIN wants to read the
	// value of it, the ISR can lazily return its updated value
	hal_clone_arduino_set_output_isr(ARDUINO_UNIT_ID_TEST, MY_OUTPUT_PIN, my_isr);
}
```

For details, see the [Arduino.h implementation](src/Arduino.c) and the [HAL clone common GPIO implementation](../src/common/gpio.c).
