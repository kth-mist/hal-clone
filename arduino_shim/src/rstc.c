/**
 * @file      rstc.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs used by any Arduino-based peripheral
 *            simulators.
 */

#include <stdlib.h>

#include <hal-clone/semihosting.h>
#include <hal-clone/arduino.h>

struct rstc_handlers {
	int arduino_unit_id;
	void (*cb)(void);
	struct rstc_handlers *next;
};
struct rstc_handlers *_handlers = NULL;

void hal_clone_arduino_rstc_reset(int _unit, int base)
{
	(void) base;
	struct rstc_handlers **iter;

	for (iter = &_handlers; *iter != NULL; iter = &(*iter)->next) {
		if ((*iter)->arduino_unit_id == _unit) {
			(*iter)->cb();
			return;
		}
	}

	HAL_CLONE_ARDUINO_WARNING("No reset handler registered for unit %d", _unit);
}

/**
 * @brief Registers a callback function that should be called when an
 *        Arduino unit requests to soft reset.
 *
 * @param[in] unit The Arduino unit identifier for which the callback
 *                 should be called.
 * @param[in] reset The reset handler callback function.
 */
void hal_clone_arduino_rstc_regsiter_cb(int unit, void (*reset)(void))
{
	struct rstc_handlers **iter;

	for (iter = &_handlers; *iter != NULL; iter = &(*iter)->next) {
		if ((*iter)->arduino_unit_id == unit) {
			(*iter)->cb = reset;
			return;
		}
	}
	*iter = malloc(sizeof(struct rstc_handlers));
	if (*iter == NULL) {
		HAL_CLONE_ARDUINO_WARNING("Failed to allocate memory");
		semihosting_exit(ADP_Stopped_ApplicationExit);
	}
	(*iter)->arduino_unit_id = unit;
	(*iter)->cb = reset;
	(*iter)->next = NULL;
}
