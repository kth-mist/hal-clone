/**
 * @file      Arduino.cpp
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Arduino stubs used by any Arduino-based peripheral
 *            simulators.
 */

extern "C" {
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdarg.h>
	#include <string.h>

	#include <Arduino.h>

	#include <hal-clone/common/gpio.h>
	#include <hal-clone/arduino.h>
	#include <hal-clone/semihosting.h>

	#include <freertos/FreeRTOS.h>
	#include <freertos/task.h>
}

/* @brief Input pin format, used to provide a simpler ISR signature
          compared to the common GPIO format's ISR */
struct arduino_shim_input_pin {
	struct hal_clone_gpio input;
	void (*isr)(void);
	unsigned char mode; // FALLING, RISING, or CHANGE
	struct arduino_shim_input_pin *next;
};
/* @brief Output pin format, used to provide a simpler ISR signature
          compared to the common GPIO format's ISR */
struct arduino_shim_output_pin {
	struct hal_clone_gpio output;
	bool (*isr)(void);
	struct arduino_shim_output_pin *next;
};
static struct arduino_shim_input_pin *_arduino_input_pins = NULL;
static struct arduino_shim_output_pin *_arduino_output_pins = NULL;

static enum hal_clone_gpio_value arduino_shim_isr(struct hal_clone_gpio pin,
                                                  enum hal_clone_gpio_status status,
                                                  enum hal_clone_gpio_value new_value);

#ifdef __cplusplus
ArduinoSerial Serial;
#endif

unsigned long millis(void)
{
	return (unsigned long) (xTaskGetTickCount() / portTICK_RATE_MS);
}

void arduino_shim_pin_mode(int _unit, int pin, unsigned char mode)
{
	enum hal_clone_gpio_status status;
	enum hal_clone_gpio_value initial_value = HAL_CLONE_GPIO_UNDEFINED;
	switch (mode) {
	case INPUT:
		status = HAL_CLONE_GPIO_INPUT;
		break;
	case OUTPUT:
		status = HAL_CLONE_GPIO_OUTPUT;
		break;
	case INPUT_PULLUP:
		status = HAL_CLONE_GPIO_INPUT;
		initial_value = HAL_CLONE_GPIO_HIGH;
		break;
	default:
		HAL_CLONE_ARDUINO_WARNING("Bad mode %d", mode);
		status = HAL_CLONE_GPIO_UNINITIALIZED;
		break;
	}
	(void) hal_clone_gpio_configure(hal_clone_arduino_pin_to_gpio(_unit, pin), status, initial_value);
}

void arduino_shim_digital_write(int _unit, int pin, bool value)
{
	(void) hal_clone_gpio_set(hal_clone_arduino_pin_to_gpio(_unit, pin),
	                          value ? HAL_CLONE_GPIO_HIGH : HAL_CLONE_GPIO_LOW);
}

bool arduino_shim_digital_read(int _unit, int pin)
{
	enum hal_clone_gpio_value value;
	int err = hal_clone_gpio_get(hal_clone_arduino_pin_to_gpio(_unit, pin), &value);
	if (err != 0 || value == HAL_CLONE_GPIO_UNDEFINED) {
		return (rand() & 1) ? true : false;
	}
	return value == HAL_CLONE_GPIO_HIGH ? true : false;
}

void arduino_shim_attach_interrupt(int _unit, int interrupt, void (*isr)(void), unsigned char mode)
{
	int err;
	int pin = interrupt;
	struct arduino_shim_input_pin **iter;
	struct hal_clone_gpio gpio_pin = hal_clone_arduino_pin_to_gpio(_unit, pin);

	err = hal_clone_gpio_set_isr(gpio_pin, arduino_shim_isr);
	if (err != 0)
		return;

	for (iter = &_arduino_input_pins; *iter != NULL; iter = &(*iter)->next) {
		if (hal_clone_gpio_equal((*iter)->input, gpio_pin)) {
			(*iter)->isr = isr;
			(*iter)->mode = mode;
			return;
		}
	}

	*iter = (struct arduino_shim_input_pin*) malloc(sizeof(struct arduino_shim_input_pin));
	if (*iter == NULL) {
		HAL_CLONE_ARDUINO_WARNING("Failed to allocate memory");
		semihosting_exit(ADP_Stopped_ApplicationExit);
	}
	(*iter)->input = gpio_pin;
	(*iter)->isr = isr;
	(*iter)->mode = mode;
	(*iter)->next = NULL;
}

void hal_clone_arduino_set_output_isr(int unit, int pin, bool (*isr)(void))
{
	int err;
	struct arduino_shim_output_pin **iter;
	struct hal_clone_gpio gpio_pin = hal_clone_arduino_pin_to_gpio(unit, pin);

	err = hal_clone_gpio_set_isr(gpio_pin, arduino_shim_isr);
	if (err != 0)
		return;

	for (iter = &_arduino_output_pins; *iter != NULL; iter = &(*iter)->next) {
		if (hal_clone_gpio_equal((*iter)->output, gpio_pin)) {
			(*iter)->isr = isr;
			return;
		}
	}

	*iter = (struct arduino_shim_output_pin*) malloc(sizeof(struct arduino_shim_output_pin));
	if (*iter == NULL) {
		HAL_CLONE_ARDUINO_WARNING("Failed to allocate memory");
		semihosting_exit(ADP_Stopped_ApplicationExit);
	}
	(*iter)->output = gpio_pin;
	(*iter)->isr = isr;
	(*iter)->next = NULL;
}

int arduino_shim_digital_pin_to_interrupt(int _unit, int pin)
{
	(void) _unit;

	return pin; // No need to stub interrupt numbers, just return the pin
}

/**
 * @brief Converts Arduino pins to HAL clone common GPIOs.
 *
 * @param[in] unit The Arduino unit identifier that the pin belongs to.
 * @param[in] pin The pin to convert.
 *
 * @return The common GPIO format.
 */
struct hal_clone_gpio hal_clone_arduino_pin_to_gpio(int unit, int pin)
{
	struct hal_clone_gpio gpio_pin;
	gpio_pin.kind = HAL_CLONE_GPIO_KIND_ARDUINO;
	gpio_pin.id = ((unit & 0xFF) << 16) | (pin & 0xFFFF);

	return gpio_pin;
}

/**
 * @brief Interrupt service routine that should be called by the common
 *        GPIO implementation. Figures out which ISR to call based on
 *        the pin, and whether it should be called based on if there was
 *        a falling or rising event.
 */
static enum hal_clone_gpio_value arduino_shim_isr(struct hal_clone_gpio pin,
                                                  enum hal_clone_gpio_status status,
                                                  enum hal_clone_gpio_value new_value)
{
	if (status == HAL_CLONE_GPIO_INPUT) {
		struct arduino_shim_input_pin **iter;
		for (iter = &_arduino_input_pins; *iter != NULL; iter = &(*iter)->next) {
			if (hal_clone_gpio_equal((*iter)->input, pin)) {
				int mode = (*iter)->mode;
				if (mode == CHANGE ||
				    (mode == RISING && new_value == HAL_CLONE_GPIO_HIGH) ||
				    (mode == FALLING && new_value == HAL_CLONE_GPIO_LOW)) {
					(*iter)->isr();
				}
				return HAL_CLONE_GPIO_UNDEFINED;
			}
		}
		HAL_CLONE_ARDUINO_WARNING("Pin kind %d and id 0x%x has no input ISR", pin.kind, pin.id);
	} else if (status == HAL_CLONE_GPIO_OUTPUT) {
		struct arduino_shim_output_pin **iter;
		for (iter = &_arduino_output_pins; *iter != NULL; iter = &(*iter)->next) {
			if (hal_clone_gpio_equal((*iter)->output, pin)) {
				return (*iter)->isr() ? HAL_CLONE_GPIO_HIGH : HAL_CLONE_GPIO_LOW;
			}
		}
		HAL_CLONE_ARDUINO_WARNING("Pin kind %d and id 0x%x has no output ISR", pin.kind, pin.id);
	}

	return HAL_CLONE_GPIO_UNDEFINED;
}

void hal_clone_arduino_warning(const char *file, int line, const char *format, ...)
{
	va_list argp;

	const char *prefixes[] = {
		"../arduino_shim/src/"
	};
	size_t numprefixes = sizeof(prefixes) / sizeof(prefixes[0]);

	for (size_t i = 0; i < numprefixes; i++) {
		const char *trimmed_file = strstr(file, prefixes[i]);
		if (trimmed_file != NULL) {
			size_t len = strlen(prefixes[i]);
			file = &trimmed_file[len];
			break;
		}
	}

	va_start(argp, format);
	fprintf(stderr, "[ARDUINO WARNING] %s:%d: ", file, line);
	vfprintf(stderr, format, argp);
	va_end(argp);

	fprintf(stderr, "\n");
}
