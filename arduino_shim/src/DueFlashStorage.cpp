/**
 * @file      DueFlashStorage.cpp
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs for the DueFlashStorage library
 */

extern "C" {
	#include <stdint.h>
	#include <stdlib.h>
	#include <string.h>
	#include <Arduino.h>

	#include <hal-clone/semihosting.h>
	#include <hal-clone/semihosting_memory.h>
	#include <hal-clone/arduino.h>
}

#include <DueFlashStorage.h>

struct DueFlashStorages {
	int arduino_unit_id;
	int semihosting_fd;
	uint8_t *memory;
	uint32_t base_address;
	size_t size;
	uint8_t *last_readaddress_mem = NULL;
	struct DueFlashStorages *next;
};
struct DueFlashStorages *_flash_storages = NULL;

static struct DueFlashStorages *find_flash_storage(int unit);

uint8_t DueFlashStorage::_read(int unit, uint32_t address)
{
	int err;
	uint8_t value;
	struct DueFlashStorages *flash_storage = find_flash_storage(unit);
	if (flash_storage == NULL) {
		HAL_CLONE_ARDUINO_WARNING("No DueFlashStorage unit found for unit %d", unit);
		return 0;
	}

	if (address < flash_storage->base_address || address >= flash_storage->base_address + flash_storage->size) {
		HAL_CLONE_ARDUINO_WARNING("Address out of bounds for DueFlashStorage unit %d, attempted read at 0x%x",
		                          unit, address);
		return 0;
	}

	if (flash_storage->semihosting_fd > 0) {
		err = semihosting_memory_read(flash_storage->semihosting_fd,
		                              (off_t) (address - flash_storage->base_address),
		                              &value,
		                              1);
		if (err != 0) {
			HAL_CLONE_ARDUINO_WARNING("Failed to read from semihosting file");
			return 0;
		}
		return value;
	} else if (flash_storage->memory != NULL) {
		return flash_storage->memory[address - flash_storage->base_address];
	}

	HAL_CLONE_ARDUINO_WARNING("Internal error: No storage backend for DueFlashStorage unit %d", unit);
	semihosting_exit(ADP_Stopped_InternalError);
	return 0;
}

uint8_t *DueFlashStorage::_readAddress(int unit, uint32_t address)
{
	int err;
	size_t len;
	struct DueFlashStorages *flash_storage = find_flash_storage(unit);
	if (flash_storage == NULL) {
		HAL_CLONE_ARDUINO_WARNING("No DueFlashStorage unit found for unit %d", unit);

		// Exit since at this point there be chaos...
		semihosting_exit(ADP_Stopped_InternalError);
	}

	if (address < flash_storage->base_address || address >= flash_storage->base_address + flash_storage->size) {
		HAL_CLONE_ARDUINO_WARNING("Address out of bounds for DueFlashStorage unit %d, attempting read at 0x%x",
		                          unit, address);
		
		// Exit since at this point there be chaos...
		semihosting_exit(ADP_Stopped_InternalError);
	}

	if (flash_storage->last_readaddress_mem != NULL) {
		free(flash_storage->last_readaddress_mem);
		flash_storage->last_readaddress_mem = NULL;
	}

	if (flash_storage->semihosting_fd > 0) {
		len = flash_storage->base_address + flash_storage->size - address;
		flash_storage->last_readaddress_mem = (uint8_t *) malloc(len);

		err = semihosting_memory_read(flash_storage->semihosting_fd,
		                              (off_t) (address - flash_storage->base_address),
		                              flash_storage->last_readaddress_mem,
		                              len);
		if (err != 0) {
			HAL_CLONE_ARDUINO_WARNING("Failed to read from semihosting file");
			
			// Fall through and return pointer to garbage, at least it isn't out of bounds...
		}
		return flash_storage->last_readaddress_mem;
	} else if (flash_storage->memory != NULL) {
		return flash_storage->memory + address - flash_storage->base_address;
	}

	HAL_CLONE_ARDUINO_WARNING("Internal error: No storage backend for DueFlashStorage unit %d", unit);
	semihosting_exit(ADP_Stopped_InternalError);
	return NULL;
}

bool DueFlashStorage::_write(int unit, uint32_t address, uint8_t value)
{
    uint8_t data[1] = { value };

    return _write(unit, address, data, 1);
}

bool DueFlashStorage::_write(int unit, uint32_t address, uint8_t *data, uint32_t len)
{
	int err;
	struct DueFlashStorages *flash_storage = find_flash_storage(unit);
	if (flash_storage == NULL) {
		HAL_CLONE_ARDUINO_WARNING("No DueFlashStorage unit found for unit %d", unit);
		return false;
	}

	if (address < flash_storage->base_address || address + len > flash_storage->base_address + flash_storage->size) {
		HAL_CLONE_ARDUINO_WARNING("Address out of bounds for DueFlashStorage unit %d, attempted to write %u byte(s) at 0x%x",
		                          unit, len, address);
		return false;
	}

	if (flash_storage->last_readaddress_mem != NULL) {
		free(flash_storage->last_readaddress_mem);
		flash_storage->last_readaddress_mem = NULL;
	}

	if (flash_storage->semihosting_fd > 0) {
		err = semihosting_memory_write(flash_storage->semihosting_fd,
		                               (off_t) (address - flash_storage->base_address),
		                               data,
		                               len);
		if (err != 0) {
			HAL_CLONE_ARDUINO_WARNING("Failed to write to semihosting file");
			return false;
		}
		return true;
	} else if (flash_storage->memory != NULL) {
		memcpy(flash_storage->memory + address - flash_storage->base_address, data, len);
		return true;
	}

	HAL_CLONE_ARDUINO_WARNING("Internal error: No storage backend for DueFlashStorage unit %d", unit);
	semihosting_exit(ADP_Stopped_InternalError);
	return false;
}

/**
 * @brief Sets a path to a file on the host that should be used as DueFlashStorage memory with semihosting.
 *
 * @param[in] unit The Arduino unit identifier for which the memory applies.
 * @param[in] path The path to the file on the host.
 * @param[in] base_address The base address that the Arduino uses to access the flash memory.
 *                         Address 0 in the semihosting file corresponds to this address.
 * @param[in] size The size of the memory, i.e. the size of the file on the host. The size
 *                 must be large enough to encompass all addresses that the Arduino accesses.
 */
void hal_clone_arduino_DueFlashStorage_set_semihosting_file(int unit, const char *path, uint32_t base_address, size_t size)
{
	int fd;
	struct DueFlashStorages **iter;

	if (path == NULL || size == 0) {
		HAL_CLONE_ARDUINO_WARNING("Invalid arguments for DueFlashStorage semihosting file");
		semihosting_exit(ADP_Stopped_InternalError);
	}

	for (iter = &_flash_storages; *iter != NULL; iter = &(*iter)->next) {
		if ((*iter)->arduino_unit_id == unit) {
			fd = (*iter)->semihosting_fd;
			if (fd >= 0) {
				HAL_CLONE_ARDUINO_WARNING("Semihosting file already set for unit %d, closing existing file", unit);
				semihosting_memory_close(fd);
			}
			break;
		}
	}

	if (*iter == NULL) {
		*iter = (struct DueFlashStorages *) malloc(sizeof(struct DueFlashStorages));
		if (*iter == NULL) {
			HAL_CLONE_ARDUINO_WARNING("Failed to allocate memory");
			semihosting_exit(ADP_Stopped_InternalError);
		}
		(*iter)->arduino_unit_id = unit;
		(*iter)->next = NULL;
	}

	fd = semihosting_memory_open(path, size, 0xFF);
	if (fd < 1) {
		HAL_CLONE_ARDUINO_WARNING("Failed to open semihosting file %s", path);
		semihosting_exit(ADP_Stopped_InternalError);
	}
	(*iter)->semihosting_fd = fd;
	(*iter)->base_address = base_address;
	(*iter)->size = size;
}

/**
 * @brief Sets a memory location that should be used as DueFlashStorage memory.
 *
 * @note If the semihost file is set, this function will not have any effect.
 *
 * @param[in] unit The Arduino unit identifier for which the memory applies.
 * @param[in] memory The memory location to use.
 * @param[in] base_address The base address that the Arduino uses to access the flash memory.
 *                         Address 0 in the provided memory corresponds to this address.
 * @param[in] size The size of the provided memory. The size must be large enough to encompass
 *                 all addresses that the Arduino accesses.
 */
void hal_clone_arduino_DueFlashStorage_set_in_memory(int unit, uint8_t *memory, uint32_t base_address, size_t size)
{
	unsigned int i;
	struct DueFlashStorages **iter;

	if (memory == NULL || size == 0) {
		HAL_CLONE_ARDUINO_WARNING("Invalid arguments for DueFlashStorage in memory");
		semihosting_exit(ADP_Stopped_InternalError);
	}

	for (iter = &_flash_storages; *iter != NULL; iter = &(*iter)->next) {
		if ((*iter)->arduino_unit_id == unit) {
			if ((*iter)->semihosting_fd >= 0) {
				HAL_CLONE_ARDUINO_WARNING("Semihosting file already set for unit %d, closing existing file", unit);
				semihosting_memory_close((*iter)->semihosting_fd);
			}
			break;
		}
	}

	if (*iter == NULL) {
		*iter = (struct DueFlashStorages *) malloc(sizeof(struct DueFlashStorages));
		if (*iter == NULL) {
			HAL_CLONE_ARDUINO_WARNING("Failed to allocate memory");
			semihosting_exit(ADP_Stopped_InternalError);
		}
		(*iter)->arduino_unit_id = unit;
		(*iter)->next = NULL;
	}

	for (i = 0; i < size; i++)
		memory[i] = 0xFF;

	(*iter)->semihosting_fd = -1;
	(*iter)->memory = memory;
	(*iter)->base_address = base_address;
	(*iter)->size = size;
}

/**
 * @brief Erases the DueFlashStorage memory of an Arduino unit.
 *
 * @param[in] unit The Arduino unit identifier whose memory to erase.
 */
void hal_clone_arduino_DueFlashStorage_erase(int unit)
{
	int err;

	struct DueFlashStorages *flash_storage = find_flash_storage(unit);
	if (flash_storage == NULL) {
		HAL_CLONE_ARDUINO_WARNING("No DueFlashStorage unit found for unit %d", unit);
		return;
	}

	if (flash_storage->semihosting_fd > 0) {
		err = semihosting_memory_fill(flash_storage->semihosting_fd, flash_storage->size, 0xFF);
		if (err != 0) {
			HAL_CLONE_ARDUINO_WARNING("Failed to erase semihosting file");
			// TODO Handle error
		}
		return;
	} else if (flash_storage->memory != NULL) {
		memset(flash_storage->memory, 0xFF, flash_storage->size);
		return;
	}

	HAL_CLONE_ARDUINO_WARNING("Internal error: No storage backend for DueFlashStorage unit %d", unit);
	semihosting_exit(ADP_Stopped_InternalError);
}

static struct DueFlashStorages *find_flash_storage(int unit)
{
	struct DueFlashStorages **iter;

	for (iter = &_flash_storages; *iter != NULL; iter = &(*iter)->next) {
		if ((*iter)->arduino_unit_id == unit) {
			return *iter;
		}
	}

	return NULL;
}

