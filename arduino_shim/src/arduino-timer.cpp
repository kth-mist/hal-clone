/**
 * @file      arduino-timer.cpp
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs for the arduino-timer library
 */

extern "C" {
	#include <freertos/FreeRTOS.h>
	#include <freertos/timers.h>

	#include <Arduino.h>

	#include <hal-clone/arduino.h>
}

#include <arduino-timer.h>

static void arduino_shim_timer_start(
	int _unit,
	struct timer_task *_timers,
	unsigned long interval,
	bool (*opaque)(void *),
	unsigned portBASE_TYPE repeat
);

static void timer_cb(xTimerHandle handle)
{
	if (handle == NULL) {
		HAL_CLONE_ARDUINO_WARNING("Timer handle was NULL!");
		return;
	}
	struct timer_task *timer = (struct timer_task *) pvTimerGetTimerID(handle);
	if (timer == NULL || timer->opaque == NULL) {
		HAL_CLONE_ARDUINO_WARNING("timer task or callback was NULL!");
		return;
	}

	// Call the timer callback and terminate the timer if it returns false
	if (!timer->opaque(NULL)) {
		xTimerStop(handle, 0 );
	}
}

Timer<> timer_create_default(void)
{
	return Timer<>();
}

void arduino_shim_timer_tick(int _unit, struct timer_task *_timers)
{
	(void) _unit;
	(void) _timers;

	// Do nothing since FreeRTOS does not require the use of a function like this
}

void arduino_shim_timer_every(int _unit, struct timer_task *_timers, unsigned long interval, bool (*opaque)(void *))
{
	arduino_shim_timer_start(_unit, _timers, interval, opaque, pdTRUE);
}

void arduino_shim_timer_in(int _unit, struct timer_task *_timers, unsigned long delay, bool (*opaque)(void *))
{
	arduino_shim_timer_start(_unit, _timers, delay, opaque, pdFALSE);
}

void arduino_shim_timer_cancel(int _unit, struct timer_task *_timers)
{
	for (int i = 0; i < TIMER_MAX_TASKS; i++) {
		if (_timers[i].occupied && _timers[i].arduino_unit_id == _unit) {
			xTimerStop(_timers[i].timer, 0);
			_timers[i].occupied = false;
		}
	}
}

static void arduino_shim_timer_start(
	int _unit,
	struct timer_task *_timers,
	unsigned long interval,
	bool (*opaque)(void *),
	unsigned portBASE_TYPE repeat
)
{
	for (int i = 0; i < TIMER_MAX_TASKS; i++) {
		if (!_timers[i].occupied) {
			_timers[i].timer = xTimerCreate(
			    (const signed char *) "Timer",
			    interval / portTICK_RATE_MS,
			    repeat,
			    (void*) &_timers[i],
			    timer_cb
			);

			if (_timers[i].timer == NULL) {
				HAL_CLONE_ARDUINO_WARNING("The timer was not created.");
			} else if (xTimerStart(_timers[i].timer, 0) != pdPASS) {
				HAL_CLONE_ARDUINO_WARNING("The timer could not be set into the Active state.");
			}
			_timers[i].opaque = opaque;
			_timers[i].arduino_unit_id = _unit;
			_timers[i].occupied = true;
			return;
		}
	}

	HAL_CLONE_ARDUINO_WARNING("No more timers can be started.");
}
