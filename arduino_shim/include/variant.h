/**
 * @file      variant.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs used by any Arduino-based peripheral
 *            simulators.
 */

#ifndef VARIANT_H_
#define VARIANT_H_

#define WIRE_INTERFACES_COUNT    1

#endif /* VARIANT_H_ */
