/**
 * @file      rstc.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio reset stubs used by any Arduino-based peripheral
 *            simulators.
 */

#ifndef ARDUINO_RSTC_H_
#define ARDUINO_RSTC_H_

#define RSTC 0

#ifdef __cplusplus
extern "C" {
#endif

#define rstc_start_software_reset(base)  hal_clone_arduino_rstc_reset(ARDUINO_UNIT_ID, base)
void hal_clone_arduino_rstc_reset(int _unit, int base);

#ifdef __cplusplus
}
#endif

#endif /* ARDUINO_RSTC_H_ */
