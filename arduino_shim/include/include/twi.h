/**
 * @file      twi.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs used by any Arduino-based peripheral
 *            simulators.
 * @note      These are all empty or do nothing. Use the iOBC's I2C
 *            stub to register callbacks for your Arduino's I2C handlers
 *            instead.
 */

#ifndef TWI_H_
#define TWI_H_

#include <stdio.h>

#define WIRE_INTERFACE              (void*)0
#define WIRE_INTERFACE_ID           0
#define WIRE_ISR_ID                 0

#define PIN_WIRE_SDA                0
#define PIN_WIRE_SCL                0

#define TWI_PTCR_RXTDIS             0
#define TWI_PTCR_TXTDIS             0
#define TWI_IDR_TXCOMP              0
#define TWI_IDR_RXRDY               0
#define TWI_IDR_GACC                0
#define TWI_IDR_NACK                0
#define TWI_IDR_SCL_WS              0
#define TWI_IDR_EOSACC              0
#define TWI_IER_SVACC               0

#define TWI_STATUS_SVACC(x)         1
#define TWI_STATUS_SVREAD(x)        1
#define TWI_STATUS_EOSACC(x)        1
#define TWI_STATUS_RXRDY(x)         1
#define TWI_STATUS_TXRDY(x)         1
#define TWI_STATUS_NACK(x)          1

#define TWI_DisableIt(a, b)         printf("%s", "")
#define TWI_EnableIt(a, b)          printf("%s", "")
#define TWI_ConfigureSlave(a, b)    printf("%s", "")
#define TWI_GetStatus(x)            printf("%s", "")
#define TWI_WriteByte(a, b)         printf("%s", "")
#define TWI_ReadByte(x)             printf("%s", "")

#define NVIC_EnableIRQ(x)           printf("%s", "")
#define NVIC_DisableIRQ(x)          printf("%s", "")
#define NVIC_ClearPendingIRQ(x)     printf("%s", "")
#define NVIC_SetPriority(a, b)      printf("%s", "")

#define pmc_enable_periph_clk(x)    printf("%s", "")
#define pmc_disable_periph_clk(x)   printf("%s", "")

#define PIO_Configure(a, b, c, d)   printf("%s", "")

typedef struct {
	int TWI_PTCR;
} Twi;

typedef struct {
	int pPort;
	int ulPinType;
	int ulPin;
	int ulPinConfiguration;
} PinConfig;

PinConfig g_APinDescription[1] = {
	{0, 0, 0, 0}
};

#endif /* TWI_H_ */
