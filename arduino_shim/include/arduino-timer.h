/**
 * @file      arduino-timer.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs for the arduino-timer library
 * @details Limitations:
 *          - Only Timer<> is supported.
 *          - Timer::Task is not supported, every(), in(), and cancel() return void.
 *          - It is not possible to specify an argument to the opaque function.
 *          - cancel() stops all timers in the local Arduino unit.
 */

#ifndef ARDUINO_TIMER_H_
#define ARDUINO_TIMER_H_

#include <Arduino.h>

extern "C" {
	#include <freertos/FreeRTOS.h>
	#include <freertos/timers.h>
}

#define TIMER_MAX_TASKS  (10)

struct timer_task {
	xTimerHandle timer;
	bool (*opaque)(void *);
	int arduino_unit_id;
	bool occupied;
};

void arduino_shim_timer_tick(int _unit, struct timer_task *_timers);
void arduino_shim_timer_every(int _unit, struct timer_task *_timers, unsigned long interval, bool (*opaque)(void *));
void arduino_shim_timer_in(int _unit, struct timer_task *_timers, unsigned long delay, bool (*opaque)(void *));
void arduino_shim_timer_cancel(int _unit, struct timer_task *_timers);

template <typename T = void> class Timer {
public:
	Timer()
	{
		for (int i = 0; i < TIMER_MAX_TASKS; i++) {
			_timers[i].occupied = false;
		}
	}

	~Timer() {}

	void tick(int unit)
	{
		arduino_shim_timer_tick(unit, _timers);
	}

	void every(int unit, unsigned long interval, bool (*opaque)(void *))
	{
		arduino_shim_timer_every(unit, _timers, interval, opaque);
	}

	void in(int unit, unsigned long delay, bool (*opaque)(void *))
	{
		arduino_shim_timer_in(unit, _timers, delay, opaque);
	}

	void cancel(int unit)
	{
		arduino_shim_timer_cancel(unit, _timers);
	}

private:
	struct timer_task _timers[TIMER_MAX_TASKS];
};

#define tick()                  tick(ARDUINO_UNIT_ID)
#define every(interval, opaque) every(ARDUINO_UNIT_ID, interval, opaque)
#define in(delay, opaque)       in(ARDUINO_UNIT_ID, delay, opaque)
#define cancel()                cancel(ARDUINO_UNIT_ID)

Timer<> timer_create_default(void);

#endif /* ARDUINO_TIMER_H_ */
