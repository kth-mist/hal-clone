/**
 * @file      Arduino.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs used by any Arduino-based peripheral
 *            simulators.
 */

#ifndef MIST_HAL_CLONE_ARDUINO_H_
#define MIST_HAL_CLONE_ARDUINO_H_

#include <Arduino.h>

#include <hal-clone/common/gpio.h>

/* @brief Identifier for Arduino kind of GPIOs, used by the HAL clone to differentiate
          it between other GPIO kinds. */
#define HAL_CLONE_GPIO_KIND_ARDUINO   (42)

#ifdef __cplusplus
extern "C" {
#endif

struct hal_clone_gpio hal_clone_arduino_pin_to_gpio(int unit, int pin);
void hal_clone_arduino_set_output_isr(int unit, int pin, bool (*isr)(void));

void hal_clone_arduino_rstc_regsiter_cb(int unit, void (*reset)(void));
void hal_clone_arduino_DueFlashStorage_set_semihosting_file(int unit, const char *path, uint32_t base_address, size_t size);
void hal_clone_arduino_DueFlashStorage_set_in_memory(int unit, uint8_t *memory, uint32_t base_address, size_t size);
void hal_clone_arduino_DueFlashStorage_erase(int unit);

#define HAL_CLONE_ARDUINO_WARNING(...) hal_clone_arduino_warning(__FILE__, __LINE__, __VA_ARGS__)
void hal_clone_arduino_warning(const char *file, int line, const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif /* MIST_HAL_CLONE_ARDUINO_H_ */
