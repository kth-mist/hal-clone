/**
 * @file      Arduino.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs used by any Arduino-based peripheral
 *            simulators.
 */

#ifndef ARDUINO_H_
#define ARDUINO_H_

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
// TODO Implement return values
class ArduinoSerial {
public:
	ArduinoSerial() {}

	void _begin(int _unit, int baud)
	{
		(void) _unit;
		(void) baud;
	}

	void _print(int _unit, bool newline, int x)
	{
		(void) _unit;

		fprintf(stderr, "%d", x);

		if (newline) {
			fputs("\r\n", stderr);
		}
	}

	void _print(int _unit, bool newline, unsigned int x)
	{
		(void) _unit;

		fprintf(stderr, "%u", x);

		if (newline) {
			fputs("\r\n", stderr);
		}
	}

	void _print(int _unit, bool newline, char x)
	{
		(void) _unit;

		fprintf(stderr, "%c", x);

		if (newline) {
			fputs("\r\n", stderr);
		}
	}

	void _print(int _unit, bool newline, double x)
	{
		(void) _unit;

		fprintf(stderr, "%f", x);

		if (newline) {
			fputs("\r\n", stderr);
		}
	}

	void _print(int _unit, bool newline, const char *x)
	{
		(void) _unit;

		fputs(x, stderr);

		if (newline) {
			fputs("\r\n", stderr);
		}
	}

	void _flush(int _unit) { (void) _unit; }
	void _end(int _unit) {  (void) _unit; }
};
#define begin(baud)  _begin(ARDUINO_UNIT_ID, baud)
#define print(x)     _print(ARDUINO_UNIT_ID, false, x)
#define println(x)   _print(ARDUINO_UNIT_ID, true, x)
#define flush()      _flush(ARDUINO_UNIT_ID)
#define end()        _end(ARDUINO_UNIT_ID)

extern ArduinoSerial Serial;

#else /* __cplusplus */
// Stub C++ bool type
typedef unsigned char bool;
#define true     1
#define false    0

// No Serial is provided for C files, C files that need Serial should be converted to C++
#endif /* __cplusplus */

#define HIGH     1
#define LOW      0

#define INPUT         0
#define OUTPUT        1
#define INPUT_PULLUP  2

#define FALLING  3
#define RISING   4
#define CHANGE   5

#define pinMode(pin, mode)        arduino_shim_pin_mode(ARDUINO_UNIT_ID, pin, mode)
#define digitalWrite(pin, value)  arduino_shim_digital_write(ARDUINO_UNIT_ID, pin, value)
#define digitalRead(pin)          arduino_shim_digital_read(ARDUINO_UNIT_ID, pin)

#define attachInterrupt(interrupt, isr, mode)  arduino_shim_attach_interrupt(ARDUINO_UNIT_ID, interrupt, isr, mode)
#define digitalPinToInterrupt(pin)             arduino_shim_digital_pin_to_interrupt(ARDUINO_UNIT_ID, pin)

#ifdef __cplusplus
extern "C" {
#endif

unsigned long millis(void);

void arduino_shim_pin_mode(int _unit, int pin, unsigned char mode);
void arduino_shim_digital_write(int _unit, int pin, bool value);
bool arduino_shim_digital_read(int _unit, int pin);

void arduino_shim_attach_interrupt(int _unit, int interrupt, void (*isr)(void), unsigned char mode);
int arduino_shim_digital_pin_to_interrupt(int _unit, int pin);

#ifdef __cplusplus
}
#endif

#endif /* ARDUINO_H_ */
