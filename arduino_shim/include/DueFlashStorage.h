/**
 * @file      DueFlashStorage.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Ardunio stubs for the DueFlashStorage library
 * @details   The DueFlashLibrary does not allow the caller to read several bytes of data at once. Instead, it breaks
 *            abstraction by providing the user with a pointer to memory where the data is stored with the readAddress()
 *            function. Because this behavior is not possible to emulate using semihosting, this shim reads in the
 *            entire semihosting file using malloc, and returns the address of the requested data.
 *            Because the caller will not free this memory, this shim frees it automatically once the caller calls
 *            readAddress() another time or when writing to flash, meaning that the caller must not use the pointer
 *            afterwards.
 */

#ifndef ARDUINO_DUE_FLASH_STORAGE_H_
#define ARDUINO_DUE_FLASH_STORAGE_H_

#include <stdio.h>
#include <stdint.h>

// See section "Memories" in
// https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-11057-32-bit-Cortex-M3-Microcontroller-SAM3X-SAM3A_Datasheet.pdf
#define IFLASH0_ADDR      0x00080000
#define IFLASH0_SIZE      0x00040000
#define IFLASH0_PAGE_SIZE 256
#define IFLASH1_ADDR      0x000C0000
#define IFLASH1_SIZE      0x00040000
#define IFLASH1_PAGE_SIZE 256

#define FLASH_START  IFLASH1_ADDR

class DueFlashStorage {
public:
	DueFlashStorage() {}

	uint8_t _read(int unit, uint32_t address);
	uint8_t *_readAddress(int unit, uint32_t address);
	bool _write(int unit, uint32_t address, uint8_t value);
	bool _write(int unit, uint32_t address, uint8_t *data, uint32_t len);
};

#define read(address)                      _read(ARDUINO_UNIT_ID, address)
#define readAddress(address)               _readAddress(ARDUINO_UNIT_ID, address)
#define write(address, ...)                _write_selector(address, __VA_ARGS__, multi, single)
#define write_unlocked(address, ...)       _write_selector(address, __VA_ARGS__, multi, single)

// Workaround for defining a macro with a variable number of arguments.
// If only a value should be written, then the unused arg2 becomes "multi" and the selector becomes "single"
// If multiple values should be written, then arg2 becomes the length of the data to write, the selector becomes "multi",
// and the "single" argument becaomse discarded in the ellipsis.
//
// Inspired by https://stackoverflow.com/a/36669449
#define _write_selector(address, arg1, arg2, selector, ...) _write_##selector(address, arg1, arg2)
#define _write_single(address, value, _unused)              _write(ARDUINO_UNIT_ID, address, value)
#define _write_multi(address, data, len)                    _write(ARDUINO_UNIT_ID, address, data, len)

#endif /* ARDUINO_DUE_FLASH_STORAGE_H_ */
