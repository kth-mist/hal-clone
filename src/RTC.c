/**
 * @file      RTC.c
 * @author    Calin Capitanu
 * @copyright MIT License
 * @brief     "hal/Timing/RTC.h" stub implementations
 *
 * @note Many of the functions here are not properly implemented, and some functions
 *       may be missing.
 *       It is not difficult to break any of these functions, so just because the
 *       program passes/fails these tests does not necessarily mean that it the same
 *       will happen when running the code on the iOBC.
 */

#include <hal/Timing/Time.h>
#include "macros.h"

int RTC_start()
{
	return 0;
}

int RTC_stop()
{
	PRINT_UNSUPPORTED("RTC_stop is unsupported");
	return 0;
}

int RTC_setTime(Time *time)
{
	PRINT_UNSUPPORTED("RTC_setTime is unsupported");
	return 0;
}

int RTC_getTime(Time *time)
{
	PRINT_UNSUPPORTED("RTC_getTime is unsupported");
	return 0;
}

int RTC_testGetSet()
{
	PRINT_UNSUPPORTED("RTC_testGetSet is unsupported");
	return 0;
}

int RTC_testSeconds()
{
	PRINT_UNSUPPORTED("RTC_testSeconds is unsupported");
	return 0;
}

void RTC_printSeconds()
{
	PRINT_UNSUPPORTED("RTC_printSeconds is unsupported");
}

int RTC_checkTimeValid(Time *time)
{
	PRINT_UNSUPPORTED("RTC_checkTimeValid is unsupported");
	return 0;
}

int RTC_getTemperature(float *temperature)
{
	*temperature = 27.5;
	return 0;
}
