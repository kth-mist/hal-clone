/**
 * @file      WatchDogTimer.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "hal/Timing/WatchDogTimer.h" stub implementations
 *
 * @note These are all unsupported for now since the watchdog has
 *       little use from a testing perspective.
 */

#include <hal/Timing/WatchDogTimer.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "macros.h"


void WDT_start()
{
	return;
}

void WDT_kickEveryNcalls(unsigned int N)
{
	PRINT_UNSUPPORTED("Watchdog is unsupported");
}

void WDT_forceKick(void)
{
	PRINT_UNSUPPORTED("Watchdog is unsupported");
}

void WDT_forceKickEveryNms(portTickType N)
{
	// Called by the solar panel v2 library while it is waiting for a reply
	return;
}

int WDT_startWatchdogKickTask(portTickType kickInterval, Boolean toggleLed1)
{
	PRINT_UNSUPPORTED("Watchdog is unsupported");
	return 0;
}

void WDT_stopWatchdogKickTask(void)
{
	PRINT_UNSUPPORTED("Watchdog is unsupported");
}
