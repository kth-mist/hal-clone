/**
 * @file      Time.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "hal/Timing/Time.h" stub implementations
 */

#include <stdio.h>
#include <time.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>

#include <hal/boolean.h>
#include <hal/Timing/Time.h>
#include "macros.h"

static volatile unsigned int rtc = UNIX_TIME_AT_Y2K;
static xSemaphoreHandle rtcSemaphore = NULL;

/**
 * Corresponds to 2099-12-31 23:59:59.
 * 2100 and beyond has been confirmed to not be supported.
 */
const unsigned int MAX_EPOCH = 0xF48656FF;
const portTickType spin = 5 / portTICK_RATE_MS;

static Boolean is_init = FALSE;
static unsigned int sync_interval = 0;

/**
 * @brief Task that simulates the time sync and the RTC itself
 */
static void rtc_task_procedure(void *args)
{
	unsigned int i;
	const portTickType delay = 1000 / portTICK_RATE_MS;

	for (;;) {
		for (i = 0; sync_interval == 0 || i < sync_interval; i++) {
			vTaskDelay(delay);

			// Interesting side note: There seems to be a bug in the ISIS HAL RTC.
			// If the RTC is 2099-12-31 23:59:59, the next time it ticks the time
			// will be 2101-01-01 00:00:00, skipping 2100 altogether. But 2101 is not
			// even a valid year in the first place, as attempting to set the RTC
			// to that year will result in an error. This HAL clone RTC will however
			// stop ticking once the maximum RTC value is reached.
			if (rtc < MAX_EPOCH && xSemaphoreTake(rtcSemaphore, spin) == pdTRUE) {
				rtc++;
				xSemaphoreGive(rtcSemaphore);
			}
		}
		Time_syncIfNeeded();
	}
}

int Time_start(Time *time, unsigned int syncInterval)
{
	int err;
	portBASE_TYPE res;
	xTaskHandle rtc_task;

	portBASE_TYPE state = xTaskGetSchedulerState();
	if (state != taskSCHEDULER_RUNNING) {
		return 3;
	}
	if (rtcSemaphore == NULL) {
		rtcSemaphore = xSemaphoreCreateMutex();
	}

	if (time == NULL) {
		// We must initialize the RTC variable here since
		// there is no RTT that it can be synced with.
		err = Time_setUnixEpoch(UNIX_TIME_AT_Y2K);
		if (err)
			return 2;
	}
	else {
		err = Time_set(time);
		if (err)
			return 2;
	}

	Time_setSyncInterval(syncInterval);
	res = xTaskCreate(rtc_task_procedure,
	                  (const signed char*)"rtc_task",
	                  512,
	                  (void*)&syncInterval,
	                  tskIDLE_PRIORITY,
	                  &rtc_task);
	if (res != pdPASS) {
		// Since the task acts as an RTC simulator,
		// if it fails to run it is the same as the
		// RTC not functioning.
		return 0xFF00;
	}
	is_init = TRUE;
	return 0;
}

int Time_set(Time *time)
{
	int err;
	struct tm tm_str;
	unsigned int new;

	if (time == NULL)
		return 1;

	if (time->seconds > 59 ||
	    time->minutes > 59 ||
	    time->hours > 23 ||
	    time->date == 0 ||
	    time->day == 0 ||
	    time->day > 7 ||
	    time->month == 0 ||
	    time->month > 12 ||
	    time->year > 99) {
		return 1;
	}

	tm_str.tm_sec = time->seconds;
	tm_str.tm_min = time->minutes;
	tm_str.tm_hour = time->hours;
	tm_str.tm_mday = time->date;
	tm_str.tm_mon = time->month - 1;
	tm_str.tm_year = time->year + 100;
	tm_str.tm_isdst = -1;

	new = (unsigned int)mktime(&tm_str);
	err = Time_setUnixEpoch(new);
	return err;
}

int Time_setUnixEpoch(unsigned int epochTime)
{
	if (epochTime < UNIX_TIME_AT_Y2K || epochTime > MAX_EPOCH)
		return 1;

	if (xSemaphoreTake(rtcSemaphore, spin) == pdTRUE) {
		rtc = epochTime;
		xSemaphoreGive(rtcSemaphore);
	} else {
		// Should never end up here or the rtc task is not letting go of the mutex.
		return 1;
	}
	return 0;
}

int Time_get(Time *time)
{
	if (!is_init || time == NULL)
		return 1;

	unsigned int new;
	struct tm *tm_str;

	// The ISIS HAL seems to return the current time added by one second.
	// Note that it could be the case that it sets the current time subtracted
	// by one second instead.
	if (rtc < MAX_EPOCH)
		new = rtc + 1;
	else
		new = rtc;

	tm_str = localtime((time_t*) &new);

	time->seconds = (unsigned char)tm_str->tm_sec;
	time->minutes = (unsigned char)tm_str->tm_min;
	time->hours = (unsigned char)tm_str->tm_hour;
	time->day = (unsigned char)(tm_str->tm_wday + 1);
	time->date = (unsigned char)tm_str->tm_mday;
	time->month = (unsigned char)(tm_str->tm_mon + 1);
	time->year = (unsigned char)(tm_str->tm_year - 100);
	time->secondsOfYear = (unsigned int)(tm_str->tm_yday * 86400 +
	                                     tm_str->tm_hour * 3600 +
	                                     tm_str->tm_min * 60 +
	                                     tm_str->tm_sec);
	return 0;
}


unsigned int Time_getUptimeSeconds(void)
{
	return xTaskGetTickCount() / configTICK_RATE_HZ;
}

int Time_getUnixEpoch(unsigned int *epochTime)
{
	if (!is_init || epochTime == NULL)
		return 1;

	// The ISIS HAL seems to return the current time added by one second.
	// Note that it could be the case that it sets the current time subtracted
	// by one second instead.
	if (rtc < MAX_EPOCH)
		*epochTime = rtc + 1;
	else
		*epochTime = rtc;
	return 0;
}

/*!
 * Does nothing since there is no RTT.
 */
int Time_sync(void)
{
	fprintf(stderr, "-D- Time sync done \n");
	return 0;
}

int Time_syncIfNeeded(void)
{
	return Time_sync();
}

void Time_setSyncInterval(unsigned int seconds)
{
	sync_interval = seconds;
}


Boolean Time_isLeapYear(unsigned int year)
{
	if (year % 4 == 0)
		return TRUE;

	return FALSE;
}

unsigned int Time_diff(Time *newTime, Time *oldTime)
{
	if (newTime == NULL || oldTime == NULL ||
	    newTime->year < oldTime->year ||
	    (newTime->year == oldTime->year &&
	    newTime->secondsOfYear < oldTime->secondsOfYear)) {
		return TIMEDIFF_INVALID;
	}
	unsigned int days_until_old_year = 365 * oldTime->year +
	                                   ((oldTime->year + 3) >> 2);
	unsigned int days_until_new_year = 365 * newTime->year +
	                                   ((newTime->year + 3) >> 2);
	return days_until_new_year * 86400 + newTime->secondsOfYear -
	       days_until_old_year * 86400 - oldTime->secondsOfYear;
}

Boolean Time_isRTCworking(void)
{
	return TRUE;
}
