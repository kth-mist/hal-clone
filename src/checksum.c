/**
 * @file      checksum.c
 * @author    John Wikman
 * @copyright MIT License
 * @brief     "hal/checksum.h" stub implementations
 */

#include <hal/boolean.h>
#include <hal/checksum.h>
#include "macros.h"

/**
 * Not sure if this is 100% correct, but then again it does not matter in this case
 */
void checksum_prepareLUTCRC8(unsigned char polynomial, unsigned char* LUT)
{
	int i, j;
	unsigned char rem;

	for (i = 0; i < 256; i++) {
		rem = i;  /* remainder from polynomial division */
		for (j = 0; j < 8; j++) {
			if (rem & 1) {
				rem >>= 1;
				rem ^= polynomial;
			} else {
				rem >>= 1;
			}
		}
		LUT[i] = rem;
	}
}

/**
 * Again, this implementation is probably incorrect. But it does not matter since
 * all we care about is that the output of 2 identical calls are the same.
 */
unsigned char checksum_calculateCRC8LUT(unsigned char* data, unsigned int length, unsigned char* LUT, unsigned char start_remainder, Boolean endofdata)
{
	(void) endofdata;

	unsigned int i;
	unsigned char crc = ~start_remainder;

	for (i = 0; i < length; i++)
		crc = LUT[data[i] ^ (crc & 0xff)];

	return ~crc;
}

