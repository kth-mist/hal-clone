/**
 * @file      macros.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Contains macros used by all stub implementations
 */

#ifndef MACROS_H_
#define MACROS_H_

#include <stdio.h>

/**
 * @brief Used to notify that an unexpected error has occured, and that is not
 *        able to be handled in a suitable way by returing any error code
 *        defined by the interface.
 */
#define PRINT_UNEXPECTED(err)    fprintf(stderr, "%s:%d UNEXPECTED ERROR: %d\n", __func__, __LINE__, err)

/**
 * @brief Used to notify if certain behaviour that is expected in the actual
 *        implementation is not supported by the stub implementation.
 */
#define PRINT_UNSUPPORTED(msg)        fprintf(stderr, "%s:%d UNSUPPORTED ERROR: %s\n", __func__, __LINE__, msg)

/**
 * @brief Used to notify if an unimplemented function has been called.
 */
#define PRINT_UNIMPLEMENTED           fprintf(stderr, "%s:%d UNIMPLEMENTED FUNCTION: %s\n", __FILE__, __LINE__, __func__)

#endif /* MACROS_H_ */
