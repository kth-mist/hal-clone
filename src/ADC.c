/**
 * @file      ADC.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "hal/Drivers/ADC.h" stub implementations
 */

#include <stdlib.h>
#include <string.h>

#include <hal/boolean.h>
#include <hal/Drivers/ADC.h>

#include <hal-clone/adc.h>
#include <hal-clone/error.h>

#include "macros.h"


static unsigned short rand_adc_sample(void);
static void dummy_cb(SystemContext context, void* adcSamples);

static ADCparameters adc_params = {0};
static char is_init = 0;

static unsigned short adc_channels[8];
static unsigned char adc_channels_valid[8];

unsigned int ADC_ConvertRaw10bitToMillivolt(unsigned short adcRawSample)
{
	return ((ADC_REFERENCE_VOLTAGE * (unsigned int)adcRawSample) / 0x3FF);
}

unsigned int ADC_ConvertRaw8bitToMillivolt(unsigned char adcRawSample)
{
	return ((ADC_REFERENCE_VOLTAGE * (unsigned int)adcRawSample) / 0xFF);
}

int ADC_SingleShot(unsigned short adcSamples[8])
{
	ADCparameters init_params;

	if (adcSamples == NULL)
		return -2;

	if (!is_init) {
		init_params.sampleRate = 32000; // Doesn't matter what this value is really
		init_params.resolution10bit = TRUE;
		init_params.channels = 8;
		init_params.samplesInBufferPerChannel = 2048;
		init_params.callback = dummy_cb;
		ADC_start(init_params);
	}

	for (unsigned int i = 0; i < adc_params.channels; i++) {
		adcSamples[i] = adc_channels_valid[i] ? adc_channels[i] : rand_adc_sample();
	}

	return 0;
}

int ADC_start(ADCparameters params)
{
	if (params.sampleRate < 4030 ||
	    (params.resolution10bit && (params.sampleRate > 75000 || params.samplesInBufferPerChannel * params.channels > 32768)) ||
	    (!params.resolution10bit && (params.sampleRate > 250000 || params.samplesInBufferPerChannel * params.channels > 65536)) ||
	    params.channels == 0 ||
	    (params.channels > 4 && params.channels < 8) ||
	    params.channels > 8 ||
	    params.callback == NULL)
		return -2;

	memset(adc_channels_valid, 0, sizeof(adc_channels_valid));
	memcpy(&adc_params, &params, sizeof(ADCparameters));
	is_init = 1;
	return 0;
}

void ADC_stop(void)
{
	memset(&adc_params, 0, sizeof(ADCparameters));
	is_init = 0;
}

/* Functions not part of the driver */

/**
 * @brief Set the given value that should be returned by ADC_SingleShot at the given channel.
 *        Initializes the ADC driver with default parameters if it is not already initialized.
 *
 * @param[in] channel The channel to set.
 * @param[in] val The raw value to set the channel to.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int hal_clone_set_adc(int channel, unsigned short val)
{
	unsigned short sample_buf[8];

	if (!is_init)
		ADC_SingleShot(sample_buf);

	if (channel < 0 || channel >= (int) adc_params.channels)
		return HAL_CLONE_ERR_OOB;

	if (val > (adc_params.resolution10bit ? 0x3FF : 0xFF))
		return HAL_CLONE_ERR_OOB;

	adc_channels[channel] = val;
	adc_channels_valid[channel] = 1;

	return 0;
}

/**
 * @brief Set the given values that should be returned by ADC_SingleShot at all channels,
 *        Initializes the ADC driver with default parameters if it is not already initialized.
 *
 * @param[in] vals Pointer to adc_params.channels number of unsigned short values to set.
 *
 * @return 0 on success.
 *         1 if vals was NULL.
 *         2 if any of the values were too large.
 */
int hal_clone_set_adc_all(unsigned short *vals)
{
	unsigned short sample_buf[8];
	unsigned int i;

	if (!is_init)
		ADC_SingleShot(sample_buf);

	if (vals == NULL)
		return HAL_CLONE_ERR_NULL;

	for (i = 0; i < adc_params.channels; i++) {
		if (vals[i] > (adc_params.resolution10bit ? 0x3FF : 0xFF))
			return HAL_CLONE_ERR_OOB;
	}

	for (i = 0; i < adc_params.channels; i++) {
		adc_channels[i] = vals[i];
		adc_channels_valid[i] = 1;
	}

	return 0;
}

static unsigned short rand_adc_sample(void)
{
	//TODO: What is a reasonable range for the ADC samples?
	return rand() & ((adc_params.resolution10bit) ? 0x3FF : 0xFF);
}

static void dummy_cb(SystemContext context, void* adcSamples)
{
	return; // Do nothing...
}
