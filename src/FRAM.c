/**
 * @file      FRAM.c
 * @author    John Wikman
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "hal/Storage/FRAM.h" stub implementations
 */

#include <stdlib.h>
#include <string.h>

#include <hal/Storage/FRAM.h>

#include <hal-clone/storage.h>
#include <hal-clone/semihosting_memory.h>

#include "macros.h"

static const char *_fram_semihost_file = NULL;
static int _fd = -1;

static unsigned char *_fram_loc = NULL;

int FRAM_start(void)
{
	if (_fram_semihost_file != NULL) {
		// TODO: Perhaps the initial value should be 0xFF instead of 0x00
		_fd = semihosting_memory_open(_fram_semihost_file, FRAM_MAX_ADDRESS + 1, 0x00);
		if (_fd < 1) {
			return -1;
		}
	} else if (_fram_loc == NULL) {
		return -1;
	}

	return 0;
}

void FRAM_stop(void)
{
	if (_fd >= 0) {
		semihosting_memory_close(_fd);
	}
}

int FRAM_write(unsigned char *data, unsigned int address, unsigned int size)
{
	if ((address + size) > FRAM_MAX_ADDRESS + 1)
		return -2;

	if (_fd >= 0) {
		int err = semihosting_memory_write(_fd,
		                                   (off_t) address,
		                                   (const uint8_t*) data,
		                                   (size_t) size);
		if (err != 0) {
			return -1;
		}
	} else if (_fram_loc != NULL) {
		memcpy(&_fram_loc[address], data, size);
	} else {
		return -1;
	}

	return 0;
}

int FRAM_read(unsigned char *data, unsigned int address, unsigned int size)
{
	if ((address + size) > FRAM_MAX_ADDRESS + 1)
		return -2;

	if (_fd >= 0) {
		int err = semihosting_memory_read(_fd,
		                                  (off_t) address,
		                                  (uint8_t*) data,
		                                  (size_t) size);
		if (err != 0) {
			return -1;
		}
	} else if (_fram_loc != NULL) {
		memcpy(data, &_fram_loc[address], size);
	} else {
		return -1;
	}

	return 0;
}

int FRAM_writeAndVerify(unsigned char *data, unsigned int address, unsigned int size)
{
	return FRAM_write(data, address, size);
}

int FRAM_protectBlocks(FRAMblockProtect blocks)
{
	PRINT_UNIMPLEMENTED;
	return 0;
}

int FRAM_getProtectedBlocks(FRAMblockProtect* blocks)
{
	PRINT_UNIMPLEMENTED;
	return 0;
}

int FRAM_getDeviceID(unsigned char *deviceID)
{
	deviceID[0] = 0x7f;
	deviceID[1] = 0x7f;
	deviceID[2] = 0x7f;
	deviceID[3] = 0x7f;
	deviceID[4] = 0x7f;
	deviceID[5] = 0x7f;
	deviceID[6] = 0xc2;
	deviceID[7] = 0x25;
	deviceID[8] = 0x08;

	return 0;
}

unsigned int FRAM_getMaxAddress(void)
{
	return FRAM_MAX_ADDRESS;
}

/* Functions not part of the driver */

/**
 * @brief Sets a path to a file on the host that should be used as the FRAM memory with semihosting.
 *
 * @param[in] path The path to the file on the host.
 */
void hal_clone_fram_set_semihosting_file(const char *path)
{
	_fram_semihost_file = path;
}

/**
 * @brief Sets a memory location that should be used as the FRAM memory.
 *
 * @note If the semihost file is set, this function will not have any effect.
 *
 * @param[in] memory The memory location to use.
 *                   The memory MUST be at least FRAM_MAX_ADDRESS + 1 bytes.
 */
void hal_clone_set_fram_in_memory(unsigned char *memory)
{
	_fram_loc = memory;
}
