/**
 * @file      hcc.c
 * @author    John Wikman
 * @author    William Stackenäs
 * @author    Calin Capitanu
 * @copyright MIT License
 * @brief     "hcc/" stub implementations
 *
 * @note Many of the functions here are not properly implemented, and some functions
 *       may be missing.
 *       It is not difficult to break any of these functions, so just because the
 *       program passes/fails these tests does not necessarily mean that it the same
 *       will happen when running the code on the iOBC.
 */

#ifdef USE_HAL_CLONE_HCC_STUBS

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <search.h>
#include <sys/queue.h>

#include <hcc/api_fat.h>
#include <hcc/api_fs_err.h>
#include <hcc/api_hcc_mem.h>
#include "macros.h"

struct fs_entry {
	char fullname[F_MAXPATHNAME];
	FN_STAT stat;
	void *data;

	size_t current_offset;

	long entryid;
	TAILQ_ENTRY(fs_entry) entries;
};
TAILQ_HEAD(fs_entry_head, fs_entry); // Declares "struct fs_entry_head"

static char _curdir[F_MAXPATHNAME];
static struct fs_entry_head _head;
static int _drivenum = -1;

static long next_id(void)
{
	static long id = 0;

	id = id + 1;

	return id;
}

static void check_and_init_hcc(void)
{
	static int is_initialized = 0;
	if (is_initialized)
		return;

	snprintf(_curdir, sizeof(_curdir), "/");

	TAILQ_INIT(&_head);

	is_initialized = 1;
}

static char *getfullname(const char *filename)
{
	static char fullname[F_MAXPATHNAME];

	if (filename[0] == '/') {
		strcpy(fullname, filename);
	} else if (strcmp(_curdir, "/") == 0) {
		snprintf(fullname, sizeof(fullname), "/%s", filename);
	} else {
		int ret = snprintf(fullname, sizeof(fullname), "%s/%s", _curdir, filename);
		assert(ret >= 0);
	}

	return fullname;
}


// ACTUAL FUNCTIONS!! //

void f_releaseFS()
{
	return;
}

int f_enterFS()
{
	return F_NO_ERROR;
}

int fn_init()
{
	return F_NO_ERROR;
}

int fsn_delete() {
	return F_NO_ERROR;
}

int hcc_mem_init() {
	return HCC_MEM_SUCCESS;
}

int hcc_mem_delete() {
	return HCC_MEM_SUCCESS;
}

F_DRIVER *atmel_mcipdc_initfunc(unsigned long driver_param) {
	return NULL;
}

int fm_initvolume(int drvnumber, F_DRIVERINIT driver_init, unsigned long driver_param)
{
	_drivenum = drvnumber;
	return F_NO_ERROR;
}

int fm_delvolume(int drvnumber)
{
	return F_NO_ERROR;
}


int fm_format(int drivenum, long fattype)
{
	if (drivenum == 1)
		return F_NO_ERROR;

	struct fs_entry *e;
	check_and_init_hcc();

	TAILQ_FOREACH(e, &_head, entries) {
		TAILQ_REMOVE(&_head, e, entries);
		free(e->data);
		free(e);
	}

	return F_NO_ERROR;
}

int fm_getfreespace(int drivenum, F_SPACE *pspace)
{
	// TODO
	pspace->total = 1;
	pspace->free = 2;
	pspace->used = 3;
	pspace->bad = 4;
	pspace->total_high = 5;
	pspace->free_high = 6;
	pspace->used_high = 7;
	pspace->bad_high = 8;

	return F_NO_ERROR;
}

int fm_get_oem(int drivenum, char *str, long len)
{
	(void) drivenum;
	strncpy(str, "HALCLONE", len - 1);
	return F_NO_ERROR;
}

int fm_getdrive()
{
	return _drivenum;
}

int fm_getc(F_FILE *filehandle)
{
	long len;
	char buf;
	len = fm_read(&buf, sizeof(char), 1, filehandle);
	if (len != 1)
		return -1;
	return buf;
}

int fm_getcwd(char *buffer, int maxlen)
{
	strncpy(buffer, _curdir, maxlen - 1);
	return F_NO_ERROR;
}

int fm_rmdir(const char *dirname)
{
	char *fullname;
	size_t fullnamelen, entrylen;
	struct fs_entry *e;

	check_and_init_hcc();

	fullname = getfullname(dirname);
	fullnamelen = strlen(fullname);

	// Hacky way of checking if the directory has any entries
	// Compare if fullpaths are equal, and entry fullname has
	// a slash where the name of the directory ends
	TAILQ_FOREACH(e, &_head, entries) {
		entrylen = strlen(e->fullname);
		if (entrylen > fullnamelen &&
		    strncmp(fullname, e->fullname, fullnamelen) == 0 &&
		    e->fullname[fullnamelen] == '/') {
			return F_ERR_NOTEMPTY;
		}
	}

	TAILQ_FOREACH(e, &_head, entries) {
		if (strcmp(fullname, e->fullname) == 0) {
			if ((e->stat.attr & F_ATTR_DIR) == 0) {
				return F_ERR_INVALIDDIR;
			}
			TAILQ_REMOVE(&_head, e, entries);
			if (e->data != NULL)
				free(e->data);
			free(e);
			return F_NO_ERROR;
		}
	}

	return F_ERR_NOTFOUND;
}

int fm_chdir(const char *dirname)
{
	check_and_init_hcc();

	if (dirname[0] != '/') {
		PRINT_UNSUPPORTED("only absolute paths allowed");
		return F_ERR_UNKNOWN;
	}

	strncpy(_curdir, dirname, sizeof(_curdir));

	return F_NO_ERROR;
}

int fm_mkdir(const char *dirname)
{
	size_t dirnamelen;
	struct fs_entry *e;

	check_and_init_hcc();

	if (dirname[0] != '/') {
		PRINT_UNSUPPORTED("only absolute paths allowed");
		return F_ERR_UNKNOWN;
	}
	dirnamelen = strlen(dirname);
	if (dirname[dirnamelen - 1] == '/') {
		PRINT_UNSUPPORTED("created directories may not have an auxilliary slash");
		return F_ERR_UNKNOWN;
	}

	TAILQ_FOREACH(e, &_head, entries) {
		if (strcmp(e->fullname, dirname) == 0) {
			if (e->stat.attr & F_ATTR_DIR)
				return F_NO_ERROR;
			else
				return F_ERR_DUPLICATED;
		}
	}

	e = malloc(sizeof(struct fs_entry));
	strncpy(e->fullname, dirname, sizeof(e->fullname));
	e->stat.attr = F_ATTR_DIR;
	e->entryid = next_id();

	TAILQ_INSERT_TAIL(&_head, e, entries); // Insert the directory entry at the back

	return F_NO_ERROR;
}

static int fm_findfromid(const char *filename, FN_FIND *find, long startid)
{
	struct fs_entry *e;
	int curdir_plus_wildcard = 0;

	check_and_init_hcc();

	if (strcmp(filename, "*.*") == 0) {
		curdir_plus_wildcard = 1; // List all files in current directory
	} else if (strchr(filename, '*') != NULL) {
		PRINT_UNSUPPORTED("wildcard is only supported if the entire string is \"*.*\"");
		return F_ERR_UNKNOWN;
	}

	TAILQ_FOREACH(e, &_head, entries) {
		char entrydir[F_MAXPATHNAME];
		char *namestart = &entrydir[1];

		// Skip entries previous to the ones we are interested in
		if (e->entryid < startid)
			continue;

		strncpy(entrydir, e->fullname, sizeof(entrydir));

		assert(entrydir[0] == '/');

		while (strchr(namestart, '/') != NULL)
			namestart = strchr(namestart, '/') + 1;

		assert(strlen(namestart) != 0);

		if (curdir_plus_wildcard) {
			namestart[-1] = '\0';
			if (strcmp(_curdir, entrydir) == 0) {
				// this item is placed in current directory, return it
				strcpy(find->filename, namestart);
				find->attr = e->stat.attr;
				strcpy(find->findfsname.path, filename);
				find->pos.pos = e->entryid;

				return F_NO_ERROR;
			}
		} else {
			if (strcmp(filename, entrydir) == 0) {
				namestart[-1] = '\0';
				strcpy(find->filename, namestart);
				find->attr = e->stat.attr;
				strcpy(find->findfsname.path, filename);
				find->pos.pos = e->entryid;

				return F_NO_ERROR;
			}
		}
	}

	return F_ERR_NOTFOUND;
}

int fm_findfirst(const char *filename, FN_FIND *find)
{
	return fm_findfromid(filename, find, 0);
}
int fm_findnext(FN_FIND *find)
{
	char buf[F_MAXPATHNAME];

	strcpy(buf, find->findfsname.path);

	return fm_findfromid(buf, find, find->pos.pos + 1);
}


FN_FILE *fm_open(const char *filename, const char *mode)
{
	char *fullname;
	struct fs_entry *e;

	check_and_init_hcc();

	fullname = getfullname(filename);

	TAILQ_FOREACH(e, &_head, entries) {
		if ((strcmp(e->fullname, fullname) == 0)) {
			// Do not open directories
			if (e->stat.attr & F_ATTR_DIR)
				return NULL;

			if (strcmp(mode, "w+") == 0 || strcmp(mode, "w") == 0) {
				// Truncate file
				e->stat.filesize = 0;
			}

			FN_FILE *file = malloc(sizeof(FN_FILE));
			file->reference = e;
			e->current_offset = 0;
			return file;
		}
	}

	// File does not exist, create it if possible!
	if (strcmp(mode, "w+") == 0 || strcmp(mode, "w") == 0 ||
	    strcmp(mode, "a+") == 0 || strcmp(mode, "a") == 0) {
		e = malloc(sizeof(struct fs_entry));
		strcpy(e->fullname, fullname);
		e->stat.attr = 0;
		e->stat.filesize = 0;
		e->data = NULL;
		e->current_offset = 0;
		e->entryid = next_id();

		TAILQ_INSERT_TAIL(&_head, e, entries);

		FN_FILE *file = malloc(sizeof(FN_FILE));
		file->reference = e;
		return file;
	} else {
		// file does not exist, and we are not allowed to create it
		return NULL;
	}
}
int fm_close(FN_FILE *filehandle)
{
	struct fs_entry *e = (struct fs_entry *) filehandle->reference;

	check_and_init_hcc();

	e->current_offset = 0;
	free(filehandle);
	return F_NO_ERROR;
}

long fm_read(void *buf, long size, long size_st, FN_FILE *filehandle)
{
	struct fs_entry *e = (struct fs_entry *) filehandle->reference;
	uint8_t *dst = (uint8_t *) buf;
	const uint8_t *src;

	if (e->data == NULL)
		return 0L;

	src = (const uint8_t *) e->data;

	check_and_init_hcc();

	if (size != sizeof(uint8_t)) {
		PRINT_UNSUPPORTED("elements to be read must be a single byte each");
		return -1;
	}

	long pos = 0L;
	long end = size_st;
	if ((end + e->current_offset) > e->stat.filesize)
		end = e->stat.filesize - e->current_offset;

	if (pos >= end)
		return 0L;

	while (pos < end) {
		dst[pos] = src[pos + e->current_offset];
		pos++;
	}

	e->current_offset += pos;
	return pos;
}

long fm_write(const void *buf, long size, long size_st, FN_FILE *filehandle)
{
	struct fs_entry *e = (struct fs_entry *) filehandle->reference;
	const uint8_t *src = (const uint8_t *) buf;

	check_and_init_hcc();

	if (size != sizeof(uint8_t)) {
		PRINT_UNSUPPORTED("elements to be written must be a single byte each");
		return -1;
	}

	if (e->data != NULL &&
	    e->stat.filesize >= sizeof(uint8_t) * (e->current_offset + size_st)) {
		memcpy(&(e->data[e->current_offset]), src, size_st);
	} else {
		// Copy any existing data into a bigger buffer
		uint8_t *dst = malloc(sizeof(uint8_t) * (e->current_offset + size_st));
		if (e->data == NULL) {
			memset(dst, 0, e->current_offset);
		} else {
			memcpy(dst, e->data, e->current_offset);
			free(e->data);
		}
		memcpy(&dst[e->current_offset], src, size_st);
		e->data = dst;
		e->stat.filesize = sizeof(uint8_t) * (e->current_offset + size_st);
	}

	e->current_offset = sizeof(uint8_t) * (e->current_offset + size_st);

	return size_st;
}

int fm_delete(const char *filename)
{
	char *fullname;
	struct fs_entry *e;

	check_and_init_hcc();

	fullname = getfullname(filename);

	TAILQ_FOREACH(e, &_head, entries) {
		if (strcmp(fullname, e->fullname) == 0) {
			if (e->stat.attr & F_ATTR_DIR) {
				return F_ERR_INVALIDNAME;
			}
			TAILQ_REMOVE(&_head, e, entries);
			if (e->data != NULL)
				free(e->data);
			free(e);
			return F_NO_ERROR;
		}
	}

	return F_ERR_NOTFOUND;
}

int fm_seek(FN_FILE *filehandle, long offset, long whence)
{
	struct fs_entry *e = (struct fs_entry *) filehandle->reference;

	check_and_init_hcc();

	if (whence != F_SEEK_SET) {
		PRINT_UNSUPPORTED("is F_SEEK_SET is supported for the whence argument");
		return F_ERR_UNKNOWN;
	}

	e->current_offset = offset;
	return F_NO_ERROR;
}

int fm_stat(const char *filename, F_STAT *stat)
{
	char *fullname = getfullname(filename);
	struct fs_entry *e;

	check_and_init_hcc();

	TAILQ_FOREACH(e, &_head, entries) {
		if (strcmp(fullname, e->fullname) == 0) {
			*stat = e->stat;
			return F_NO_ERROR;
		}
	}

	return F_ERR_NOTFOUND;
}

long fm_filelength(const char *filename)
{
	char *fullname = getfullname(filename);
	struct fs_entry *e;

	check_and_init_hcc();

	TAILQ_FOREACH(e, &_head, entries) {
		if (strcmp(fullname, e->fullname) == 0) {
			return e->stat.filesize;
		}
	}

	return -(F_ERR_NOTFOUND);
}

#endif /* USE_HAL_CLONE_HCC_STUBS */
