/**
 * @file      pio.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "at91/peripherals/pio/pio.h" stub implementations
 */

#include <stdio.h>
#include <stdlib.h>

#include <at91/peripherals/pio/pio.h>

#include <hal-clone/pio.h>

#include "macros.h"

/* @brief List of pins to ignore, because they are configured as a part
          of the early board initialization and cannot be stubbed */
static const Pin _hardcoded_ignore_pins[] = {
	PINS_DBGU,
};

static struct hal_clone_gpio hal_clone_next_pio_to_gpio(const Pin *pin, int *bit);
static unsigned char should_ignore_pio(const Pin *pin);
static void hal_clone_pio_print_warning(const char *msg, const Pin *pin);

unsigned char PIO_Configure(const Pin *list, unsigned int size)
{
	unsigned int i;
	int pin_idx;
	struct hal_clone_gpio gpio;
	enum hal_clone_gpio_status status;
	enum hal_clone_gpio_value initial_value;

	if (list == NULL)
		return 0;

	for (i = 0; i < size; i++) {
		if (should_ignore_pio(&list[i]))
			continue;

		pin_idx = 0;
		for (gpio = hal_clone_next_pio_to_gpio(&list[i], &pin_idx);
		     pin_idx < 32;
		     gpio = hal_clone_next_pio_to_gpio(&list[i], &pin_idx)) {

			switch (list[i].type) {
			case PIO_PERIPH_A:
			case PIO_PERIPH_B:
				// TODO: Not sure if this is accurate behavior
				status = HAL_CLONE_GPIO_UNINITIALIZED;
				break;
			case PIO_INPUT:
				status = HAL_CLONE_GPIO_INPUT;
				// TODO: Not sure if this is accurate behavior
				if (list[i].attribute & PIO_PULLUP) {
					initial_value = HAL_CLONE_GPIO_HIGH;
				} else {
					initial_value = HAL_CLONE_GPIO_UNDEFINED;
				}
				break;
			case PIO_OUTPUT_0:
				status = HAL_CLONE_GPIO_OUTPUT;
				initial_value = HAL_CLONE_GPIO_LOW;
				break;
			case PIO_OUTPUT_1:
				status = HAL_CLONE_GPIO_OUTPUT;
				initial_value = HAL_CLONE_GPIO_HIGH;
				break;
			default:
				hal_clone_pio_print_warning("Pin has unknown type", &list[i]);
				return 0;
			}
			if (hal_clone_gpio_configure(gpio, status, initial_value) != 0) {
				return 0;
			}
		}
	}

	return 1;
}

void PIO_Set(const Pin *pin)
{
	int pin_idx = 0;
	struct hal_clone_gpio gpio;

	if (pin == NULL) {
		PRINT_UNEXPECTED(0);
	}

	for (gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx);
	     pin_idx < 32;
	     gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx)) {
		(void) hal_clone_gpio_set(gpio, HAL_CLONE_GPIO_HIGH);
	}
}

void PIO_Clear(const Pin *pin)
{
	int pin_idx = 0;
	struct hal_clone_gpio gpio;

	if (pin == NULL) {
		PRINT_UNEXPECTED(0);
	}

	for (gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx);
	     pin_idx < 32;
	     gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx)) {
		(void) hal_clone_gpio_set(gpio, HAL_CLONE_GPIO_LOW);
	}
}

unsigned char PIO_Get(const Pin *pin)
{
	int pin_idx = 0;
	struct hal_clone_gpio gpio;
	enum hal_clone_gpio_value value;
	enum hal_clone_gpio_value output_value = HAL_CLONE_GPIO_UNDEFINED;

	if (pin == NULL) {
		PRINT_UNEXPECTED(0);
	}

	for (gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx);
	     pin_idx < 32;
	     gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx)) {
		if (hal_clone_gpio_get(gpio, &value) == 0) {
			if (output_value == HAL_CLONE_GPIO_UNDEFINED ||
			    value == HAL_CLONE_GPIO_HIGH) {
				output_value = value;
			}
		}
	}
	if (output_value == HAL_CLONE_GPIO_UNDEFINED) {
		return rand() & 1;
	}
	return value == HAL_CLONE_GPIO_HIGH ? 1 : 0;
}

unsigned char PIO_GetOutputDataStatus(const Pin *pin)
{
	// PIO_Get should work the same as this function, as long
	// as the pin is an output. For input pins, the behavior isn't
	// really well defined anyway.

	return PIO_Get(pin);
}

unsigned int PIO_GetISR(const Pin *pin)
{
	(void) pin;

	PRINT_UNIMPLEMENTED;

	return 0;
}

/* Functions not part of the driver */

/**
 * @brief Converts AT91 PIO pins to fixed line values.
 *
 * @param[in] pin The pin that should be connected to fixed value.
 *                If multiple bits in the mask are set, all pins
 *                are connected to the fixed value.
 * @param[in] name The name of each bit in the mask of the the pin,
 *                 which is used only for debugging.
 * @param[in] value The fixed value.
 */
void hal_clone_at91_pio_connect_to_fixed(const Pin *pin, const char *name, enum hal_clone_gpio_value value)
{
	int pin_idx = 0;
	struct hal_clone_gpio gpio;

	for (gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx);
	     pin_idx < 32;
	     gpio = hal_clone_next_pio_to_gpio(pin, &pin_idx)) {
		hal_clone_gpio_connect_to_fixed(gpio, name, value);
	}
}

/**
 * @brief Converts an AT91 PIO pin to HAL clone common GPIOs.
 *
 * @note This function is only able to convert PIO pins where
 *       only one bit in the mask is set. Other bits in the mask
 *       are ignored.
 *
 * @param[in] pin The pin whose bit in its mask should be converted
 *                to an common GPIO.
 *
 * @return The common GPIO structure that corresponds to the PIO
 *         mask bit.
 */
struct hal_clone_gpio hal_clone_at91_pio_to_gpio(const Pin *pin)
{
	int bit = 0;
	return hal_clone_next_pio_to_gpio(pin, &bit);
}

/**
 * @brief Converts AT91 PIO pins to HAL clone common GPIOs.
 *
 * @note This function is intended to be called in a loop, iterating
 *       over each bit in the mask of the PIO pin, returning a GPIO
 *       for each set bit.
 *
 * @param[in] pin The pin whose next bit in its mask should be converted
 *                to an common GPIO.
 * @param[in,out] bit A pointer that points to the index of the next bit
 *                    to convert, and also to the location to store the
 *                    index of the next bit that should be converted.
 *
 * @return The common GPIO structure that corresponds to the next PIO
 *         mask bit. If there are no more bits to convert, then the next
 *         bit is set to 32.
 */
static struct hal_clone_gpio hal_clone_next_pio_to_gpio(const Pin *pin, int *bit)
{
	struct hal_clone_gpio gpio;
	int i;

	gpio.kind = HAL_CLONE_GPIO_KIND_AT91;
	for (i = *bit; i < 32; i++) {
		if (pin->mask & (1 << i)) {
			gpio.id = (pin->id << 8) | i;
			break;
		}
	}

	*bit = i + 1;

	return gpio;
}

static void hal_clone_pio_print_warning(const char *msg, const Pin *pin)
{
	fprintf(stderr, "PIO WARNING: %s\n", msg);
	if (pin != NULL) {
		fprintf(stderr, "\tmask:      0x%x\n", pin->mask);
		fprintf(stderr, "\tid:        0x%x\n", pin->id);
		fprintf(stderr, "\ttype:      0x%x\n", pin->type);
		fprintf(stderr, "\tattribute: 0x%x\n", pin->attribute);
	}
}

static unsigned char should_ignore_pio(const Pin *pin)
{
	for (size_t i = 0; i < PIO_LISTSIZE(_hardcoded_ignore_pins); i++) {
		if (pin->id == _hardcoded_ignore_pins[i].id &&
		    pin->mask == _hardcoded_ignore_pins[i].mask)
			return 1;
	}
	return 0;
}
