/**
 * @file      semihosting.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     ARM semihosting API implementation
 * @info https://developer.arm.com/documentation/dui0471/g/Semihosting/What-is-semihosting-?lang=en
 */

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <hal-clone/semihosting.h>
#include <hal-clone/semihosting_memory.h>

#include "macros.h"

#define SEMIHOSTING_SYS_OPEN           0x01
#define SEMIHOSTING_SYS_CLOSE          0x02
#define SEMIHOSTING_SYS_WRITEC         0x03
#define SEMIHOSTING_SYS_WRITE0         0x04
#define SEMIHOSTING_SYS_WRITE          0x05
#define SEMIHOSTING_SYS_READ           0x06
#define SEMIHOSTING_SYS_READC          0x07
#define SEMIHOSTING_SYS_ISERROR        0x08
#define SEMIHOSTING_SYS_ISTTY          0x09
#define SEMIHOSTING_SYS_SEEK           0x0A
#define SEMIHOSTING_SYS_FLEN           0x0C
#define SEMIHOSTING_SYS_TMPNAM         0x0D
#define SEMIHOSTING_SYS_REMOVE         0x0E
#define SEMIHOSTING_SYS_RENAME         0x0F
#define SEMIHOSTING_SYS_CLOCK          0x10
#define SEMIHOSTING_SYS_TIME           0x11
#define SEMIHOSTING_SYS_SYSTEM         0x12
#define SEMIHOSTING_SYS_ERRNO          0x13
#define SEMIHOSTING_SYS_GET_CMDLINE    0x15
#define SEMIHOSTING_SYS_HEAPINFO       0x16
#define SEMIHOSTING_SYS_EXIT           0x18
#define SEMIHOSTING_SYS_ELAPSED        0x30
#define SEMIHOSTING_SYS_TICKFREQ       0x31

#ifdef USE_NONSTANDARD_HALT_SEMIHOSTING
#define SEMIHOSTING_SYS_HALT           0xFE
#endif

static int _semihosting_call(int r0, int r1)
{
	register int reg0 asm("r0");
	register int reg1 asm("r1");
	reg0 = r0;
	reg1 = r1;
	asm("svc 0x00123456");
	return reg0;
}

int semihosting_open(const char *pathname, enum semihosting_open_flags flags)
{
	int len = strlen(pathname);
	int args[3] = {
		(int) pathname,
		(int) flags,
		(int) len
	};
	return _semihosting_call(SEMIHOSTING_SYS_OPEN, (int) args);
}

int semihosting_close(int fd)
{
	return _semihosting_call(SEMIHOSTING_SYS_CLOSE, (int) &fd);
}

void semihosting_writec(char c)
{
	(void) _semihosting_call(SEMIHOSTING_SYS_WRITEC, (int) &c);
}

void semihosting_write0(const char *str)
{
	(void) _semihosting_call(SEMIHOSTING_SYS_WRITE0, (int) str);
}

int semihosting_write(int fd, const uint8_t *buf, size_t count)
{
	int args[3] = {
		fd,
		(int) buf,
		(int) count,
	};
	return _semihosting_call(SEMIHOSTING_SYS_WRITE, (int) args);
}

int semihosting_read(int fd, uint8_t *buf, size_t count)
{
	int args[3] = {
		fd,
		(int) buf,
		(int) count
	};
	return _semihosting_call(SEMIHOSTING_SYS_READ, (int) args);
}

int semihosting_readc(void)
{
	return _semihosting_call(SEMIHOSTING_SYS_READC, 0);
}

int semihosting_iserror(int status)
{
	return _semihosting_call(SEMIHOSTING_SYS_ISERROR, (int) &status);
}

int semihosting_istty(int fd)
{
	return _semihosting_call(SEMIHOSTING_SYS_ISTTY, (int) &fd);
}

int semihosting_seek(int fd, off_t offset)
{
	int args[2] = {
		fd,
		(int) offset
	};
	return _semihosting_call(SEMIHOSTING_SYS_SEEK, (int) args);
}

ssize_t semihosting_flen(int fd)
{
	return (ssize_t) _semihosting_call(SEMIHOSTING_SYS_FLEN, (int) &fd);
}

int semihosting_tmpnam(char *buf, uint8_t id, size_t buflen)
{
	int args[3] = {
		(int) buf,
		(int) id,
		(int) buflen,
	};
	return _semihosting_call(SEMIHOSTING_SYS_TMPNAM, (int) args);
}

int semihosting_remove(const char *pathname)
{
	int len = strlen(pathname);
	int args[2] = {
		(int) pathname,
		(int) len
	};
	return _semihosting_call(SEMIHOSTING_SYS_REMOVE, (int) args);
}

int semihosting_rename(const char *old_name, const char *new_name)
{
	int oldlen = strlen(old_name);
	int newlen = strlen(new_name);
	int args[4] = {
		(int) old_name,
		oldlen,
		(int) new_name,
		newlen
	};
	return _semihosting_call(SEMIHOSTING_SYS_RENAME, (int) args);
}

uint32_t semihosting_clock(void)
{
	return _semihosting_call(SEMIHOSTING_SYS_CLOCK, 0);
}

uint32_t semihosting_time(void)
{
	return (uint32_t) _semihosting_call(SEMIHOSTING_SYS_TIME, 0);
}

int semihosting_system(const char *command)
{
	int len = strlen(command);
	int args[2] = {
		(int) command,
		(int) len
	};
	return _semihosting_call(SEMIHOSTING_SYS_SYSTEM, (int) args);
}

int semihosting_errno(void)
{
	return _semihosting_call(SEMIHOSTING_SYS_ERRNO, 0);
}

ssize_t semihosting_get_cmdline(char *buf, size_t buflen)
{
	int ret;
	int args[2] = {
		(int) buf,
		(int) buflen
	};
	ret = _semihosting_call(SEMIHOSTING_SYS_GET_CMDLINE, (int) args);
	if (ret != 0) {
		return -1;
	}
	// Return the size of the null-terminated cmdline args
	return (ssize_t) args[1];
}

void semihosting_heapinfo(struct semihosting_heapblock *block)
{
	(void) _semihosting_call(SEMIHOSTING_SYS_HEAPINFO, (int) block);
}

void semihosting_exit(enum semihosting_exit_reason reason)
{
	(void) _semihosting_call(SEMIHOSTING_SYS_EXIT, (int) reason);
}

int64_t semihosting_elapsed(void)
{
	int ret[2];
	(void) _semihosting_call(SEMIHOSTING_SYS_ELAPSED, (int) ret);
	if (ret[1] < 0)
		return -1;
	return (((int64_t) ret[1]) << 32) | ret[0];
}

int semihosting_tickfreq(void)
{
	return _semihosting_call(SEMIHOSTING_SYS_TICKFREQ, 0);
}

#ifdef USE_NONSTANDARD_HALT_SEMIHOSTING
int64_t semihosting_halt(int64_t max_ticks)
{
	int err;
	int ret[2];

	ret[0] = (int) (max_ticks & 0xFFFFFFFF);
	ret[1] = (int) (max_ticks >> 32);
	err = _semihosting_call(SEMIHOSTING_SYS_HALT, (int) ret);
	if (err != 0)
		return -1;
	return (((int64_t) ret[1]) << 32) | ret[0];
}
#endif

/* Semihosting memory functions */

int semihosting_memory_open(const char *pathname, ssize_t full_size, uint8_t default_value)
{
	int err;
	ssize_t len;
	int fd;

	if (full_size < 1)
		return 0;

	// Open memory file and probe its length
	fd = semihosting_open(pathname, OPEN_FLAGS_AB);
	if (fd < 1) {
		PRINT_UNEXPECTED(semihosting_errno());
		return 0;
	}

	len = semihosting_flen(fd);
	if (len < 0) {
		PRINT_UNEXPECTED(semihosting_errno());
		(void) semihosting_close(fd);
		return 0;
	} else if (len != full_size) {
		// Invalid memory file length, close and truncate
		err = semihosting_close(fd);
		if (err != 0) {
			PRINT_UNEXPECTED(semihosting_errno());
			return 0;
		}
		fd = semihosting_open(pathname, OPEN_FLAGS_WB);
		if (fd < 1) {
			PRINT_UNEXPECTED(semihosting_errno());
			return 0;
		}
		len = 0;
	}
	if (len == 0) {
		// Memory is empty, create it
		err = semihosting_memory_fill(fd, full_size, default_value);
		if (err != 0) {
			return 0;
		}
	}

	// Memory file is now valid, open for read/write
	err = semihosting_close(fd);
	if (err != 0) {
		PRINT_UNEXPECTED(semihosting_errno());
		return 0;
	}
	fd = semihosting_open(pathname, OPEN_FLAGS_RBP);
	if (fd < 1) {
		PRINT_UNEXPECTED(semihosting_errno());
		return 0;
	}

	return fd;
}

void semihosting_memory_close(int fd)
{
	int err;

	err = semihosting_close(fd);
	if (err != 0) {
		PRINT_UNEXPECTED(semihosting_errno());
	}
}

int semihosting_memory_fill(int fd, ssize_t full_size, uint8_t fill)
{
	int err, i, write_size;
	uint8_t buffer[1024];

	for (i = 0; i < sizeof(buffer); i++)
		buffer[i] = fill;

	err = semihosting_seek(fd, 0);
	if (err != 0) {
		PRINT_UNEXPECTED(semihosting_errno());
		return -1;
	}
	for (i = 0; i < full_size; i += sizeof(buffer)) {
		write_size = full_size - i > sizeof(buffer) ? sizeof(buffer) : full_size - i;

		err = semihosting_write(fd, buffer, (size_t) write_size);
		if (err != 0) {
			PRINT_UNEXPECTED(semihosting_errno());
			return -1;
		}
	}

	return 0;
}

int semihosting_memory_read(int fd, off_t address, uint8_t *buf, size_t count)
{
	int err;

	err = semihosting_seek(fd, address);
	if (err != 0) {
		PRINT_UNEXPECTED(semihosting_errno());
		return -1;
	}
	err = semihosting_read(fd, buf, count);
	if (err != 0) {
		PRINT_UNEXPECTED(err);
		PRINT_UNEXPECTED(semihosting_errno());
		return -1;
	}

	return 0;
}

int semihosting_memory_write(int fd, off_t address, const uint8_t *buf, size_t count)
{
	int err;

	err = semihosting_seek(fd, address);
	if (err != 0) {
		PRINT_UNEXPECTED(semihosting_errno());
		return -1;
	}
	err = semihosting_write(fd, buf, count);
	if (err != 0) {
		PRINT_UNEXPECTED(err);
		PRINT_UNEXPECTED(semihosting_errno());
		return -1;
	}

	return 0;
}
