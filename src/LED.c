/**
 * @file      LED.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "hal/Drivers/LED.h" stub implementations
 *
 * @note These are all unsupported for now since the LEDs have
 *       little use from a testing perspective.
 */

#include <hal/Drivers/LED.h>
#include "macros.h"


void LED_start(void)
{
	return;
}

void LED_glow(LED led)
{
	PRINT_UNSUPPORTED("LEDs are unsupported");
}

void LED_dark(LED led)
{
	PRINT_UNSUPPORTED("LEDs are unsupported");
}

void LED_toggle(LED led)
{
	PRINT_UNSUPPORTED("LEDs are unsupported");
}

void LED_wave(unsigned int times)
{
	PRINT_UNSUPPORTED("LEDs are unsupported");
}

void LED_waveReverse(unsigned int times)
{
	PRINT_UNSUPPORTED("LEDs are unsupported");
}
