/**
 * @file      gpio.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Provides base GPIO abstraction that can be used to represent
 *            and use different GPIO formats together
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <hal-clone/common/gpio.h>
#include <hal-clone/semihosting.h>

#define HAL_CLONE_GPIO_WARNING(...) hal_clone_gpio_warning(__func__, __LINE__, __VA_ARGS__)

/*
 * @brief Represents the runtime-state of a GPIO
 */
struct hal_clone_gpio_pin {
	/* @brief A name given to the GPIO. Is only used for debugging as a
	          GPIO is always referenced by its ID internally. */
	const char *name;
	/* @brief the global identifier for ths GPIO */
	struct hal_clone_gpio id;

	/* @brief The status the GPIO has been configured in */
	enum hal_clone_gpio_status status;
	/*
	 * @brief The interrupt serivce routine of the GPIO.
	 */
	enum hal_clone_gpio_value (*isr)(struct hal_clone_gpio, enum hal_clone_gpio_status, enum hal_clone_gpio_value);
};

/*
 * @brief Represents a line connected to two GPIO endpoints
 */
struct hal_clone_gpio_line {
	/* @brief The GPIO at one end of the line */
	struct hal_clone_gpio_pin one;
	/* @brief The GPIO at the other end of the line */
	struct hal_clone_gpio_pin other;

	/* @brief The logical value of the line */
	enum hal_clone_gpio_value value;

	/* @brief The next line in the list of all lines */
	struct hal_clone_gpio_line *next;
};
static struct hal_clone_gpio_line *_lines = NULL;

static void hal_clone_gpio_warning(const char *func, int line, const char *format, ...);
static enum hal_clone_gpio_value *hal_clone_gpio_find_line(struct hal_clone_gpio pin,
                                                           struct hal_clone_gpio_pin **our,
                                                           struct hal_clone_gpio_pin **their);
static struct hal_clone_gpio_line *hal_clone_gpio_line_create(struct hal_clone_gpio one,
                                                              const char* one_name,
                                                              struct hal_clone_gpio other,
                                                              const char *other_name);
static struct hal_clone_gpio_line *hal_clone_gpio_line_alloc(struct hal_clone_gpio one,
                                                             const char* one_name,
                                                             struct hal_clone_gpio other,
                                                             const char* other_name);

/*
 * @brief Prints status information about all GPIO lines to stderr
 */
void hal_clone_gpio_print_status(void)
{
	char buf[31];
	struct hal_clone_gpio_line **line;
	struct hal_clone_gpio_pin *one;
	struct hal_clone_gpio_pin *other;
	size_t one_name_len;
	const char *line_char;

	for (line = &_lines; *line != NULL; line = &(*line)->next) {
		one = &(*line)->one;
		other = &(*line)->other;

		if (one->id.kind != HAL_CLONE_GPIO_KIND_RESERVED) {
			fprintf(stderr, "Kind 0x%02x Id 0x%06x", one->id.kind, one->id.id);
		} else {
			fprintf(stderr, "%*c", 21, ' ');
		}
		if (other->id.kind != HAL_CLONE_GPIO_KIND_RESERVED) {
			fprintf(stderr, "%*c", 36, ' ');
			fprintf(stderr, "Kind 0x%02x Id 0x%06x", other->id.kind, other->id.id);
		}
		fprintf(stderr, "\n");

		snprintf(buf, sizeof(buf) / sizeof(buf[0]), "%s", one->name);
		buf[30] = '\0';
		one_name_len = strlen(buf) + 1; // for trailing space
		fputs(buf, stderr);
		fprintf(stderr, " ");

		switch (one->status) {
		case HAL_CLONE_GPIO_INPUT:
			line_char = "<";
			break;
		case HAL_CLONE_GPIO_OUTPUT:
			line_char = ">";
			break;
		default:
			line_char = "-";
			break;
		}
		for (size_t i = 0; i < 36 - one_name_len; i++) {
			fprintf(stderr, line_char);
		}

		switch ((*line)->value) {
		case HAL_CLONE_GPIO_LOW:
			fprintf(stderr, " LO ");
			break;
		case HAL_CLONE_GPIO_HIGH:
			fprintf(stderr, " HI ");
			break;
		default:
			fprintf(stderr, " ?? ");
			break;
		}

		switch (other->status) {
		case HAL_CLONE_GPIO_INPUT:
			line_char = ">";
			break;
		case HAL_CLONE_GPIO_OUTPUT:
			line_char = "<";
			break;
		default:
			line_char = "-";
			break;
		}
		for (size_t i = 0; i < 16; i++) {
			fprintf(stderr, line_char);
		}
		fprintf(stderr, " ");

		snprintf(buf, sizeof(buf) / sizeof(buf[0]), "%s", other->name);
		buf[30] = '\0';
		fputs(buf, stderr);
		fprintf(stderr, "\n");

		if (one->id.kind != HAL_CLONE_GPIO_KIND_RESERVED) {
			fprintf(stderr, "ISR @0x%08x", (unsigned int) one->isr);
		} else {
			fprintf(stderr, "%*c", 15, ' ');
		}
		if (other->id.kind != HAL_CLONE_GPIO_KIND_RESERVED) {
			fprintf(stderr, "%*c", 42, ' ');
			fprintf(stderr, "ISR @0x%08x", (unsigned int) other->isr);
		}
		fprintf(stderr, "\n\n");
	}
}

/**
 * @brief Connects two GPIOs together, without configuring their status,
 *        resulting in a new GPIO line. This must be done before the pins
 *        are configured or accessed in other ways.
 *
 * @param[in] one One of the GPIOs that should be connected.
 * @param[in] one_name The name of that GPIO, used for debugging.
 * @param[in] other The other of the GPIOs that should be connected.
 * @param[in] other_name The name of the other GPIO, used for debugging.
 */
void hal_clone_gpio_connect(struct hal_clone_gpio one,
                            const char *one_name,
                            struct hal_clone_gpio other,
                            const char *other_name)
{
	if (one.kind == HAL_CLONE_GPIO_KIND_RESERVED || other.kind == HAL_CLONE_GPIO_KIND_RESERVED) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d is reserved and may not be used.",
		                       HAL_CLONE_GPIO_KIND_RESERVED);
		return;
	}

	if (hal_clone_gpio_equal(one, other)) {
		HAL_CLONE_GPIO_WARNING("Cannot connect equal pins '%s' and '%s' to each other", one_name, other_name);
		return;
	}

	(void) hal_clone_gpio_line_create(one, one_name, other, other_name);
}

/**
 * @brief Connects a GPIO to a fixed value, either ground, VDD, or
 *        to nothing (dangling line)
 *
 * @param[in] pin The GPIO to connect.
 * @param[in] name The name of that GPIO, used for debugging.
 * @param[in] value The fixed value to connect the GPIO to.
 */
void hal_clone_gpio_connect_to_fixed(struct hal_clone_gpio pin, const char *name, enum hal_clone_gpio_value value)
{
	struct hal_clone_gpio_line *line;

	if (pin.kind == HAL_CLONE_GPIO_KIND_RESERVED) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d is reserved and may not be used.",
		                       HAL_CLONE_GPIO_KIND_RESERVED);
		return;
	}

	struct hal_clone_gpio fixed;
	fixed.kind = HAL_CLONE_GPIO_KIND_RESERVED;
	fixed.id = (uint32_t) value;
	const char *fixed_name;
	switch (value) {
	case HAL_CLONE_GPIO_LOW:
		fixed_name = "__GND__";
		break;
	case HAL_CLONE_GPIO_HIGH:
		fixed_name = "__VDD__";
		break;
	default:
		fixed_name = "__???__";
		break;
	}

	line = hal_clone_gpio_line_create(pin, name, fixed, fixed_name);
	if (line == NULL)
		return;

	line->value = value;

	// Assume that a defined value is an output and an undefined
	// value is a black hole
	if (value != HAL_CLONE_GPIO_UNDEFINED) {
		line->other.status = HAL_CLONE_GPIO_OUTPUT;
	} else {
		line->other.status = HAL_CLONE_GPIO_INPUT;
	}
}

/**
 * @brief Configures a GPIO, setting its status as input or output
 *        along with an initial value.
 *
 * @param[in] pin The GPIO to configure.
 * @param[in] status The status to configure the GPIO as.
 * @param[in] initial_value The initial value of the line of that GPIO,
 *                          assigned only if it hasn'ät already been assigned
 *                          by the GPIO that this GPIO is connected to. Note
 *                          that the initial value does not trigger any interupt
 *                          service routines.
 *
 * @return 0 on success. Otherwise -1
 */
int hal_clone_gpio_configure(struct hal_clone_gpio pin,
                             enum hal_clone_gpio_status status,
                             enum hal_clone_gpio_value initial_value)
{
	struct hal_clone_gpio_pin *our = NULL;
	struct hal_clone_gpio_pin *their = NULL;

	if (pin.kind == HAL_CLONE_GPIO_KIND_RESERVED) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d is reserved and may not be used.",
		                       HAL_CLONE_GPIO_KIND_RESERVED);
		return -1;
	}

	enum hal_clone_gpio_value *line_value = hal_clone_gpio_find_line(pin, &our, &their);
	if (line_value == NULL) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d and id 0x%x is not connected", pin.kind, pin.id);
		return -1;
	}

	our->status = status;

	if (status == HAL_CLONE_GPIO_OUTPUT || *line_value == HAL_CLONE_GPIO_UNDEFINED) {
		*line_value = initial_value;
	}

	return 0;
}

/**
 * @brief Sets an output GPIO and its input peer to a value. If the value of their
 *        line changes, the peer's interrupt service routine is called.
 *
 * @param[in] pin The GPIO to set.
 * @param[in] status The value to set that GPIO to.
 *
 * @return 0 on success. Otherwise -1
 */
int hal_clone_gpio_set(struct hal_clone_gpio pin, enum hal_clone_gpio_value value)
{
	struct hal_clone_gpio_pin *our = NULL;
	struct hal_clone_gpio_pin *their = NULL;
	enum hal_clone_gpio_value prev_value;

	if (pin.kind == HAL_CLONE_GPIO_KIND_RESERVED) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d is reserved and may not be used.",
		                       HAL_CLONE_GPIO_KIND_RESERVED);
		return -1;
	}

	enum hal_clone_gpio_value *line_value = hal_clone_gpio_find_line(pin, &our, &their);
	if (line_value == NULL) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d and id 0x%x is not connected", pin.kind, pin.id);
		return -1;
	}
	prev_value = *line_value;
	*line_value = value;

	if (our->status != HAL_CLONE_GPIO_OUTPUT) {
		HAL_CLONE_GPIO_WARNING("Pin '%s' of kind %d and id 0x%x is not an output or not configured", our->name, pin.kind, pin.id);
	}
	if (their->status == HAL_CLONE_GPIO_OUTPUT) {
		HAL_CLONE_GPIO_WARNING("Peer pin '%s' of kind %d and id 0x%x is an output", their->name, their->id.kind, their->id.id);
	} else if (their->isr != NULL) {
		// Only call their ISR if they are an input and expect to receive a signal
		if ((prev_value != HAL_CLONE_GPIO_LOW && value == HAL_CLONE_GPIO_LOW) ||
			(prev_value != HAL_CLONE_GPIO_HIGH && value == HAL_CLONE_GPIO_HIGH)) {
			(void) their->isr(their->id, their->status, value);
		}
	}

	return 0;
}

/**
 * @brief Gets the value of an input or output GPIO. If its peer is an output GPIO,
 *        the value is retrieved by calling the peer's interrupt service routine.
 *
 * @param[in] pin The GPIO to get.
 * @param[in] status Where to store the retrieved value of the GPIO.
 *
 * @return 0 on success. Otherwise -1
 */
int hal_clone_gpio_get(struct hal_clone_gpio pin, enum hal_clone_gpio_value *value)
{
	struct hal_clone_gpio_pin *our = NULL;
	struct hal_clone_gpio_pin *their = NULL;

	if (pin.kind == HAL_CLONE_GPIO_KIND_RESERVED) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d is reserved and may not be used.",
		                       HAL_CLONE_GPIO_KIND_RESERVED);
		return -1;
	}

	enum hal_clone_gpio_value *line_value = hal_clone_gpio_find_line(pin, &our, &their);
	if (line_value == NULL) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d and id 0x%x is not connected", pin.kind, pin.id);
		return -1;
	}
	if (our->status == HAL_CLONE_GPIO_UNINITIALIZED) {
		// Do not print warning if we are an output since we can get our own output signal
		HAL_CLONE_GPIO_WARNING("Pin '%s' of kind %d and id 0x%x is not configured", our->name, pin.kind, pin.id);
	}
	if (their->status == HAL_CLONE_GPIO_UNINITIALIZED && their->id.kind != HAL_CLONE_GPIO_KIND_RESERVED) {
		HAL_CLONE_GPIO_WARNING("Peer pin '%s' of kind %d and id 0x%x is not configured", their->name, their->id.kind, their->id.id);
	} else if (their->status == HAL_CLONE_GPIO_OUTPUT && their->isr != NULL) {
		// Only call their ISR if they are an output and expect to return a signal
		*line_value = their->isr(their->id, their->status, HAL_CLONE_GPIO_UNDEFINED);
	}
	if (*line_value == HAL_CLONE_GPIO_UNDEFINED) {
		HAL_CLONE_GPIO_WARNING("Pin '%s' of kind %d and id 0x%x value is undefined", our->name, pin.kind, pin.id);
	}
	*value = *line_value;

	return 0;
}

/**
 * @brief Sets the interupt service routine of a GPIO. This can be done before
 *        configuring the GPIO as input or output, but must be done after connecting
 *        it to another GPIO.
 *
 * @param[in] pin The GPIO whose ISR to set.
 * @param[in] isr The interrupt serivce routine to set, called when a GPIO should be read
 *                or when the value of the GPIO has been toggled, for output and input GPIOs
 *                respectively.
 *
 *                The first argument is the GPIO ID that
 *                should handle the interrupt, the second argument is the status of
 *                that GPIO as configured previously, and the third argument is the
 *                new value that the GPIO has been set to (for input GPIOs only, for
 *                output GPIOs this argument is undefined).
 *
 *                The return value should be the new value of the GPIO (for output
 *                GPIOs only, for input GPIOs the return value is ignored)
 *
 *
 * @return 0 on success. Otherwise -1
 */
int hal_clone_gpio_set_isr(
    struct hal_clone_gpio pin,
    enum hal_clone_gpio_value (*isr)(struct hal_clone_gpio, enum hal_clone_gpio_status, enum hal_clone_gpio_value)
) {
	struct hal_clone_gpio_pin *our = NULL;
	struct hal_clone_gpio_pin *their = NULL;

	if (pin.kind == HAL_CLONE_GPIO_KIND_RESERVED) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d is reserved and may not be used.",
		                       HAL_CLONE_GPIO_KIND_RESERVED);
		return -1;
	}

	enum hal_clone_gpio_value *line_value = hal_clone_gpio_find_line(pin, &our, &their);
	if (line_value == NULL) {
		HAL_CLONE_GPIO_WARNING("Pin of kind %d and id 0x%x is not connected", pin.kind, pin.id);
		return -1;
	}

	our->isr = isr;

	return 0;
}

/**
 * @brief Checks if two GPIOs are equal
 *
 * @param[in] one One GPIO to compare
 * @param[in] other The other GPIO to compare.
 *
 * @return 1 if they are equal. Otherwise 0.
 */
unsigned char hal_clone_gpio_equal(struct hal_clone_gpio one, struct hal_clone_gpio other)
{
	return one.kind == other.kind && one.id == other.id;
}

static void hal_clone_gpio_warning(const char *func, int line, const char *format, ...)
{
	va_list argp;

	fprintf(stderr, "[HAL-CLONE GPIO] %s:%d: ", func, line);

	va_start(argp, format);
	vfprintf(stderr, format, argp);
	va_end(argp);

	fprintf(stderr, "\n");
}

static enum hal_clone_gpio_value *hal_clone_gpio_find_line(struct hal_clone_gpio pin,
                                                           struct hal_clone_gpio_pin **our,
                                                           struct hal_clone_gpio_pin **their)
{
	struct hal_clone_gpio_line **line;

	for (line = &_lines; *line != NULL; line = &(*line)->next) {
		if (hal_clone_gpio_equal((*line)->one.id, pin)) {
			*our = &(*line)->one;
			*their = &(*line)->other;

			return &(*line)->value;
		} else if (hal_clone_gpio_equal((*line)->other.id, pin)) {
			*our = &(*line)->other;
			*their = &(*line)->one;

			return &(*line)->value;
		}
	}

	return NULL;
}

static struct hal_clone_gpio_line *hal_clone_gpio_line_create(struct hal_clone_gpio one,
                                                              const char* one_name,
                                                              struct hal_clone_gpio other,
                                                              const char *other_name)
{
	struct hal_clone_gpio_line **line;
	struct hal_clone_gpio_pin already_connected;

	for (line = &_lines; *line != NULL; line = &(*line)->next) {
		already_connected = (*line)->one;
		for (int p = 0; p < 2; p++) {
			if (already_connected.id.kind != HAL_CLONE_GPIO_KIND_RESERVED) {
				if (hal_clone_gpio_equal(already_connected.id, one)) {
					HAL_CLONE_GPIO_WARNING("Pin '%s' of kind %d and id 0x%x is already connected as '%s'",
					                       one_name,
					                       one.kind,
					                       one.id,
					                       already_connected.name);
					return NULL;
				} else if (hal_clone_gpio_equal(already_connected.id, other)) {
					HAL_CLONE_GPIO_WARNING("Pin '%s' of kind %d and id 0x%x is already connected as '%s'",
					                       other_name,
					                       other.kind,
					                       other.id,
					                       already_connected.name);
					return NULL;
				}
			}
			already_connected = (*line)->other;
		}
	}

	*line = hal_clone_gpio_line_alloc(one, one_name, other, other_name);

	return *line;
}

static struct hal_clone_gpio_line *hal_clone_gpio_line_alloc(struct hal_clone_gpio one,
                                                             const char* one_name,
                                                             struct hal_clone_gpio other,
                                                             const char* other_name)
{
	struct hal_clone_gpio_line *line = malloc(sizeof(struct hal_clone_gpio_line));
	if (line == NULL) {
		HAL_CLONE_GPIO_WARNING("Failed to allocate memory, exiting");
		semihosting_exit(ADP_Stopped_ApplicationExit);
	}
	line->one.id = one;
	line->one.name = one_name;
	line->one.status = HAL_CLONE_GPIO_UNINITIALIZED;
	line->one.isr = NULL;

	line->other.id = other;
	line->other.name = other_name;
	line->other.status = HAL_CLONE_GPIO_UNINITIALIZED;
	line->other.isr = NULL;

	line->value = HAL_CLONE_GPIO_UNDEFINED;
	line->next = NULL;

	return line;
}
