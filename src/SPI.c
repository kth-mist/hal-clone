/**
 * @file      SPI.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "hal/Drivers/SPI.h" stub implementations
 */

#include <stdlib.h>

#include <hal/Drivers/SPI.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>

#include <hal-clone/error.h>
#include <hal-clone/spi.h>

#include "macros.h"

// Arbitrary timeout used when taking the main semaphore
#define SPI_SEMAPHORE_TIMEOUT (1000)

static SPIdriverState state = uninitialized_spiState;
static SPIslave max_slave;

static xSemaphoreHandle spiSemaphore = NULL;

static const char* UNSUPPORTED_BUS_MSG = "Only SPI bus 1 is supported. For bus 0, use Time.h, FRAM.h, or supervisor.h instead.";

struct {
	int (*write_read)(const uint8_t*, uint8_t*, size_t);
} spi_bus1_dev[slave7_spi + 1] = {{0}};

int SPI_start(SPIbus bus, SPIslave spi1_max_cs)
{
	if (bus != bus1_spi) {
		PRINT_UNSUPPORTED(UNSUPPORTED_BUS_MSG);
		return -1;
	}
	if (state != uninitialized_spiState)
		return 0;

	if (spiSemaphore == NULL)
		spiSemaphore = xSemaphoreCreateMutex();

	max_slave = (spi1_max_cs > slave2_spi) ? spi1_max_cs : slave2_spi;
	if (max_slave > slave7_spi)
		max_slave = slave7_spi;

	state = idle_spiState;
	return 0;
}

void SPI_stop(SPIbus bus)
{
	if (bus != bus1_spi) {
		PRINT_UNSUPPORTED(UNSUPPORTED_BUS_MSG);
	}
	state = uninitialized_spiState;
}

int SPI_queueTransfer(SPItransfer *tx)
{
	PRINT_UNIMPLEMENTED;
	return -1;
}

int SPI_writeRead(SPItransfer *tx)
{
	int err = 0;

	if (state == uninitialized_spiState)
		return -1;

	if (tx == NULL || tx->transferSize == 0 ||
	    tx->writeData == NULL || tx->readData == NULL ||
	    tx->slaveParams == NULL)
		return -3;

	if (tx->slaveParams->busSpeed_Hz > SPI_MAX_BUS_SPEED ||
	    tx->slaveParams->busSpeed_Hz < SPI_MIN_BUS_SPEED)
		return -2;

	if (xSemaphoreTake(spiSemaphore, SPI_SEMAPHORE_TIMEOUT) != pdTRUE)
		return -4;

	state = transfer_spiState;

	switch (tx->slaveParams->bus) {
	case bus0_spi:
		PRINT_UNSUPPORTED(UNSUPPORTED_BUS_MSG);
		err = -1;
		break;

	case bus1_spi:
		if (tx->slaveParams->slave <= max_slave &&
		    spi_bus1_dev[tx->slaveParams->slave].write_read != NULL) {
			err = spi_bus1_dev[tx->slaveParams->slave].write_read(
			            tx->writeData,
			            (uint8_t*) tx->readData,
			            tx->transferSize);
			if (err != 0) {
				err = -1;
			}
		} else {
			err = -1;
		}
		break;

	default:
		err = -3;
		break;
	}

	xSemaphoreGive(spiSemaphore);
	state = idle_spiState;

	return err;
}

SPIdriverState SPI_getDriverState(SPIbus bus)
{
	switch (bus) {
	case bus0_spi:
		PRINT_UNSUPPORTED(UNSUPPORTED_BUS_MSG);
		break;
	case bus1_spi:
		return state;
	default:
		break;
	}
	return idle_spiState;
}

/* Functions not part of the driver */

/**
 * @brief Registers an SPI write-read callback function that should be called when
 *        The I2C master requests a write-read to the given bus and slave.
 *
 * @param[in] bus The bus of the SPI slave address that should be mapped to the callback function.
 * @param[in] slave The SPI slave address that should be mapped to the callback function.
 * @param[in] cb The function that should be called when there is a request to write and read to the address.
 *               The data to write, A buffer to store the data to read in, and the size of the data to
 *               write and read, will be provided as arguments. The function should return 0 if it
 *               was successful, or anything else if it was not.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int hal_clone_spi_register_cb(SPIbus bus,
                              SPIslave slave,
                              int (*cb)(const uint8_t*, uint8_t*, size_t))
{
	if (bus != bus1_spi) {
		PRINT_UNSUPPORTED(UNSUPPORTED_BUS_MSG);
		return HAL_CLONE_ERR_OOB;
	}
	if (slave > slave7_spi)
		return HAL_CLONE_ERR_OOB;
	if (cb == NULL)
		return HAL_CLONE_ERR_NULL;

	spi_bus1_dev[slave].write_read = cb;

	return 0;
}
