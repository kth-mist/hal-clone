/**
 * @file      NorFlash.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "at91/memories/norflash/" stub implementations
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <at91/boards/ISIS_OBC_G20/board.h>

#include <at91/memories/norflash/NorFlashCommon.h>
#include <at91/memories/norflash/NorFlashApi.h>
#include <at91/memories/norflash/NorFlashCFI.h>

#include <hal-clone/storage.h>
#include <hal-clone/semihosting_memory.h>

#include "macros.h"

static const char *_norflash_semihost_file = NULL;
static int _fd = -1;

static unsigned char *_norflash_loc = NULL;

static void _norflash_reset(struct NorFlashInfo *info, unsigned int address);
static unsigned char _norflash_write(struct NorFlashInfo *info,
                                     unsigned int address,
                                     unsigned char *buffer,
                                     unsigned int size);
static unsigned int _norflash_get_manufacturer(struct NorFlashInfo *info);
static unsigned int _norflash_get_device_id(struct NorFlashInfo *info);
static unsigned char _norflash_erase_chip(struct NorFlashInfo *info);
static unsigned char _norflash_erase_sector(struct NorFlashInfo *info,
                                            unsigned int sectorAddr);

static const struct NorFlashOperations _ops = {
	_norflash_reset,
	_norflash_write,
	_norflash_get_manufacturer,
	_norflash_get_device_id,
	_norflash_erase_chip,
	_norflash_erase_sector
};

/**
 * This is a replacement of the fuction in the AT91 library. That version of this function has a bug
 * where it mistakenly detects the first byte of a given sector as belonging to the previous sector.
 * This means that if one writes to the second byte of a sector, the first one will always be erased.
 * Likewise, if one writes to the first byte of a sector, it might not be detected that it should be erased
 * first, which leads to the write failing.
 */
static unsigned short NorFlash_GetDeviceSectorInRegion_bugfix(struct NorFlashInfo *info, unsigned int offset)
{
	int i, j;
	unsigned int current_offset = 0;
	unsigned short current_sector = 0;
	struct NorFlashCfiDeviceGeometry *geo;

	geo = &info->cfiDescription.norFlashCfiDeviceGeometry;

	for (i = 0; i < geo->numEraseRegion; i++) {
		for (j = 0; j < geo->eraseRegionInfo[i].Y + 1; j++) {
			current_offset += geo->eraseRegionInfo[i].Z << 8;
			if (current_offset > offset)
				return current_sector;

			current_sector++;
		}
	}

	return current_sector;
}

static void _norflash_reset(struct NorFlashInfo *info, unsigned int address)
{
	(void) info;
	(void) address;
}

static unsigned char _norflash_write(struct NorFlashInfo *info,
                                     unsigned int address,
                                     unsigned char *buffer,
                                     unsigned int size)
{
	(void) info;

	if ((address + size) > BOARD_NORFLASH_SIZE)
		return NorCommon_ERROR_CANNOTWRITE;

	if (_fd >= 0) {
		int err = semihosting_memory_write(_fd,
		                                   (off_t) address,
		                                   (const uint8_t*) buffer,
		                                   (size_t) size);
		if (err != 0) {
			return NorCommon_ERROR_CANNOTWRITE;
		}
	} else if (_norflash_loc != NULL) {
		memcpy(&_norflash_loc[address], buffer, size);
	} else {
		return NorCommon_ERROR_PROTECT;
	}

	return NorCommon_ERROR_NONE;
}

static unsigned int _norflash_get_manufacturer(struct NorFlashInfo *info)
{
	(void) info;

	return 0xDEADBEEF;
}

static unsigned int _norflash_get_device_id(struct NorFlashInfo *info)
{
	(void) info;

	return 0xDEADC0DE;
}

static unsigned char _norflash_erase_chip(struct NorFlashInfo *info)
{
	int err;
	unsigned int sectors;

	sectors = NorFlash_GetDeviceNumOfBlocks(info);

	for (unsigned int sector = 0; sector < sectors; sectors++) {
		err = _norflash_erase_sector(info, NorFlash_GetDeviceSectorAddress(info, sector));
		if (err != 0)
			return err;
	}

	return NorCommon_ERROR_NONE;
}

static unsigned char _norflash_erase_sector(struct NorFlashInfo *info,
                                            unsigned int sectorAddr)
{
	int err;
	unsigned int size;
	unsigned short sector;
	unsigned int actual_sector_addr;
	unsigned char erase_buf[1024];
	unsigned int erase_count;
	unsigned int erase_chunk;

	memset(erase_buf, 0xff, sizeof(erase_buf));

	sector = NorFlash_GetDeviceSectorInRegion_bugfix(info, sectorAddr);
	actual_sector_addr = NorFlash_GetDeviceSectorAddress(info, sector);
	size = NorFlash_GetDeviceBlockSize(info, sector);

	for (erase_count = 0; erase_count < size; erase_count += erase_chunk) {
		erase_chunk = size - erase_count > sizeof(erase_buf) ?
		              sizeof(erase_buf) : size - erase_count;

		err = _norflash_write(info, actual_sector_addr + erase_count, erase_buf, erase_chunk);
		if (err != 0)
			return err;
	}

	return NorCommon_ERROR_NONE;
}

unsigned char NORFLASH_ReadData(struct NorFlash *flash,
                                unsigned int address,
                                unsigned char *buffer,
                                unsigned int size)
{
	(void) flash;

	if ((address + size) > BOARD_NORFLASH_SIZE)
		return NorCommon_ERROR_CANNOTREAD;

	if (_fd >= 0) {
		int err = semihosting_memory_read(_fd,
		                                  (off_t) address,
		                                  (uint8_t*) buffer,
		                                  (size_t) size);
		if (err != 0) {
			return NorCommon_ERROR_CANNOTREAD;
		}
	} else if (_norflash_loc != NULL) {
		memcpy(buffer, &_norflash_loc[address], size);
	} else {
		return NorCommon_ERROR_PROTECT;
	}

	return NorCommon_ERROR_NONE;
}

unsigned char NorFlash_CFI_Detect(struct NorFlash *norFlash, unsigned char hardwareBusWidth)
{
	unsigned int i;
	struct NorFlashCfiQueryInfo *info;
	struct NorFlashCfiDeviceGeometry *geo;

	if (norFlash->norFlashInfo.baseAddress != BOARD_NORFLASH_BASE_ADDRESS ||
	    hardwareBusWidth != BOARD_NORFLASH_BUSWIDTH >> 3) {
		return NorCommon_ERROR_UNKNOWNMODEL;
	}

	if (_norflash_semihost_file != NULL) {
		_fd = semihosting_memory_open(_norflash_semihost_file, BOARD_NORFLASH_SIZE, 0xFF);
		if (_fd < 1) {
			return NorCommon_ERROR_UNKNOWNMODEL;
		}
		// _fd is never be closed, this is handled automatically when the host exits
	} else if (_norflash_loc != NULL) {
		for (i = 0; i < BOARD_NORFLASH_SIZE; i++)
			_norflash_loc[i] = 0xFF;
	} else {
		return NorCommon_ERROR_UNKNOWNMODEL;
	}

	norFlash->norFlashInfo.deviceChipWidth = hardwareBusWidth;
	norFlash->norFlashInfo.cfiCompatible = 1;

	info = &norFlash->norFlashInfo.cfiDescription.norFlashCfiQueryInfo;
	geo = &norFlash->norFlashInfo.cfiDescription.norFlashCfiDeviceGeometry;

	memcpy(info->queryUniqueString, "QRY", 3);
	geo->deviceSize = 20; // 1 MiB
	geo->numEraseRegion = 2; // 2 regions

	geo->eraseRegionInfo[0].Y = 7; // 7 + 1 blocks per region
	geo->eraseRegionInfo[0].Z = 32; // 32 * 256 B (8 KiB) Erase sectors
	geo->eraseRegionInfo[1].Y = 14; // 14 + 1 blocks per region
	geo->eraseRegionInfo[1].Z = 256; // 256 * 256 B (64 KiB) Erase sectors

	// TODO Set the other fields

	norFlash->pOperations = &_ops;

	return NorCommon_ERROR_NONE;
}

/* Functions not part of the driver */

/**
 * @brief Sets a path to a file on the host that should be used as the NorFlash memory with semihosting.
 *
 * @param[in] path The path to the file on the host.
 */
void hal_clone_norflash_set_semihosting_file(const char *path)
{
	_norflash_semihost_file = path;
}

/**
 * @brief Sets a memory location that should be used as the NorFlash memory.
 *
 * @note If the semihost file is set, this function will not have any effect.
 *
 * @param[in] memory The memory location to use.
 *                   The memory MUST be at least BOARD_NORFLASH_SIZE bytes.
 */
void hal_clone_set_norflash_in_memory(unsigned char *memory)
{
	_norflash_loc = memory;
}

