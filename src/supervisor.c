/**
 * @file      supervisor.c
 * @author    Calin Capitanu
 * @copyright MIT License
 * @brief     "hal/supervisor.h" stub implementations
 *
 * @note Many of the functions here are not properly implemented, and some functions
 *       may be missing.
 *       It is not difficult to break any of these functions, so just because the
 *       program passes/fails these tests does not necessarily mean that it the same
 *       will happen when running the code on the iOBC.
 */

#include <hal/supervisor.h>
#include <hal/errors.h>
#include "macros.h"

static uint8_t initialized = 0;

int Supervisor_start(uint8_t* address, uint8_t count)
{
	if (address == NULL && count != 0) {
		return E_INPUT_POINTER_NULL;
	}
	if (initialized == 0) {
		initialized = 1;
		return E_NO_SS_ERR;
	}
	else {
		return E_IS_INITIALIZED;
	}
}

int Supervisor_emergencyReset(supervisor_generic_reply_t* reply, uint8_t index)
{
	PRINT_UNSUPPORTED("Supervisor_emergencyReset is unsupported");
	return E_NO_SS_ERR;
}

int Supervisor_reset(supervisor_generic_reply_t* reply, uint8_t index)
{
	PRINT_UNSUPPORTED("Supervisor_reset is unsupported");
	return E_NO_SS_ERR;
}

int Supervisor_writeOutput(uint8_t output, supervisor_generic_reply_t* reply, uint8_t index)
{
	PRINT_UNSUPPORTED("Supervisor_writeOutput is unsupported");
	return E_NO_SS_ERR;
}

int Supervisor_powerCycleIobc(supervisor_generic_reply_t* reply, uint8_t index)
{
	PRINT_UNSUPPORTED("Supervisor_powerCycleIobc is unsupported");
	return E_NO_SS_ERR;
}

int Supervisor_getVersion(supervisor_version_configuration_t* versionReply, uint8_t index)
{
	PRINT_UNSUPPORTED("Supervisor_getVersion is unsupported");
	return E_NO_SS_ERR;
}

// Note that this function stub is partially supported.
int Supervisor_getHousekeeping(supervisor_housekeeping_t* housekeepingReply, uint8_t index)
{
	if (housekeepingReply == NULL) {
		return E_INPUT_POINTER_NULL;
	}
	(*housekeepingReply).fields.iobcResetCount = 17;
	(*housekeepingReply).fields.iobcUptime = 315;
	return E_NO_SS_ERR;
}

void Supervisor_calculateAdcValues(supervisor_housekeeping_t* housekeepingReply, int16_t* adcValue)
{
	(*housekeepingReply).fields.adcData[_temperature_measurement] = 13;
	adcValue[0] = 13;
}
