/**
 * @file      I2C.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     "hal/Drivers/I2C.h" stub implementations
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <hal/Drivers/I2C.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>

#include <hal-clone/i2c.h>
#include <hal-clone/error.h>

#include "macros.h"

/**
 * The actual value of these may not match with the actual
 * implementations, sometimes because of lack of testing,
 * sometimes to make it easier from a testing perspective.
 */
static I2CdriverState state = uninitialized_i2cState;
static I2CtransferStatus transfer = done_i2c;

static xSemaphoreHandle i2cSemaphore = NULL;

static portTickType i2c_timeout;

static struct {
	int (*write)(const uint8_t*, size_t);
	int (*read)(uint8_t*, size_t);
	unsigned int err_dur;
	unsigned char err_prob;
} i2c_dev[I2C_MAX_ADDR + 1] = {{0}};

static int generate_error_code(unsigned char addr);

int I2C_start(unsigned int i2cBusSpeed_Hz, unsigned int i2cTransferTimeout)
{
	if (state != uninitialized_i2cState)
		return 0;
	if (i2cSemaphore == NULL)
		i2cSemaphore = xSemaphoreCreateMutex();

	I2C_setTransferTimeout(i2cTransferTimeout);
	state = idle_i2cState;
	return 0;
}

void I2C_setTransferTimeout(unsigned int i2cTransferTimeout)
{
	if (i2cTransferTimeout < 1)
		i2cTransferTimeout = 1;

	i2c_timeout = i2cTransferTimeout / portTICK_RATE_MS;
}

/**
 * Does not check if there are ongoing transactions before stopping.
 * Assumes that the bus is in an erroneous state if there was a timeout.
 */
void I2C_stop(void)
{
	int err = I2C_blockBus(i2c_timeout);
	if (!err) {
		state = uninitialized_i2cState;
		I2C_releaseBus();
	} else {
		state = error_i2cState;
	}
}

/**
 * Note that if the bus isn't initialized, actual behavior
 * seems to be a crash in QEMU.
 */
int I2C_blockBus(portTickType timeout)
{
	if (state == uninitialized_i2cState)
		return 0;
	if (xSemaphoreTake(i2cSemaphore, timeout) != pdTRUE)
		return 1;

	return 0;
}

/**
 * Again, if the bus isn't initialized actual behavior
 * seems to be a crash in QEMU. This is true for most
 * functions in the I2C driver.
 */
void I2C_releaseBus(void)
{
	if (state == uninitialized_i2cState)
		return;

	xSemaphoreGive(i2cSemaphore);
}

int I2C_write(unsigned int slaveAddress, unsigned char *data, unsigned int size)
{
	int err;

	if (state == uninitialized_i2cState || data == NULL || size < 1 || slaveAddress > I2C_MAX_ADDR)
		return -2;

	err = generate_error_code(slaveAddress);
	if (err)
		return err;

	err = I2C_blockBus(i2c_timeout);
	if (err) {
		transfer = timeoutError_i2c;
		return -3;
	}

	state = write_i2cState;
	transfer = pending_i2c;

	if (i2c_dev[slaveAddress].write != NULL) {
		err = i2c_dev[slaveAddress].write((const uint8_t*) data, (size_t) size);
		transfer = (err == 0) ? done_i2c : error_i2c;
	} else {
		transfer = writeError_i2c;
	}
	state = idle_i2cState;

	I2C_releaseBus();
	return (int)I2C_getCurrentTransferStatus();
}

int I2C_read(unsigned int slaveAddress, unsigned char *data, unsigned int size)
{
	int err;

	if (state == uninitialized_i2cState || data == NULL || size < 1 || slaveAddress > I2C_MAX_ADDR)
		return -2;

	err = generate_error_code(slaveAddress);
	if (err)
		return err;

	err = I2C_blockBus(i2c_timeout);
	if (err) {
		transfer = timeoutError_i2c;
		return -3;
	}

	state = read_i2cState;
	transfer = pending_i2c;

	if (i2c_dev[slaveAddress].read != NULL) {
		err = i2c_dev[slaveAddress].read((uint8_t*) data, (size_t) size);
		transfer = (err == 0) ? done_i2c : error_i2c;
	} else {
		transfer = readError_i2c;
	}
	state = idle_i2cState;

	I2C_releaseBus();
	return (int)I2C_getCurrentTransferStatus();
}

int I2C_writeRead(I2Ctransfer *tx)
{
	int err;

	if (tx == NULL || (tx->writeSize <= 0 && tx->readSize <= 0))
		return -2;

	if (tx->writeSize > 0) {
		err = I2C_write(tx->slaveAddress, tx->writeData, tx->writeSize);
		if (err)
			return err;
		// Skip yielding
	}
	if (tx->readSize > 0) {
		err = I2C_read(tx->slaveAddress, (unsigned char*)tx->readData, tx->readSize);
		if (err)
			return err;
	}

	return (int)I2C_getCurrentTransferStatus();
}

/**
 * This seems like it would require an I2C request task and an I2CgenericTransfer
 * queue.
 */
int I2C_queueTransfer(I2CgenericTransfer *tx)
{
	PRINT_UNIMPLEMENTED;
	return 0;
}

I2CdriverState I2C_getDriverState(void)
{
	return state;
}

I2CtransferStatus I2C_getCurrentTransferStatus(void)
{
	return transfer;
}

/* Functions not part of the driver */

static int generate_error_code(unsigned char addr)
{
	int err;

	if (addr > I2C_MAX_ADDR ||
	    (i2c_dev[addr].err_prob == 0 && i2c_dev[addr].err_dur == 0)) {
		// Fall back to the generall call address
		addr = I2C_GENERAL_CALL_ADDR;
	}

	// Check if an error should be spoofed.
	if ((rand() % 100) < i2c_dev[addr].err_prob) {
		err = (int) error_i2c;
	} else {
		err = 0;
	}

	// Decrement the duration, and clear
	// the probability if it reaches zero.
	if (i2c_dev[addr].err_dur > 0) {
		i2c_dev[addr].err_dur--;
		if (i2c_dev[addr].err_dur == 0) {
			i2c_dev[addr].err_prob = 0;
		}
	}

	return err;
}

/**
 * @brief Registers an I2C write and I2C read callback function that should be called when
 *        The I2C master requests a write or read respectively to the given address.
 *
 * @param[in] addr The I2C address that should be mapped to the callback functions.
 * @param[in] write_cb The function that should be called when there is a request to write to the address.
 *                     The data to send and its length will be provided as arguments. The function should
 *                     return 0 if it was successful, or anything else if it was not.
 * @param[in] read_cb The function that should be called when there is a request to read to the address.
 *                    A buffer to store the data in and its max length will be provided as arguments.
 *                    The function should return 0 if it was successful, or anything else if it was not.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int hal_clone_i2c_register_cb(unsigned char addr,
                              int (*write_cb)(const uint8_t*, size_t),
                              int (*read_cb)(uint8_t*, size_t))
{
	if (write_cb == NULL || read_cb == NULL)
		return HAL_CLONE_ERR_NULL;

	if (addr > I2C_MAX_ADDR || addr == I2C_GENERAL_CALL_ADDR)
		return HAL_CLONE_ERR_OOB;

	i2c_dev[addr].write = write_cb;
	i2c_dev[addr].read = read_cb;

	return 0;
}

/**
 * @brief Makes any request to write or read over I2C to the given I2C address
 *        return a generic error code, for the given duration (number of I2C
 *        read or write requests) and with the given probability.
 *
 * @param[in] addr The I2C address to the slave device that should result in random
 *                 error codes when addressed. I2C_GENERAL_CALL_ADDR is interpreted
 *                 as all I2C addresses.
 * @param[in] duration The number of consecutive I2C read or write requests to the given address
 *                     that should be affected by the spoofing before it wears off. A value of
 *                     zero is interpreted as infinity.
 * @param[in] probability The probability in percent that a random error code should
 *                        be returned.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int hal_clone_i2c_err_spoof(unsigned char addr,
                            unsigned int duration,
                            unsigned char probability)
{
	if (addr > I2C_MAX_ADDR || probability > 100)
		return HAL_CLONE_ERR_OOB;

	i2c_dev[addr].err_dur = duration;
	i2c_dev[addr].err_prob = probability;

	return 0;
}
