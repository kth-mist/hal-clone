.PHONY: all bin test clean

HAL-CLONE-DIR="$(dir $(abspath $(lastword $(MAKEFILE_LIST))))"
HAL-CLONE-TEST-DIR="$(shell cd "$(HAL-CLONE-TEST-DIR)/test"; pwd -P)"

# Change this if necessary
ISIS-HAL-LINK=git@gitlab.com:kth-mist/isis-sdk.git

KNOWN-QEMU-DIRS=test/obc-qemu /opt/obc-qemu

# Candidate directories for the qemu installation (ignore error prints)
QEMU-CANDIDATES=$(shell ls -d $(KNOWN-QEMU-DIRS) 2> /dev/null)

KNOWN-ECLIPSE-DIRS=/c/isis/application/eclipse /cygdrive/c/isis/application/eclipse    \
                   /mnt/c/isis/application/eclipse ~/.wine/drive_c/ISIS/application/eclipse \
                   /opt/eclipse

# Candidate directories for the eclipse installation (ignore error prints)
ECLIPSE-CANDIDATES=$(shell ls -d $(KNOWN-ECLIPSE-DIRS) 2> /dev/null)

INCLUDE=include
SRC=src
OUT=Debug
HALOUT=Release

KERNEL-NAME=$(shell uname -s)
KERNEL-RELEASE=$(shell uname -r)

DOCKER-BUILD-DIR="$(HAL-CLONE-DIR)/docker"
DOCKER-ECLIPSE-IMAGE="wstamist/eclipse:latest"
DOCKER-QEMU-IMAGE="wstamist/qemu:latest"
DOCKER-HAL-CLONE-DIR=/opt/hal-clone
DOCKER-RUN-ARGS=--rm -it    \
	--name=hal-clone-tests-make    \
	-p 1234:1234 \
	-v $(HAL-CLONE-DIR):$(DOCKER-HAL-CLONE-DIR) -w $(DOCKER-HAL-CLONE-DIR)
DOCKER-CHOWN-CMD=chown -R $(shell id -u $(USER)):$(shell id -g $(USER)) "$(DOCKER-HAL-CLONE-DIR)"

IS-WIN=${shell case "${shell echo "$(KERNEL-NAME)" | tr a-z A-Z}" in      \
		CYGWIN*|MINGW*|MSYS*) echo 1 ;;                                \
		LINUX)                                                        \
			case "${shell echo "$(KERNEL-RELEASE)" | tr a-z A-Z}" in   \
				*MICROSOFT*) echo 1;;                              \
				*) echo 0 ;;                                    \
			esac                                             \
		;;                                                \
		*) echo 0 ;;                                   \
esac}

all: bin test

hal: halbin haltest

get-dependencies:
	@if [ ! -d "test/isis-sdk" ]; then git clone "$(ISIS-HAL-LINK)" "test/isis-sdk"; fi

eclipseinstall:
	$(eval ECLIPSE-DIR := $(word 1, $(ECLIPSE-CANDIDATES)))
	@if [ $(IS-WIN) -eq 1 ]; then   \
		if [ -z "$(ECLIPSE-DIR)" ]; then  \
			echo -e "\033[1;31mPlease install the ISIS SDK via the official installer.\033[m"; \
			exit 1; \
		fi; \
	elif [ -z "$(shell docker images -q $(DOCKER-ECLIPSE-IMAGE))" ]; then   \
		docker pull $(DOCKER-ECLIPSE-IMAGE);   \
	fi

qemuinstall:
	@if [ -z "$(shell docker images -q $(DOCKER-QEMU-IMAGE))" ]; then   \
		docker pull $(DOCKER-QEMU-IMAGE); \
	fi

bin: get-dependencies
	$(eval ECLIPSE-DIR := $(word 1, $(ECLIPSE-CANDIDATES)))
	@if [ -z "$(ECLIPSE-DIR)" ]; then                                                                    \
		echo -e "\033[1;31mCannot make output binary: Could not find ISIS SDK Eclipse install directory.\033[m"; \
		echo -e "\033[1;37mBuilding using docker instead.\033[m"; \
		$(MAKE) eclipseinstall || exit 1;   \
		docker run $(DOCKER-RUN-ARGS) $(DOCKER-ECLIPSE-IMAGE) sh -c "make bin && $(DOCKER-CHOWN-CMD)/$(OUT)";  \
	else  \
		if [ $(IS-WIN) -eq 0 ]; then   \
			if [ -f "$(ECLIPSE-DIR)/eclipsec.exe" ]; then \
				export WINEDEBUG=-all;       \
				export WINEPATH="$(WINEPATH);C:\\ISIS\\application\\toolchain\\bin;C:\\ISIS\\application\\MinGW\\bin;C:\\ISIS\\application\\MinGW\\msys\\1.0\\bin"; \
				wine "$(ECLIPSE-DIR)/eclipsec.exe" -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild  \
						-import . -build "test/$(OUT)" --launcher.suppressErrors;    \
			else \
				"$(ECLIPSE-DIR)/eclipse" -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild  \
					-data "$(ECLIPSE-DIR)" -import . -build "test/$(OUT)" --launcher.suppressErrors;       \
			fi \
		else \
			"$(ECLIPSE-DIR)/eclipsec.exe" -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild  \
			        -import . -build "test/$(OUT)" --launcher.suppressErrors; \
		fi  \
	fi

halbin: get-dependencies
	$(eval ECLIPSE-DIR := $(word 1, $(ECLIPSE-CANDIDATES)))
	@if [ -z "$(ECLIPSE-DIR)" ]; then                                                                    \
		echo -e "\033[1;31mCannot make output binary: Could not find ISIS SDK Eclipse install directory.\033[m"; \
		echo -e "\033[1;37mBuilding using docker instead.\033[m"; \
		$(MAKE) eclipseinstall || exit 1;   \
		docker run $(DOCKER-RUN-ARGS) $(DOCKER-ECLIPSE-IMAGE) sh -c "make halbin && $(DOCKER-CHOWN-CMD)/$(HALOUT)";  \
	else   \
		if [ $(IS-WIN) -eq 0 ]; then   \
			if [ -f "$(ECLIPSE-DIR)/eclipsec.exe" ]; then \
				export WINEDEBUG=-all;       \
				export WINEPATH="$(WINEPATH);C:\\ISIS\\application\\toolchain\\bin;C:\\ISIS\\application\\MinGW\\bin;C:\\ISIS\\application\\MinGW\\msys\\1.0\\bin"; \
				wine "$(ECLIPSE-DIR)/eclipsec.exe" -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild  \
						-import . -build "test/$(HALOUT)" --launcher.suppressErrors;    \
			else \
				"$(ECLIPSE-DIR)/eclipse" -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild  \
					-data "$(ECLIPSE-DIR)" -import . -build "test/$(HALOUT)" --launcher.suppressErrors;       \
			fi \
		else \
			"$(ECLIPSE-DIR)/eclipsec.exe" -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild  \
			        -import . -build "test/$(HALOUT)" --launcher.suppressErrors; \
		fi  \
	fi

test:
	$(eval QEMU-DIR := $(word 1, $(QEMU-CANDIDATES)))
	@if [ ! -d "$(QEMU-DIR)" ]; then  \
		$(MAKE) qemuinstall || exit 1;   \
		docker run $(DOCKER-RUN-ARGS) $(DOCKER-QEMU-IMAGE) make test;  \
	else   \
		"$(QEMU-DIR)/iobc-loader" -f sdram "$(OUT)/test.bin" -s sdram -o pmc-mclk \
			-- -semihosting -semihosting-config target=native -serial stdio -nographic -monitor none -s;   \
	fi

haltest:
	$(eval QEMU-DIR := $(word 1, $(QEMU-CANDIDATES)))
	@if [ ! -d "$(QEMU-DIR)" ]; then  \
		$(MAKE) qemuinstall || exit 1;   \
		docker run $(DOCKER-RUN-ARGS) $(DOCKER-QEMU-IMAGE)  make haltest;  \
	else   \
		"$(QEMU-DIR)/iobc-loader" -f sdram "$(HALOUT)/test.bin" -s sdram -o pmc-mclk \
			-- -semihosting -semihosting-config target=native -serial stdio -nographic -monitor none -s; \
	fi

clean:
	rm -rf "$(OUT)";
	rm -rf "$(HALOUT)";
