/**
 * @file      i2c.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Functions that can be used to configure the HAL clone I2C driver
 */

#ifndef MIST_HAL_CLONE_I2C_H
#define MIST_HAL_CLONE_I2C_H

#include <stdlib.h>
#include <stdint.h>

#define I2C_GENERAL_CALL_ADDR  (0x00)
#define I2C_MAX_ADDR           (0x7F)

int hal_clone_i2c_register_cb(unsigned char addr,
                              int (*write_cb)(const uint8_t*, size_t),
                              int (*read_cb)(uint8_t*, size_t));

int hal_clone_i2c_err_spoof(unsigned char addr,
                            unsigned int duration,
                            unsigned char probability);

#endif /* MIST_HAL_CLONE_I2C_H */
