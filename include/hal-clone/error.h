/**
 * @file      error.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Error codes used by HAL clone functions that are not part of the real ISIS HAL
 */

#ifndef MIST_HAL_CLONE_ERROR_H
#define MIST_HAL_CLONE_ERROR_H

#define HAL_CLONE_ERR_NULL   (1)  // Received an argument that was NULL
#define HAL_CLONE_ERR_OOB    (2)  // Received an argument that was either too small or too large (out of bounds)

#endif /* MIST_HAL_CLONE_ERROR_H */
