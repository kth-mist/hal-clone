/**
 * @file      storage.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Functions that can be used to configure HAL clone storage drivers
 */

#ifndef MIST_HAL_CLONE_STORAGE_H
#define MIST_HAL_CLONE_STORAGE_H

#define FRAM_MAX_ADDRESS (0x3FFFF)

void hal_clone_fram_set_semihosting_file(const char *path);
void hal_clone_set_fram_in_memory(unsigned char *memory);

void hal_clone_norflash_set_semihosting_file(const char *path);
void hal_clone_set_norflash_in_memory(unsigned char *memory);

#endif /* MIST_HAL_CLONE_STORAGE_H */