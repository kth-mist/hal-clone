/**
 * @file      gpio.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Provides common GPIO abstraction that can be used to represent
 *            and use different GPIO formats together
 */

#ifndef MIST_HAL_CLONE_COMMON_GPIO_H_
#define MIST_HAL_CLONE_COMMON_GPIO_H_

#include <stdint.h>

/*
 * @brief GPIO Kind for use internally witin the GPIO abstraction. May not
 *        be used by other Pin kinds.
 */
#define HAL_CLONE_GPIO_KIND_RESERVED   (0)

enum hal_clone_gpio_status {
	HAL_CLONE_GPIO_UNINITIALIZED = 0,
	HAL_CLONE_GPIO_INPUT,
	HAL_CLONE_GPIO_OUTPUT,
};

enum hal_clone_gpio_value {
	HAL_CLONE_GPIO_UNDEFINED = 0,
	HAL_CLONE_GPIO_LOW,
	HAL_CLONE_GPIO_HIGH,
};

struct hal_clone_gpio {
	/* @brief Global identifier for the pin kind (AT91 PIO, Arduino, for example) */
	uint8_t kind;
	/* @brief Global identifier for the pin unique within its kind */
	uint32_t id : 24;
};

unsigned char hal_clone_gpio_equal(struct hal_clone_gpio one, struct hal_clone_gpio other);

void hal_clone_gpio_print_status(void);

void hal_clone_gpio_connect(struct hal_clone_gpio one,
                            const char *one_name,
                            struct hal_clone_gpio other,
                            const char *other_name);
void hal_clone_gpio_connect_to_fixed(struct hal_clone_gpio pin, const char *name, enum hal_clone_gpio_value value);

int hal_clone_gpio_configure(struct hal_clone_gpio pin,
                             enum hal_clone_gpio_status status,
                             enum hal_clone_gpio_value initial_value);
int hal_clone_gpio_set(struct hal_clone_gpio pin, enum hal_clone_gpio_value value);
int hal_clone_gpio_get(struct hal_clone_gpio pin, enum hal_clone_gpio_value *value);
int hal_clone_gpio_set_isr(
    struct hal_clone_gpio pin,
    enum hal_clone_gpio_value (*isr)(struct hal_clone_gpio, enum hal_clone_gpio_status, enum hal_clone_gpio_value)
);

#endif /* MIST_HAL_CLONE_COMMON_GPIO_H_ */
