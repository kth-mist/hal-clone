/**
 * @file      semihosting.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Functions that can be used to make ARM semihosting calls
 */

#ifndef MIST_HAL_CLONE_SEMIHOSTING_H
#define MIST_HAL_CLONE_SEMIHOSTING_H

#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>

enum semihosting_open_flags {
	OPEN_FLAGS_R   = 0,
	OPEN_FLAGS_RB  = 1,
	OPEN_FLAGS_RP  = 2,
	OPEN_FLAGS_RBP = 3,
	OPEN_FLAGS_W   = 4,
	OPEN_FLAGS_WB  = 5,
	OPEN_FLAGS_WP  = 6,
	OPEN_FLAGS_WBP = 7,
	OPEN_FLAGS_A   = 8,
	OPEN_FLAGS_AB  = 9,
	OPEN_FLAGS_AP  = 10,
	OPEN_FLAGS_ABP = 11
};

enum semihosting_exit_reason {
	ADP_Stopped_BranchThroughZero    = 0x20000,
	ADP_Stopped_UndefinedInstr       = 0x20001,
	ADP_Stopped_SoftwareInterrupt    = 0x20002,
	ADP_Stopped_PrefetchAbort        = 0x20003,
	ADP_Stopped_DataAbort            = 0x20004,
	ADP_Stopped_AddressException     = 0x20005,
	ADP_Stopped_IRQ                  = 0x20006,
	ADP_Stopped_FIQ                  = 0x20007,
	ADP_Stopped_BreakPoint           = 0x20020,
	ADP_Stopped_WatchPoint           = 0x20021,
	ADP_Stopped_StepComplete         = 0x20022,
	ADP_Stopped_RunTimeErrorUnknown  = 0x20023,
	ADP_Stopped_InternalError        = 0x20024,
	ADP_Stopped_UserInterruption     = 0x20025,
	ADP_Stopped_ApplicationExit      = 0x20026,
	ADP_Stopped_StackOverflow        = 0x20027,
	ADP_Stopped_DivisionByZero       = 0x20028,
	ADP_Stopped_OSSpecific           = 0x20029
};

struct semihosting_heapblock {
	int heap_base;
	int heap_limit;
	int stack_base;
	int stack_limit;
};

int semihosting_open(const char *pathname, enum semihosting_open_flags flags);
int semihosting_close(int fd);
void semihosting_writec(char c);
void semihosting_write0(const char *str);
int semihosting_write(int fd, const uint8_t *buf, size_t count);
int semihosting_read(int fd, uint8_t *buf, size_t count);
int semihosting_readc(void);
int semihosting_iserror(int status);
int semihosting_istty(int fd);
int semihosting_seek(int fd, off_t offset);
ssize_t semihosting_flen(int fd);
int semihosting_tmpnam(char *buf, uint8_t id, size_t buflen);
int semihosting_remove(const char *pathname);
int semihosting_rename(const char *old_name, const char *new_name);
uint32_t semihosting_clock(void);
uint32_t semihosting_time(void);
int semihosting_system(const char *command);
int semihosting_errno(void);
ssize_t semihosting_get_cmdline(char *buf, size_t buflen);
void semihosting_heapinfo(struct semihosting_heapblock *block);
void semihosting_exit(enum semihosting_exit_reason reason);
int64_t semihosting_elapsed(void);
int semihosting_tickfreq(void);

#ifdef USE_NONSTANDARD_HALT_SEMIHOSTING
int64_t semihosting_halt(int64_t max_ticks);
#endif

#endif /* MIST_HAL_CLONE_SEMIHOSTING_H */
