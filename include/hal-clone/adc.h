/**
 * @file      adc.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Functions that can be used to configure the HAL clone ADC driver
 */

#ifndef MIST_HAL_CLONE_ADC_H
#define MIST_HAL_CLONE_ADC_H

int hal_clone_set_adc(int channel, unsigned short val);
int hal_clone_set_adc_all(unsigned short *vals);

#endif /* MIST_HAL_CLONE_ADC_H */
