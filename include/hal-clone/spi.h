/**
 * @file      spi.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Functions that can be used to configure the HAL clone SPI driver
 */

#ifndef MIST_HAL_CLONE_SPI_H
#define MIST_HAL_CLONE_SPI_H

#include <stdlib.h>
#include <stdint.h>

#include <hal/Drivers/SPI.h>

int hal_clone_spi_register_cb(SPIbus bus,
                              SPIslave slave,
                              int (*cb)(const uint8_t*, uint8_t*, size_t));

#endif /* MIST_HAL_CLONE_SPI_H */
