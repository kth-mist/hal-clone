/**
 * @file      semihosting_memory.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Higher level functions that can be used to emulate a memory device
 *            through ARM semihosting calls
 */

#ifndef MIST_HAL_CLONE_SEMIHOSTING_MEMORY_H
#define MIST_HAL_CLONE_SEMIHOSTING_MEMORY_H

#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>

int semihosting_memory_open(const char *pathname, ssize_t full_size, uint8_t default_value);
void semihosting_memory_close(int fd);
int semihosting_memory_fill(int fd, ssize_t full_size, uint8_t fill);
int semihosting_memory_read(int fd, off_t address, uint8_t *buf, size_t count);
int semihosting_memory_write(int fd, off_t address, const uint8_t *buf, size_t count);

#endif /* MIST_HAL_CLONE_SEMIHOSTING_MEMORY_H */
