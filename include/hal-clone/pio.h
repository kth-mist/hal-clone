/**
 * @file      pio.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     Functions that can be used to configure the HAL clone PIO driver
 */

#ifndef MIST_HAL_CLONE_PIO_H
#define MIST_HAL_CLONE_PIO_H

#include <hal-clone/common/gpio.h>

/* @brief Identifier for AT91 PIO kind of GPIOs, used by the HAL clone to differentiate
          it between other GPIO kinds. */
#define HAL_CLONE_GPIO_KIND_AT91   (9)

struct hal_clone_gpio hal_clone_at91_pio_to_gpio(const Pin *pin);

void hal_clone_at91_pio_connect_to_fixed(const Pin *pin, const char *name, enum hal_clone_gpio_value value);

#endif /* MIST_HAL_CLONE_PIO_H */
