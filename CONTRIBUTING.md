# HAL Clone contribution guide
This document lists how to add new code to the HAL clone.

## Adding new stubs
To add new stubs, start by placing a new file `<name>.c` under the `src` folder and implement each function in the
respective `<name>.h` ISIS HAL header file. If some functions will not be implemented, use the `PRINT_UNIMPLEMENTED`
macro defined in [`macros.h`](src/macros.h) to notify that an unimplemented function has been called. Once the stubs are finished,
it is recommended to add unit tests to test them, see **Adding test functions**.

## Adding test functions
All files mentioned from here on are located under the `test/tests` directory.

A _test function_ is a function of the following format, whose name is prefixed with _test_.
```c
int test_<name_of_function>(void);
```
The return value should be `false` if the test failed and `true` if the test was successful.
The macros defined in `test.h` should be used to print status messages and return whether or
not the test was successful. For more information, see [`test.h`](test/tests/test.h).
```c
#include <math.h>

// Must be included by all test source files
#include "test.h"

int test_example(void)
{
	int true = 1;
	ASSERT_TRUE(true);
	ASSERT_RETURNS(sqrt(81), 9);
	ASSERT_EQUALS(7, 3 + 4);
	
	TEST_SUCCESS;
}
```

After a test function has been added, it should be appended to the `int (*tests[])()` array
in [`main.c`](test/tests/main.c). Make sure that the last entry in the array is `NULL`.
```c
int (*tests[])() = {
	// More test functions here...
	
	test_example,
	NULL
};
```
