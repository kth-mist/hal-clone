# HAL Clone

Contains stubs for various ISIS HAL functions that can be used to build the OBCSW
to run in the QEMU emulator for testing purposes.

## Table of Contents
 - [1. Configuration](#1-configuration)
   - [1.1. Adding Peripheral Simulators](#11-adding-peripheral-simulators)
 - [2. Running Tests](#2-running-tests)
   - [2.1. Running Tests Using The ISIS HAL](#21-running-tests-using-the-isis-hal)
 - [3. Common Errors](#3-common-errors)

## 1. Configuration
This section will explain how to configure the HAL clone.

### 1.1. Adding Peripheral Simulators
I2C peripheral simulators that can be communicated with using the HAL clone's [I2C stub](src/I2C.c), can be registered
using the `hal_clone_i2c_register_cb` function by providing a callback to the simulator's I2C read- and write handler function
and its address. Under the [`include`](include) folder, there are a number of stub headers for Arduino simulators that can
be used to ensure compatability with the HAL clone. Similarily, SPI peripherals simulators can be registered using
the `hal_clone_spi_register_cb` function, except only one callback for write-read is needed instead of separate ones
for read and write.
PIO simulators can also be registered using the `hal_clone_pio_register_cb` function.

## 2. Running Tests
All commands mentioned below should be run in the `test` directory.

To build the test binary, run `make bin`.
This requires the following programs installed on a Windows machine or a Linux machine with WINE installed.
 - **make** (Using Windows Subsystem for Linux, MSYS2, Cygwin, or another Linux-like environment.)
 - **ISIS Eclipse** (Installed in C:\ISIS\application\eclipse, and can be installed using WINE on Linux)

Note that it is also possible to build a docker image that contains all these dependencies, see [3.2. Running Tests using GitLab CI](README.md#32-running-tests-using-gitlab-ci)
in the main README file, which also makes it possible to build and run the tests on Linux machines. However, these is currently no convenient
way to run the HAL clone tests outside of gitlab runner since that is what it was designed for.
Building the binary directly in Eclipse is not recommended as it requires some moving and deleting of files beforehand.
A link to the ISIS SDK installer can be found in the dropbox under "M153 - SW Development Tools".
Use the default settings to install Eclipse in the above mentioned folder.

**Note**: The ISIS HAL, or `isis-sdk` repository, will be cloned to the `test` directory if not present.

To run the test binary, run `make test`.
This requires the following programs installed.
 - **make** (Using Windows Subsystem for Linux or another environment that supports the SRC OBC QEMU dependencies mentioned below.)
 - **[SRC OBC QEMU](https://git.ksat-stuttgart.de/source/obc-qemu)**

**Note**: SRC OBC QEMU will be cloned and configured in the `test` directory if not present.
You must install SRC OBC QEMU's dependencies beforehand using the following command.
```sh
sudo apt-get install build-essential cmake libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev
```
Furthermore, if you already have SRC OBC QEMU installed in another directory, use `ln -s /your/path/to/obc-qemu obc-qemu`
to create a symbolic link to it. (You cannot do this for the ISIS HAL, as linux style symbolic links are not understood by Eclipse.)

To build *and* run the test binary, run `make`.

### 2.1. Running Tests Using The ISIS HAL
It is also possible to run the tests using the authentic ISIS HAL instead of the HAL clone.
This could be useful for testing how the real HAL behaves, but expect many of the existing tests
to fail and even hang QEMU since the ISIS HAL expects there to be an external RTC and other devices.
These aren't present in SRC OBC QEMU.

To build *and* run the test binary using the ISIS HAL, run `make hal`.

## 3. Common Errors
 - If the Makefile cannot find `arm-none-eabi-gcc` during execution of `make bin`, make sure that
  `C:\ISIS\application\toolchain\bin` is appended to your path. This can be done using for example
  `export PATH=/your/path/to/toolchain/bin:$PATH`
 - If you changed a compiler flag in the test eclipse project, but can't see the effects of your changes
   after running `make`, run `make clean` first.
