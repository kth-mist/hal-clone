# HAL Clone

Contains stubs for various ISIS HAL functions that can be used to build iOBC software
to run in the QEMU emulator for testing purposes.

For a README more specific for the MIST project see [here](README-MIST.md)

## Table of Contents
 - [1. Configuration](#1-configuration)
   - [1.1. Adding Peripheral Simulators](#11-adding-peripheral-simulators)
 - [2. How To Use](#2-how-to-use)
 - [3. Running Tests](#3-running-tests)
   - [3.1. Running Tests Using The ISIS HAL](#31-running-tests-using-the-isis-hal)
   - [3.2. Running Tests using GitLab CI](#32-running-tests-using-gitlab-ci)
 - [4. Common Errors](#4-common-errors)

## 1. Configuration
This section will explain how to configure the HAL clone to be used by any iOBC software. To do this, start by
forking this repository.

### 1.1. Adding peripheral simulators
I2C peripheral simulators that can be communicated with using the HAL clone's [I2C stub](src/I2C.c), can be registered
using the `hal_clone_i2c_register_cb` function by providing a callback to the simulator's I2C read- and write handler function
and its address. Under the [`include`](include) folder, there are a number of stub headers for Arduino simulators that can
be used to ensure compatability with the HAL clone. Similarily, SPI peripherals simulators can be registered using
the `hal_clone_spi_register_cb` function, except only one callback for write-read is needed instead of separate ones
for read and write.
PIO simulators can also be registered using the `hal_clone_pio_register_cb` function.

## 2. How To Use
 1. Add this repository, optionally as a submodule, to your iOBC software repository
    Eclipse project. Make sure that the ISIS HAL is added as well.
 2. Open your project in Eclipse, right click it, and choose _Properties_. Then select _C/C++ General_,
    followed by _Paths and Symbols_.
 3. Choose a build configuration that will be used to compile the OBCSW using the HAL clone.
 4. Click _Includes_ and add an include to `/path/to/hal-clone/include`. Make sure that all headers
    from the ISIS HAL are included as well. See figure 1.
	
    <img src="img/fig1.png" alt="Figure 1"/>
 5. In _Libraries_ and _Library Paths_ make sure that the libraries `HALD` and `HCCD` are not included.
    All other ISIS libraries should be included from the ISIS HAL, see figures 2 and 3.
	
    <img src="img/fig2.png" alt="Figure 2"/>
	
    <img src="img/fig3.png" alt="Figure 3"/>
 6. In _Source Location_ add a path to `/path/to/hal-clone/src`

## 3. Running Tests
All commands mentioned below should be run in the `test` directory.

To build the test binary, run `make bin`.
This requires the following programs installed on a Windows machine or a Linux machine with WINE installed.
 - **make** (Using Windows Subsystem for Linux, MSYS2, Cygwin, or another Linux-like environment.)
 - **ISIS Eclipse** (Installed in C:\ISIS\application\eclipse, and can be installed using WINE on Linux)

Note that it is also possible to build a docker image that contains all these dependencies, see [3.2. Running Tests using GitLab CI](#32-running-tests-using-gitlab-ci),
which also makes it possible to build and run the tests on Linux machines. However, these is currently no convenient
way to run the HAL clone tests outside of gitlab runner since that is what it was designed for.
Building the binary directly in Eclipse is not recommended as it requires some moving and deleting of files beforehand.
When installing Eclipse, use the default settings to install it in the above mentioned folder.

**Note**: Please update the `ISIS-HAL-LINK` variable at the top of the [`Makefile`](test/Makefile) to your own ISIS-HAL
repository. If you do not have such a repository, you should add the ISIS HAL to the `test` directory manually before running
`make bin`. To clarify, it must have the folder name `isis-sdk` and if it contains the folders `adcs`, `hal`, `mission-suppport`,
etc., then you can be certain that you have added it correctly.

To run the test binary, run `make test`.
This requires the following programs installed.
 - **make** (Using Windows Subsystem for Linux or another environment that supports the SRC OBC QEMU dependencies mentioned below.)
 - **[SRC OBC QEMU](https://git.ksat-stuttgart.de/source/obc-qemu)**

**Note**: SRC OBC QEMU will be cloned and configured in the `test` directory if not present.
You must install SRC OBC QEMU's dependencies beforehand using the following command.
```sh
sudo apt-get install build-essential cmake libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev
```
Furthermore, if you already have SRC OBC QEMU installed in another directory, use `ln -s /your/path/to/obc-qemu obc-qemu`
to create a symbolic link to it. (You cannot do this for the ISIS HAL, as linux style symbolic links are not understood by Eclipse.)

To build *and* run the test binary, run `make`.

### 3.1. Running Tests Using The ISIS HAL
It is also possible to run the tests using the authentic ISIS HAL instead of the HAL clone.
This could be useful for testing how the real HAL behaves, but expect many of the existing tests
to fail and even hang QEMU since the ISIS HAL expects there to be an external RTC and other devices.
These aren't present in SRC OBC QEMU.

To build *and* run the test binary using the ISIS HAL, run `make hal`.

### 3.2. Running Tests using GitLab CI
To run the tests using [`.gitlab-ci.yml`](.gitlab-ci.yml), you need to host your own GitLab runner instance with
the [`Dockerfile`](Dockerfile). The built image is based on Ubuntu 18.04 and uses WINE to install and run the
ISIS toolchain. To build it, you need to place the ISIS SDK installer in the same directory as the dockerfile
and rename it to `ISIS-OBC-SDK.exe`. The dockerfile can then be built using the following command.
(You could probably move the dockerfile and ISIS SDK installer to another folder to reduce the build context)
```sh
docker build -t wstamist/hal-tests .
```
You should then [install](https://docs.gitlab.com/runner/install/linux-manually.html) gitlab runner on your host and
[register](https://docs.gitlab.com/runner/register/index.html#linux) it using the docker executor in your forked
repository. You will also need to define a public and private SSH key pair and add them to the `Settings->CI/CD->Variables`
section to allow the runner to clone your ISIS HAL repository. The variables are expected to be called
`MIST_SHARED_PUBLIC_KEY` and `MIST_SHARED_PRIVATE_KEY` in `.gitlab-ci.yml`, which can be changed.

## 4. Common Errors
 - If the Makefile cannot find `arm-none-eabi-gcc` during execution of `make bin`, make sure that
  `C:\ISIS\application\toolchain\bin` is appended to your path. This can be done using for example
  `export PATH=/your/path/to/toolchain/bin:$PATH`
 - If you changed a compiler flag in the test eclipse project, but can't see the effects of your changes
   after running `make`, run `make clean` first.
