#ifndef TIME_TEST_H_
#define TIME_TEST_H_

int test_time_set_get_epoch(void);
int test_set_epoch_get(void);
int test_set_edge_case(void);
int test_time_diff(void);
int test_rtc_tick(void);

#endif /* TIME_TEST_H_ */
