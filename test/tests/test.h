// Macros used by testing functions

#ifndef TEST_H_
#define TEST_H_

#define SUCCESS		1
#define	FAILURE		0

/**
 * @brief Fails the test if cond is true
 */
#define ASSERT_FALSE(cond) \
    if (cond) { \
        fprintf(stderr, "[FAILED] %s: %s was not false at %s:%d\n", __FUNCTION__, #cond, __FILE__, __LINE__); \
        return FAILURE; \
    }

/**
 * @brief Fails the test if cond is false
 */
#define ASSERT_TRUE(cond) \
    if (!(cond)) { \
        fprintf(stderr, "[FAILED] %s: %s was not true at %s:%d\n", __FUNCTION__, #cond, __FILE__, __LINE__); \
        return FAILURE; \
    }
/**
 * @brief Fails the test if a is not equal to b
 */
#define ASSERT_EQUAL(a, b) \
    if ((a) != (b)) { \
        fprintf(stderr, "[FAILED] %s: %s (%d) is not equal to %s (%d) at %s:%d\n", __FUNCTION__, #a, (int)a, #b, (int)b, __FILE__, __LINE__); \
        return FAILURE; \
    }

/**
 * @brief Fails the test if the function f does not return b
 */
#define ASSERT_RETURNS(f, b) \
    { \
        int a = f; \
        if ((a) != (b)) { \
            fprintf(stderr, "[FAILED] %s: %s returned %d at %s:%d\n", __FUNCTION__, #f, (int)a, __FILE__, __LINE__); \
            return FAILURE; \
        } \
    }

/**
 * @brief Passes the test
 */
#define TEST_SUCCESS \
    fprintf(stderr, "[SUCCESS] %s\n", __FUNCTION__); \
    return SUCCESS;

/**
 * @brief Prints the contents in bytes of array arr
 *        with length len, separated by commas.
 */
#define PRINT_ARRAY(arr, len) \
    fprintf(stderr, "%s: ", #arr); \
    if ((len) > 0) { \
        size_t iter; \
        fprintf(stderr, "0x%02X", (((uint8_t*)(arr))[0] & 0xFF)); \
        for (iter = 1; iter < (len); iter++) { \
            fprintf(stderr, ", 0x%02X", (((uint8_t*)(arr))[iter] & 0xFF)); \
        } \
    } \
    fprintf(stderr, "\n");

#endif /* TEST_H_ */
