#include <stdio.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <hal/Timing/Time.h>
#include "test.h"

int test_time_set_get_epoch(void)
{
	unsigned int epoch;
	Time t;
	t.seconds = 10;
	t.minutes = 10;
	t.hours = 10;
	t.date = 10;
	t.day = 1;
	t.month = 10;
	t.year = 10;

	ASSERT_RETURNS(Time_start(NULL, 60), 0);
	ASSERT_RETURNS(Time_set(&t), 0);
	ASSERT_RETURNS(Time_getUnixEpoch(&epoch), 0);
	ASSERT_EQUAL(epoch, 1286705411);

	TEST_SUCCESS;
}

int test_set_epoch_get(void)
{
	unsigned int epoch = 1010101010;
	Time t;
	ASSERT_RETURNS(Time_setUnixEpoch(epoch), 0);
	ASSERT_RETURNS(Time_get(&t), 0);
	ASSERT_EQUAL(t.seconds, 51);
	ASSERT_EQUAL(t.minutes, 36);
	ASSERT_EQUAL(t.hours, 23);
	ASSERT_EQUAL(t.day, 5); // Thursday
	ASSERT_EQUAL(t.date, 3);
	ASSERT_EQUAL(t.month, 1);
	ASSERT_EQUAL(t.year, 2);
	ASSERT_EQUAL(t.secondsOfYear, 1010101011 - 1009843200);

	TEST_SUCCESS;
}

int test_set_edge_case(void)
{
	unsigned int epoch = 0;
	Time t;
	t.seconds = 0;
	t.minutes = 0;
	t.hours = 0;
	t.date = 1;
	t.day = 0;
	t.month = 1;
	t.year = 0;

	ASSERT_RETURNS(Time_setUnixEpoch(epoch), 1);
	ASSERT_RETURNS(Time_set(NULL), 1);
	ASSERT_RETURNS(Time_set(&t), 1);

	t.day = 1;
	t.year = 107;
	ASSERT_RETURNS(Time_set(&t), 1);

	TEST_SUCCESS;
}

int test_time_diff(void)
{
	unsigned int e1 = 0xABCD1234;
	unsigned int e2 = e1 - 7;
	Time t1, t2;
	ASSERT_RETURNS(Time_setUnixEpoch(e1), 0);
	ASSERT_RETURNS(Time_get(&t1), 0);
	ASSERT_RETURNS(Time_setUnixEpoch(e2), 0);
	ASSERT_RETURNS(Time_get(&t2), 0);
	ASSERT_EQUAL(Time_diff(&t1, &t2), 7);
	ASSERT_RETURNS(Time_diff(&t2, &t1), TIMEDIFF_INVALID);

	t1.secondsOfYear = 1606746600 - 1577836800;
	t1.year = 20;
	t2.secondsOfYear = 100;
	t2.year = 0;
	// TODO: The ISIS HAL returned an error here, which may be due
	// to secondsOfYear not matching the other fields? Not sure.
	ASSERT_EQUAL(Time_diff(&t1, &t2), 1606746600 - 946684900);

	TEST_SUCCESS;
}

int test_rtc_tick(void)
{
	unsigned int e1 = 0x5A5A5A5A;
	unsigned int e2 = 0;
	ASSERT_RETURNS(Time_setUnixEpoch(e1), 0);

	// A little more than one second to make sure the RTC has ticked.
	vTaskDelay(1050 / portTICK_RATE_MS);
	ASSERT_RETURNS(Time_getUnixEpoch(&e2), 0);
	// Compare to two seconds since the HAL returns the RTC value plus one.
	ASSERT_EQUAL(e2, e1 + 2);

	// Check that the RTC stops ticking once the maximum value is reached.
	ASSERT_RETURNS(Time_setUnixEpoch(0xF48656FF), 0);
	vTaskDelay(1050 / portTICK_RATE_MS);
	ASSERT_RETURNS(Time_getUnixEpoch(&e2), 0);
	ASSERT_EQUAL(e2, 0xF48656FF);

	TEST_SUCCESS;
}
