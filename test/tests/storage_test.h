#ifndef STORAGE_TEST_H_
#define STORAGE_TEST_H_

int test_hcc(void);
int test_fram_write_read(void);
int test_norflash_write_read(void);

#endif /* STORAGE_TEST_H_ */
