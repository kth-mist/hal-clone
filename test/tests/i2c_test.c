#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <hal/Drivers/I2C.h>
#include <hal-clone/i2c.h>
#include <hal-clone/error.h>

#include "test.h"

int i2c_35_write_cb(const uint8_t *data, size_t size)
{
	return strcmp((const char*) data, "Hello!");
}
int i2c_35_read_cb(uint8_t *data, size_t size)
{
	for (size_t i = 0; i < size - 1; i++)
		data[i] = i;
	data[size - 1] = 0xff;
	return 0;
}

static uint8_t i2c_4c_buf[10] = {0x56, 0x71, 0xfa, 0x1b, 0x05, 0xa8, 0x99, 0x5e, 0x37, 0xdd};
static uint8_t i2c_4c_index = 0;
int i2c_4c_write_cb(const uint8_t *data, size_t size)
{
	if (size != 1 || data[0] >= sizeof(i2c_4c_buf))
		return 1;

	i2c_4c_index = data[0];
	return 0;
}
int i2c_4c_read_cb(uint8_t *data, size_t size)
{
	if (size < 1)
		return 1;

	data[0] = i2c_4c_buf[i2c_4c_index];
	return 0;
}

int test_i2c_uninitialized(void)
{
	ASSERT_RETURNS(I2C_getDriverState(), uninitialized_i2cState);
	ASSERT_RETURNS(I2C_getCurrentTransferStatus(), done_i2c);

	ASSERT_RETURNS(I2C_start(400000, 1), 0);
	ASSERT_RETURNS(I2C_getDriverState(), idle_i2cState);
	// Test re-initialize
	ASSERT_RETURNS(I2C_start(400000, 1), 0);

	ASSERT_RETURNS(hal_clone_i2c_register_cb(0x83, i2c_35_write_cb, i2c_35_read_cb), HAL_CLONE_ERR_OOB);
	ASSERT_RETURNS(hal_clone_i2c_register_cb(0x35, NULL, i2c_35_read_cb), HAL_CLONE_ERR_NULL);
	ASSERT_RETURNS(hal_clone_i2c_register_cb(0x35, i2c_35_write_cb, NULL), HAL_CLONE_ERR_NULL);
	ASSERT_RETURNS(hal_clone_i2c_register_cb(0x35, i2c_35_write_cb, i2c_35_read_cb), 0);
	ASSERT_RETURNS(hal_clone_i2c_register_cb(0x4c, i2c_4c_write_cb, i2c_4c_read_cb), 0);

	TEST_SUCCESS;
}

int test_i2c_write(void)
{
	ASSERT_RETURNS(I2C_write(0, (unsigned char*)"test", 0), -2);
	ASSERT_RETURNS(I2C_write(0, NULL, 1), -2);

	ASSERT_RETURNS(I2C_write(127, (unsigned char*)"test", 7), writeError_i2c);
	ASSERT_RETURNS(I2C_getDriverState(), idle_i2cState);
	ASSERT_RETURNS(I2C_getCurrentTransferStatus(), writeError_i2c);

	ASSERT_RETURNS(I2C_write(0x35, (unsigned char*)"Goodbye!", 9), error_i2c);
	ASSERT_RETURNS(I2C_getDriverState(), idle_i2cState);
	ASSERT_RETURNS(I2C_getCurrentTransferStatus(), error_i2c);

	ASSERT_RETURNS(I2C_write(0x35, (unsigned char*)"Hello!", 7), done_i2c);
	ASSERT_RETURNS(I2C_getDriverState(), idle_i2cState);
	ASSERT_RETURNS(I2C_getCurrentTransferStatus(), done_i2c);

	TEST_SUCCESS;
}

int test_i2c_read(void)
{
	size_t i;
	unsigned char buf[10];

	ASSERT_RETURNS(I2C_read(0, buf, 0), -2);
	ASSERT_RETURNS(I2C_read(0, NULL, 1), -2);

	ASSERT_RETURNS(I2C_read(1, buf, sizeof(buf)), readError_i2c);
	ASSERT_RETURNS(I2C_getDriverState(), idle_i2cState);
	ASSERT_RETURNS(I2C_getCurrentTransferStatus(), readError_i2c);

	ASSERT_RETURNS(I2C_read(0x35, buf, sizeof(buf)), done_i2c);
	ASSERT_RETURNS(I2C_getDriverState(), idle_i2cState);
	ASSERT_RETURNS(I2C_getCurrentTransferStatus(), done_i2c);

	for (i = 0; i < sizeof(buf) - 1; i++) {
		ASSERT_EQUAL(buf[i], i);
	}
	ASSERT_EQUAL(buf[9], 0xFF);

	// TODO: Run another task and have both task try to read over I2C
	// at the same time to verify that the semaphore works.

	TEST_SUCCESS;
}

int test_i2c_write_read(void)
{
	unsigned char data[1] = {0x05};
	unsigned char recv[1] = {0};
	I2Ctransfer xfer;

	xfer.slaveAddress = 0x4c;
	xfer.writeSize = sizeof(data);
	xfer.writeData = data;
	xfer.readSize = sizeof(recv);
	xfer.readData = recv;
	xfer.writeReadDelay = 0;

	ASSERT_RETURNS(I2C_writeRead(&xfer), done_i2c);
	ASSERT_EQUAL(recv[0], 0xa8);

	data[0] = 0x07;
	ASSERT_RETURNS(I2C_writeRead(&xfer), done_i2c);
	ASSERT_EQUAL(recv[0], 0x5e);

	data[0] = 0x10;
	ASSERT_RETURNS(I2C_writeRead(&xfer), error_i2c);

	xfer.writeSize = 0;
	recv[0] = 0;
	ASSERT_RETURNS(I2C_writeRead(&xfer), done_i2c);
	ASSERT_EQUAL(recv[0], 0x5e);

	xfer.readSize = 0;
	ASSERT_RETURNS(I2C_writeRead(&xfer), -2);

	TEST_SUCCESS;
}
