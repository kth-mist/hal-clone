#ifndef ARDUINO_TESTS_H_
#define ARDUINO_TESTS_H_

#ifdef __cplusplus
extern "C" {
#endif

int test_arduino_millis(void);
int test_arduino_pin_write_read(void);
int test_arduino_pin_interrupt(void);

int test_arduino_timer(void);
int test_arduino_rstc(void);
int test_arduino_DueFlashStorage(void);
int test_arduino_DueFlashStorage_semihosting(void);

#ifdef __cplusplus
}
#endif

#endif /* ARDUINO_TESTS_H_ */
