extern "C" {
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>

	#include <Arduino.h>
	#include <include/rstc.h>

	#include <hal-clone/semihosting.h>
	#include <hal-clone/arduino.h>

	#include <freertos/FreeRTOS.h>
	#include <freertos/task.h>

	#include "../test.h"

	#include "tests.h"
}

#include <arduino-timer.h>
#include <DueFlashStorage.h>

#define ARDUINO_UNIT_ID_DUMMY  (39)
#define ARDUINO_UNIT_ID  ARDUINO_UNIT_ID_DUMMY


static bool _isr_called = false;
static void isr_called(void)
{
	_isr_called = true;
}
static bool get_isr_called(void)
{
	return _isr_called;
}

static bool _timer_in_cb_toggle = false;
static bool timer_in_cb(void* args)
{
	(void) args;

	_timer_in_cb_toggle = !_timer_in_cb_toggle;

	return true;
}
static bool _rstc_cb_called = false;
static void rstc_cb(void)
{
	_rstc_cb_called = true;
}

int test_arduino_millis(void)
{
	unsigned long m = millis();
	vTaskDelay(2 / portTICK_RATE_MS);
	ASSERT_TRUE(m < millis());

	TEST_SUCCESS;
}

int test_arduino_pin_write_read(void)
{
	hal_clone_gpio_connect(hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_DUMMY, 37), "ARDUINO_OUTPUT",
	                       hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_DUMMY, 49), "ARDUINO_INPUT");

	pinMode(37, OUTPUT);
	pinMode(49, INPUT);

	digitalWrite(37, LOW);
	ASSERT_RETURNS(digitalRead(37), LOW);
	ASSERT_RETURNS(digitalRead(49), LOW);

	digitalWrite(37, HIGH);
	ASSERT_RETURNS(digitalRead(37), HIGH);
	ASSERT_RETURNS(digitalRead(49), HIGH);

	hal_clone_gpio_connect_to_fixed(hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_DUMMY, 15),
	                                "ARDUINO_FIXED_INPUT",
	                                HAL_CLONE_GPIO_LOW);

	pinMode(15, INPUT);
	ASSERT_RETURNS(digitalRead(15), LOW);

	TEST_SUCCESS;
}

int test_arduino_pin_interrupt(void)
{
	hal_clone_gpio_connect(hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_DUMMY, 21), "ARDUINO_ISR_OUTPUT",
	                       hal_clone_arduino_pin_to_gpio(ARDUINO_UNIT_ID_DUMMY, 34), "ARDUINO_ISR_INPUT");

	pinMode(21, OUTPUT);
	pinMode(34, INPUT);

	hal_clone_arduino_set_output_isr(ARDUINO_UNIT_ID_DUMMY, 21, get_isr_called);
	attachInterrupt(digitalPinToInterrupt(34), isr_called, RISING);

	// Only call ISR on rising
	digitalWrite(21, LOW);
	ASSERT_FALSE(_isr_called);

	digitalWrite(21, HIGH);
	ASSERT_TRUE(_isr_called);

	_isr_called = false;
	ASSERT_RETURNS(digitalRead(34), LOW);

	_isr_called = true;
	ASSERT_RETURNS(digitalRead(34), HIGH);

	// Should not call ISR when reading our output
	_isr_called = false;
	ASSERT_RETURNS(digitalRead(21), HIGH);

	TEST_SUCCESS;
}

int test_arduino_timer(void)
{
	Timer<> t;

	t.in(10, timer_in_cb);

	ASSERT_FALSE(_timer_in_cb_toggle);
	vTaskDelay(12 / portTICK_RATE_MS);
	ASSERT_TRUE(_timer_in_cb_toggle);
	vTaskDelay(12 / portTICK_RATE_MS);
	ASSERT_TRUE(_timer_in_cb_toggle);

	t.every(10, timer_in_cb);

	ASSERT_TRUE(_timer_in_cb_toggle);
	vTaskDelay(12 / portTICK_RATE_MS);
	ASSERT_FALSE(_timer_in_cb_toggle);
	vTaskDelay(12 / portTICK_RATE_MS);
	ASSERT_TRUE(_timer_in_cb_toggle);

	t.cancel();

	vTaskDelay(12 / portTICK_RATE_MS);
	ASSERT_TRUE(_timer_in_cb_toggle);

	TEST_SUCCESS;
}

int test_arduino_rstc(void)
{
	hal_clone_arduino_rstc_regsiter_cb(ARDUINO_UNIT_ID_DUMMY, rstc_cb);

	rstc_start_software_reset(RSTC);

	ASSERT_TRUE(_rstc_cb_called);

	TEST_SUCCESS;
}

int test_arduino_DueFlashStorage(void)
{
	DueFlashStorage fs;
	uint8_t flash_data[10] = { 0 };
	uint8_t write_data[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	hal_clone_arduino_DueFlashStorage_set_in_memory(ARDUINO_UNIT_ID_DUMMY, flash_data, 0xCC000, 10);

	// Memory should be initialized to 0xFF
	ASSERT_RETURNS(fs.read(0xCC003), 0xFF);

	ASSERT_TRUE(fs.write(0xCC000, 0x42));
	ASSERT_RETURNS(fs.read(0xCC000), 0x42);

	ASSERT_TRUE(fs.write(0xCC000, write_data, sizeof(write_data)));

	ASSERT_EQUAL(fs.readAddress(0xCC001), flash_data + 1);

	ASSERT_RETURNS(memcmp(flash_data, write_data, sizeof(write_data)), 0);

	TEST_SUCCESS;
}

int test_arduino_DueFlashStorage_semihosting(void)
{
	DueFlashStorage fs;
	uint8_t write_data[10] = { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0xA };

	semihosting_remove("DueFlashStorage.tmp");
	hal_clone_arduino_DueFlashStorage_set_semihosting_file(ARDUINO_UNIT_ID_DUMMY, "DueFlashStorage.tmp", 0xCC000, 10);

	// Memory should be initialized to 0xFF
	ASSERT_RETURNS(fs.read(0xCC003), 0xFF);

	ASSERT_TRUE(fs.write(0xCC000, 0x42));
	ASSERT_RETURNS(fs.read(0xCC000), 0x42);

	ASSERT_TRUE(fs.write(0xCC000, write_data, sizeof(write_data)));


	ASSERT_RETURNS(memcmp(fs.readAddress(0xCC000), write_data, sizeof(write_data)), 0);

	TEST_SUCCESS;
}
