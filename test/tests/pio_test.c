#include <stdio.h>
#include <stdlib.h>

#include <at91/boards/ISIS_OBC_G20/board.h>
#include <at91/peripherals/pio/pio.h>

#include <hal-clone/pio.h>

#include "test.h"

static Pin _pin_conf[] = {
	PIN_GPIO01,
	PIN_GPIO16,
	PIN_GPIO20,
	PIN_GPIO14,
};

int test_pio_configuration(void)
{
	_pin_conf[0].type = PIO_INPUT;
	_pin_conf[1].type = PIO_OUTPUT_1;
	_pin_conf[2].type = PIO_OUTPUT_0;
	_pin_conf[3].type = PIO_INPUT;

	hal_clone_gpio_connect(hal_clone_at91_pio_to_gpio(&_pin_conf[0]), "GPIO01",
	                       hal_clone_at91_pio_to_gpio(&_pin_conf[1]), "GPIO16");
	hal_clone_gpio_connect_to_fixed(hal_clone_at91_pio_to_gpio(&_pin_conf[2]), "GPIO20",
	                                HAL_CLONE_GPIO_UNDEFINED);
	hal_clone_gpio_connect_to_fixed(hal_clone_at91_pio_to_gpio(&_pin_conf[3]), "GPIO14",
	                                HAL_CLONE_GPIO_HIGH);

	ASSERT_RETURNS(PIO_Configure(_pin_conf, PIO_LISTSIZE(_pin_conf)), 1);

	// Test connecting INPUT pin to HIGH
	ASSERT_RETURNS(PIO_Get(&_pin_conf[3]), 1);
	ASSERT_RETURNS(PIO_GetOutputDataStatus(&_pin_conf[3]), 1);

	// Test that type OUTPUT_0 sets the pin to low when configured
	ASSERT_RETURNS(PIO_Get(&_pin_conf[2]), 0);
	ASSERT_RETURNS(PIO_GetOutputDataStatus(&_pin_conf[2]), 0);

	// Test that type OUTPUT_1 sets the pin to high when configured
	ASSERT_RETURNS(PIO_Get(&_pin_conf[0]), 1);
	ASSERT_RETURNS(PIO_GetOutputDataStatus(&_pin_conf[0]), 1);
	ASSERT_RETURNS(PIO_Get(&_pin_conf[1]), 1);
	ASSERT_RETURNS(PIO_GetOutputDataStatus(&_pin_conf[1]), 1);

	TEST_SUCCESS;
}

int test_pio_set(void)
{
	PIO_Set(&_pin_conf[2]);

	ASSERT_RETURNS(PIO_Get(&_pin_conf[2]), 1);
	ASSERT_RETURNS(PIO_GetOutputDataStatus(&_pin_conf[2]), 1);

	PIO_Clear(&_pin_conf[1]);

	ASSERT_RETURNS(PIO_Get(&_pin_conf[0]), 0);
	ASSERT_RETURNS(PIO_GetOutputDataStatus(&_pin_conf[0]), 0);
	ASSERT_RETURNS(PIO_Get(&_pin_conf[1]), 0);
	ASSERT_RETURNS(PIO_GetOutputDataStatus(&_pin_conf[1]), 0);

	TEST_SUCCESS;
}
