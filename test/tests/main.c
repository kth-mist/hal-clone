// Main test framework file

#include <stdio.h>

#include <at91/peripherals/cp15/cp15.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>

#include <hal/boolean.h>

#include <hal-clone/semihosting.h>

#include "test.h"
#include "time_test.h"
#include "i2c_test.h"
#include "spi_test.h"
#include "adc_test.h"
#include "storage_test.h"
#include "pio_test.h"
#include "semihosting_test.h"

#include "arduino_shim/tests.h"

// TODO: Add your test functions here...
// test_rtc_tick in time_test.c has been omitted since it slows down the framework.
int (*tests[])() = {
	test_time_set_get_epoch,
	test_set_epoch_get,
	test_set_edge_case,
	test_time_diff,
	test_hcc,
	test_fram_write_read,
	test_norflash_write_read,
	test_i2c_uninitialized,
	test_i2c_write,
	test_i2c_read,
	test_i2c_write_read,
	test_adc_initialize,
	test_adc_shot,
	test_spi_uninitialized,
	test_spi_write_read,
	test_pio_configuration,
	test_pio_set,
	test_semihosting_elapsed,
	test_semihosting_halt,

	test_arduino_millis,
	test_arduino_pin_write_read,
	test_arduino_pin_interrupt,
	test_arduino_timer,
	test_arduino_rstc,
	test_arduino_DueFlashStorage,
	test_arduino_DueFlashStorage_semihosting,
	NULL
};

void test()
{
	fprintf(stderr, "--- TEST START ---\n");
	Boolean test_failure = FALSE;
	int i;
	for (i = 0; (*tests[i]) != NULL; i++){
		if (!(*tests[i])()) {
			test_failure = TRUE;
		}
	}
	if (test_failure) {
		fprintf(stderr, "--- TEST FAILURE ---\n");
		semihosting_exit(ADP_Stopped_InternalError);
	}
	fprintf(stderr, "--- ALL TESTS OK ---\n");
	semihosting_exit(ADP_Stopped_ApplicationExit);
}

int main()
{
	TRACE_CONFIGURE_ISP(DBGU_STANDARD, 2000000, BOARD_MCK);
	// Enable the Instruction cache of the ARM9 core. Keep the MMU and Data Cache disabled.
	CP15_Enable_I_Cache();

	xTaskHandle testTask;
	xTaskGenericCreate(test, (const signed char*)"test", 16384, NULL, 0, &testTask, NULL, NULL);
	vTaskStartScheduler();

	// Should never reach this
	fprintf(stderr, "--- FREERTOS FAILURE ---\n");
	semihosting_exit(ADP_Stopped_InternalError);
	return 0;
}
