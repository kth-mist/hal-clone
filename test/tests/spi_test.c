#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <at91/commons.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>

#include <hal/Drivers/SPI.h>
#include <hal-clone/spi.h>
#include <hal-clone/error.h>

#include "test.h"

int spi_slave1_cb(const uint8_t *write, uint8_t *read, size_t size)
{
	for (size_t i = 0; i < size; i++)
		read[size - 1 - i] = write[i];
	return 0;
}

static uint8_t spi_slave3_buf[10] = {0x56, 0x71, 0xfa, 0x1b, 0x05, 0xa8, 0x99, 0x5e, 0x37, 0xdd};
int spi_slave3_cb(const uint8_t *write, uint8_t *read, size_t size)
{
	if (size != 1 || write[0] >= sizeof(spi_slave3_buf))
		return 1;

	read[0] = spi_slave3_buf[write[0]];
	return 0;
}

void spi_dummy_callback(SystemContext context, xSemaphoreHandle sem)
{
	(void) context;
	(void) sem;

	return;
}

int test_spi_uninitialized(void)
{
	ASSERT_RETURNS(SPI_getDriverState(bus1_spi), uninitialized_spiState);

	ASSERT_RETURNS(SPI_start(bus1_spi, slave4_spi), 0);
	ASSERT_RETURNS(SPI_getDriverState(bus1_spi), idle_spiState);
	// Test re-initialize
	ASSERT_RETURNS(SPI_start(bus1_spi, slave4_spi), 0);

	ASSERT_RETURNS(hal_clone_spi_register_cb(bus1_spi, 8, spi_slave1_cb), HAL_CLONE_ERR_OOB);
	ASSERT_RETURNS(hal_clone_spi_register_cb(bus1_spi, slave2_spi, NULL), HAL_CLONE_ERR_NULL);
	ASSERT_RETURNS(hal_clone_spi_register_cb(bus1_spi, slave1_spi, spi_slave1_cb), 0);
	ASSERT_RETURNS(hal_clone_spi_register_cb(bus1_spi, slave3_spi, spi_slave3_cb), 0);

	TEST_SUCCESS;
}

int test_spi_write_read(void)
{
	unsigned char data[5] = {0x06, 0x07, 0x08, 0x09, 0x0A};
	unsigned char recv[5] = {0};
	SPIslaveParameters params;
	SPItransfer xfer;

	params.bus = bus1_spi;
	params.mode = mode0_spi; // I'm not sure what this is, but the HAL clone ignores it anyway
	params.slave = slave1_spi;
	params.dlybs = 0;
	params.dlybct = 0;
	params.busSpeed_Hz = SPI_MAX_BUS_SPEED;
	// Skipping setting postTransferDelay

	xfer.transferSize = sizeof(data);
	xfer.writeData = data;
	xfer.readData = recv;
	xfer.slaveParams = &params;
	// Skipping setting semaphore
	xfer.callback = spi_dummy_callback;

	ASSERT_RETURNS(SPI_writeRead(&xfer), 0);
	ASSERT_EQUAL(recv[0], 0x0A);
	ASSERT_EQUAL(recv[1], 0x09);
	ASSERT_EQUAL(recv[2], 0x08);
	ASSERT_EQUAL(recv[3], 0x07);
	ASSERT_EQUAL(recv[4], 0x06);

	params.slave = slave3_spi;
	ASSERT_RETURNS(SPI_writeRead(&xfer), -1);

	xfer.transferSize = 1;
	ASSERT_RETURNS(SPI_writeRead(&xfer), 0);
	ASSERT_EQUAL(recv[0], 0x99);

	data[0] = 0x02;
	ASSERT_RETURNS(SPI_writeRead(&xfer), 0);
	ASSERT_EQUAL(recv[0], 0xfa);

	data[0] = 0x10;
	ASSERT_RETURNS(SPI_writeRead(&xfer), -1);

	xfer.transferSize = 0;
	ASSERT_RETURNS(SPI_writeRead(&xfer), -3);

	xfer.transferSize = 1;
	params.busSpeed_Hz = 0;
	ASSERT_RETURNS(SPI_writeRead(&xfer), -2);

	TEST_SUCCESS;
}
