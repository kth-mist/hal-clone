#include <stdio.h>
#include <stdint.h>

#include <hal-clone/semihosting.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "test.h"

int test_semihosting_elapsed(void)
{
	int64_t last_elapsed = 0;
	int64_t elapsed;

	for (int i = 0; i < 10; i++) {
		elapsed = semihosting_elapsed();
		ASSERT_TRUE(elapsed > last_elapsed);
		last_elapsed = elapsed;
	}

	TEST_SUCCESS;
}

int test_semihosting_halt(void)
{
	// TODO

	TEST_SUCCESS;
}
