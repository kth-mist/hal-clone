#ifndef SPI_TEST_H_
#define SPI_TEST_H_

int test_spi_uninitialized(void);
int test_spi_write_read(void);

#endif /* SPI_TEST_H_ */
