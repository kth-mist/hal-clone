#ifndef ADC_TEST_H_
#define ADC_TEST_H_

int test_adc_initialize(void);
int test_adc_shot(void);

#endif /* ADC_TEST_H_ */
