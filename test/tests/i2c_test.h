#ifndef I2C_TEST_H_
#define I2C_TEST_H_

int test_i2c_uninitialized(void);
int test_i2c_write(void);
int test_i2c_read(void);
int test_i2c_write_read(void);

#endif /* I2C_TEST_H_ */
