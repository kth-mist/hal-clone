#include <stdio.h>
#include <string.h>

#include <hal/boolean.h>
#include <hal/Drivers/ADC.h>

#include <hal-clone/adc.h>
#include <hal-clone/error.h>

#include "test.h"


static void dummy_cb(SystemContext context, void* adcSamples)
{
	return; // Do nothing...
}

int test_adc_initialize(void)
{
	ADCparameters init_params;
	init_params.sampleRate = 1;
	init_params.resolution10bit = TRUE;
	init_params.channels = 8;
	init_params.samplesInBufferPerChannel = 2048;
	init_params.callback = dummy_cb;

	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.sampleRate = 75001;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.resolution10bit = FALSE;
	init_params.sampleRate = 250001;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.sampleRate = 250000;
	init_params.channels = 0;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.channels = 5;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.channels = 16;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.channels = 4;
	init_params.samplesInBufferPerChannel = 111111;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.resolution10bit = TRUE;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.samplesInBufferPerChannel = 1111;
	init_params.sampleRate = 32000;
	init_params.callback = NULL;
	ASSERT_RETURNS(ADC_start(init_params), -2);
	init_params.callback = dummy_cb;
	ASSERT_RETURNS(ADC_start(init_params), 0);

	ADC_stop();

	TEST_SUCCESS;
}

int test_adc_shot(void)
{
	int i;
	unsigned short samples[8];
	ADCparameters init_params;

	// Fill with dummy data
	for (i = 0; i < 8; i++)
		samples[i] = 0xFFFF;

	init_params.sampleRate = 32000;
	init_params.resolution10bit = FALSE;
	init_params.channels = 4;
	init_params.samplesInBufferPerChannel = 2048;
	init_params.callback = dummy_cb;

	ADC_stop();
	ASSERT_RETURNS(ADC_SingleShot(NULL), -2);
	// Initialize with 8-bit and 4 channels
	ASSERT_RETURNS(ADC_start(init_params), 0);
	ASSERT_RETURNS(hal_clone_set_adc(1, 208), 0);
	ASSERT_RETURNS(hal_clone_set_adc(3, 244), 0);
	ASSERT_RETURNS(hal_clone_set_adc(6, 103), HAL_CLONE_ERR_OOB);
	ASSERT_RETURNS(hal_clone_set_adc(2, 11103), HAL_CLONE_ERR_OOB);
	ASSERT_RETURNS(ADC_SingleShot(samples), 0);

	ASSERT_FALSE(samples[0] > 0xFF);
	ASSERT_EQUAL(samples[1], 208);
	ASSERT_FALSE(samples[2] > 0xFF);
	ASSERT_EQUAL(samples[3], 244);
	for (i = 4; i < 8; i++)
		ASSERT_EQUAL(samples[i], 0xFFFF);

	// Fill with dummy data
	for (i = 0; i < 8; i++)
		samples[i] = 0xFFFF;

	ADC_stop();
	// Make ADC_SingleShot initialize driver
	ASSERT_RETURNS(ADC_SingleShot(samples), 0);
	ASSERT_RETURNS(hal_clone_set_adc(4, 952), 0);
	ASSERT_RETURNS(ADC_SingleShot(samples), 0);

	ASSERT_FALSE(samples[0] > 0x3FF);
	ASSERT_FALSE(samples[1] == 208);
	ASSERT_FALSE(samples[2] > 0x3FF);
	ASSERT_FALSE(samples[3] == 244);
	ASSERT_EQUAL(samples[4], 952);
	ASSERT_FALSE(samples[5] > 0x3FF);
	ASSERT_FALSE(samples[6] > 0x3FF);
	ASSERT_FALSE(samples[7] > 0x3FF);

	// Sanity check
	samples[4] = 42222;
	ASSERT_RETURNS(hal_clone_set_adc_all(NULL), HAL_CLONE_ERR_NULL);
	ASSERT_RETURNS(hal_clone_set_adc_all(samples), HAL_CLONE_ERR_OOB);

	// Fill with dummy data to set
	for (i = 0; i < 8; i++)
		samples[i] = i;

	ASSERT_RETURNS(hal_clone_set_adc_all(samples), 0);

	// Fill with dummy data
	for (i = 0; i < 8; i++)
		samples[i] = 0xFFFF;

	ASSERT_RETURNS(ADC_SingleShot(samples), 0);

	for (i = 0; i < 8; i++)
		ASSERT_EQUAL(samples[i], i);

	TEST_SUCCESS;
}
