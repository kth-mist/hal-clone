#include <stdio.h>
#include <string.h>

#include <hal/Storage/FRAM.h>

#include <at91/boards/ISIS_OBC_G20/board.h>

#include <at91/memories/norflash/NorFlashCommon.h>
#include <at91/memories/norflash/NorFlashApi.h>
#include <at91/memories/norflash/NorFlashCFI.h>

#include <hal-clone/storage.h>

#include "test.h"

static unsigned char _fram_loc[FRAM_MAX_ADDRESS + 1];
static unsigned char _norflash_loc[BOARD_NORFLASH_SIZE];

int test_hcc(void)
{
	// TODO: Since the HCC stubs are used for unit testing
	// the OBS API, testing them here too is not as important.
	TEST_SUCCESS;
}

int test_fram_write_read(void)
{
	unsigned char data[8];
	unsigned char expected[8] = { 1, 34, 45, 78, 23, 124, 64, 90 };
	memset(data, 1, sizeof(data));

	hal_clone_set_fram_in_memory(_fram_loc);

	ASSERT_RETURNS(FRAM_start(), 0);
	ASSERT_RETURNS(FRAM_read(data, 100, sizeof(data)), 0);
	for (size_t i = 0; i < sizeof(data); i++) {
		ASSERT_EQUAL(data[i], 0);
	}

	ASSERT_RETURNS(FRAM_write(expected, 2000, sizeof(expected)), 0);
	ASSERT_RETURNS(FRAM_read(data, 2000, sizeof(data)), 0);
	for (size_t i = 0; i < sizeof(data); i++) {
		ASSERT_EQUAL(data[i], expected[i]);
	}

	FRAM_stop();

	TEST_SUCCESS;
}

int test_norflash_write_read(void)
{
	struct NorFlash flash;
	unsigned char data[4000];
	unsigned char expected[8] = { 1, 34, 45, 78, 23, 124, 64, 90 };
	memset(data, 1, sizeof(data));

	hal_clone_set_norflash_in_memory(_norflash_loc);

	ASSERT_RETURNS(NorFlash_CFI_Detect(&flash, BOARD_NORFLASH_BUSWIDTH >> 3), NorCommon_ERROR_UNKNOWNMODEL);

	flash.norFlashInfo.baseAddress = BOARD_NORFLASH_BASE_ADDRESS;
	ASSERT_RETURNS(NorFlash_CFI_Detect(&flash, BOARD_NORFLASH_BUSWIDTH), NorCommon_ERROR_UNKNOWNMODEL);
	ASSERT_RETURNS(NorFlash_CFI_Detect(&flash, BOARD_NORFLASH_BUSWIDTH >> 3), NorCommon_ERROR_NONE);

	ASSERT_RETURNS(NORFLASH_ReadData(&flash, 100, data, 8), NorCommon_ERROR_NONE);
	for (size_t i = 0; i < 8; i++) {
		ASSERT_EQUAL(data[i], 0xFF); // Erased data is 0xFF
	}
	for (size_t i = 0; i < BOARD_NORFLASH_SIZE; i++) {
		_norflash_loc[i] = 0; // Set entire contents of norflash to 0 to test erase
	}

	// This should erase the first sector in the second region, i.e. offset 0x10000-0x1FFFF
	ASSERT_RETURNS(NORFLASH_EraseSector(&flash, 0x10001), NorCommon_ERROR_NONE);

	ASSERT_RETURNS(NORFLASH_WriteData(&flash, 66000, expected, sizeof(expected)), 0);
	ASSERT_RETURNS(NORFLASH_ReadData(&flash, 65000, data, sizeof(data)), 0);
	for (size_t i = 65000; i < 65000 + sizeof(data); i++) {
		if (i < 0x10000) {
			ASSERT_EQUAL(data[i - 65000], 0); // First region, not erased
		} else if (i < 66000) {
			ASSERT_EQUAL(data[i - 65000], 0xff); // Second region, erased
		} else if (i < 66008) {
			ASSERT_EQUAL(data[i - 65000], expected[i - 66000]); // where we wrote the data
		} else {
			ASSERT_EQUAL(data[i - 65000], 0xff); // Second region, erased
		}
	}

	TEST_SUCCESS;
}
