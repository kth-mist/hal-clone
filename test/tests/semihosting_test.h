#ifndef SEMIHOSTING_TEST_H_
#define SEMIHOSTING_TEST_H_

int test_semihosting_elapsed(void);
int test_semihosting_halt(void);

#endif /* SEMIHOSTING_TEST_H_ */
